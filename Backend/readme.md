**Installation**

1. Run `composer install` (and select your database credentials in wizard)
2. Run `bin/console doctrine:schema:create`
3. Run `bin/console doctrine:schema:update --force`
4. Run `bin/console doctrine:fixtures:load` 

**Using**

Visit api/doc route for swagger
