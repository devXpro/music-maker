<?php

namespace SamplerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SamplerRenderMp3Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('sampler:render-mp3')
            ->setDescription('Mp3 Rendering')
            ->addArgument('project_id', InputArgument::REQUIRED, 'ProjectId');
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = $this->getContainer()->get('doctrine.orm.default_entity_manager')
            ->getRepository('SamplerBundle:Project')
            ->find($input->getArgument('project_id'));
        if (!$project) {
            $output->writeln('<error>Id was not found</error>');
            return;
        }
        $this->getContainer()->get('sampler.mp3.mp3renderer')->renderProject($project);
    }

}
