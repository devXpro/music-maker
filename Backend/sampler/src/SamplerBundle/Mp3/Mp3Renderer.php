<?php

namespace SamplerBundle\Mp3;

use Doctrine\ORM\EntityManager;
use SamplerBundle\Entity\Project;
use SamplerBundle\File\FileProvider;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Mp3Renderer
{
    const FOLDER = 'demo_samples/trap/';
    const IS_MAC = false;

    const SOXI_PATH = '/usr/'.(self::IS_MAC ? 'local/' : '').'bin/soxi';
    const SOX_PATH = '/usr/'.(self::IS_MAC ? 'local/' : '').'bin/sox';
    const LAME_PATH = '/usr/'.(self::IS_MAC ? 'local/' : '').'bin/lame';

    /** @var string */
    protected $tempFolder;

    /** @var EntityManager */
    protected $em;

    /** @var FileProvider */
    protected $fileProvider;

    public function __construct(EntityManager $em, FileProvider $fileProvider)
    {
        $this->em = $em;
        $this->fileProvider = $fileProvider;
    }

    public function renderProject(Project $project)
    {
        $save = $project->getSave();
        $samples = [];
        foreach ($save['loops'] as $loop) {
            if ($loop['muted']) {
                continue;
            }
            foreach ($loop['hits'] as $hit) {
                $samples[] = $hit;
            }
        }

        $samplesGroupedBySound = [];
        foreach ($samples as $sample) {
            $sample['startTime'] = round($sample['time'] / 1000, 6);
            $mp3SoundPath = $this->getFullPath($sample['url'], false, true);
            $pathParts = explode('/', $mp3SoundPath);
            $sound = str_replace('.mp3', '.wav', end($pathParts));
            $soundPath = $this->getTempFolder().'/'.$sound;

            if (!isset($samplesGroupedBySound[$sound])) {
                $this->runProcess(sprintf('%s --decode %s %s', self::LAME_PATH, $mp3SoundPath, $soundPath));
            }

            unset($sample['time']);
            unset($sample['sound']);
            unset($sample['url']);
            $sample['endTime'] = $this->getSampleDuration($sound) + $sample['startTime'];

            $samplesGroupedBySound[$sound][] = $sample;
        }

        $samplesGroupedBySoundSorted = [];
        foreach ($samplesGroupedBySound as $key => $timings) {
            usort(
                $timings,
                function ($timing1, $timing2) {
                    return $timing1['startTime'] <=> $timing2['startTime'];
                }
            );
            $samplesGroupedBySoundSorted[$key] = $timings;
        }
        $mixCommand = self::SOX_PATH.' -m';
        foreach ($samplesGroupedBySoundSorted as $sampleName => $timings) {
            $lastStart = 0;
            $lastFinish = 0;

            $command = self::SOX_PATH;

            foreach ($timings as $timing) {
                $nextStart = $timing['startTime'];
                $delta = $nextStart - $lastFinish;
                if ($lastStart === $nextStart) {
                    list($lastStart, $lastFinish) = $this->setNextIterationTiming($nextStart, $timing);
                    continue;
                } elseif ($delta > 0) {
                    if ($lastStart === 0) {
                        $command .= ' '.$this->makePause($delta);
                    } else {
                        $command .= ' '.$this->getFullPath($sampleName).' '.$this->makePause($delta);
                    }
                } elseif ($delta == 0) {
                    if ($lastStart === 0) {
                        list($lastStart, $lastFinish) = $this->setNextIterationTiming($nextStart, $timing);
                        continue;
                    } else {
                        $command .= ' '.$this->getFullPath($sampleName);
                    }
                } elseif ($delta < 0) {
                    $command .= ' '.$this->crop($sampleName, $delta);
                } else {
                    throw new Exception('Unexpected behaviour');
                }
                list($lastStart, $lastFinish) = $this->setNextIterationTiming($nextStart, $timing);
            }

            $compositionLength = (($save['barCount'] * 4) / $save['bpm']) * 60;
            $delta = $compositionLength - $lastFinish;
            if ($delta < 0) {
                $command .= ' '.$this->crop($sampleName, $delta);
            } elseif ($delta > 0) {
                $command .= ' '.$this->getFullPath($sampleName).' '.$this->makePause($delta);
            } else {
                $command .= ' '.$this->getFullPath($sampleName);
            }

            $trackName = sprintf('%s/track_%s', $this->getTempFolder(), $sampleName);
            $command .= ' '.$trackName;
            $this->runProcess($command);
            $mixCommand .= ' '.$trackName;
        }

        if (count($samplesGroupedBySoundSorted) === 1) {
            $finishFile = $trackName;
        } else {
            $finishFile = $this->getTempFolder().'/finish.wav';
            $mixCommand .= ' '.$finishFile;
            $this->runProcess($mixCommand);
        }
        $finMp3Name = $project->getPreset()->getId().'/mp3/'.
            preg_replace('/[^A-Za-z0-9_\-]/', '_',$project->getUser()->getUsername()).'/'
            .preg_replace('/[^A-Za-z0-9_\-]/', '_', $project->getName()).'.mp3';
        $finFullName = $this->getFullPath($finMp3Name, false);
        $this->runProcess(self::LAME_PATH.' '.$finishFile.' '.$finFullName);
        $this->normalize($finFullName);
        $this->removeTempFolder();
        $project->setMp3Url('uploads/'.$finMp3Name);
        $this->em->flush($project);
    }

    protected function normalize(string $file)
    {
        $tempFile = $file.'_temp.mp3';
        $this->runProcess(self::SOX_PATH.' --norm '.$file .' '.$tempFile);
        $this->runProcess('rm '. $file);
        $this->runProcess('mv '.$tempFile.' '.$file);
    }

    protected function runProcess($command): string
    {
        $process = new Process($command);
        $process->run();
        if (!$process->isSuccessful()) {
            $this->removeTempFolder();
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    protected function makePause($time)
    {
        $folder = $this->getTempFolder();
        $file = $folder.'/pause_'.uniqid().'.wav';

        $this->runProcess(sprintf('%s -n -r 44100 -c 2 %s trim 0 %s', self::SOX_PATH, $file, $time));

        return $file;
    }

    protected function crop($file, $crop)
    {
        $folder = $this->getTempFolder();
        $outputFile = $folder.'/crop_'.uniqid().'.wav';
        $time = $this->getSampleDuration($file) + $crop;
        $file = $this->getFullPath($file);

        $this->runProcess(sprintf('%s %s %s trim 0 %s', self::SOX_PATH, $file, $outputFile, $time));

        return $outputFile;
    }

    protected function getSampleDuration(string $name)
    {
        $filePath = $this->getFullPath($name);

        return (float)str_replace("\n", "", $this->runProcess(self::SOXI_PATH.' -D '.$filePath));
    }

    protected function getFullPath(string $name, $tempFolder = true, $pathWithUpload = false)
    {
        if ($tempFolder) {
            $file = $this->getTempFolder().'/'.$name;
        } elseif (!$pathWithUpload) {
            $file = $this->fileProvider->getFileFolder().'/'.$name;
        } else {
            $file = $this->fileProvider->getFileFolder().'/'.str_replace('uploads/', '', $name);
        }

        $parts = explode('/', $file);
        array_pop($parts);
        $dir = implode('/', $parts);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        return $file;
    }

    protected function getTempFolder()
    {
        if (!$this->tempFolder) {
            $this->tempFolder = $this->fileProvider->getFileFolder().'/'.uniqid();
            mkdir($this->tempFolder);
        }

        return $this->tempFolder;
    }

    public function setNextIterationTiming($nextStart, $timing): array
    {
        $lastStart = $nextStart;
        $lastFinish = $timing['endTime'];

        return [$lastStart, $lastFinish];
    }

    public function removeTempFolder()
    {
        $this->runProcess('rm -rf '.$this->getTempFolder());
    }
}
