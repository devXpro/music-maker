<?php

namespace SamplerBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use SamplerBundle\Entity\Preset;
use SamplerBundle\Entity\Repository\PresetRepository;
use SamplerBundle\Entity\Repository\SampleRepository;
use SamplerBundle\Entity\Sample;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Nelmio\ApiDocBundle\Annotation\Model;
use SamplerBundle\Entity\Style;

class SampleController extends AbstractRestController
{
    /**
     * @Extra\Route("/preset/list", name="preset_list")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Samples"},
     *     description="Get sample presets",
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns all presets",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Preset::class, groups={"preset_list"}))
     *        )
     * )
     *  )
     *
     * @return Response
     */
    public function presetsAction()
    {
        $presets = $this->getPresets();

        return $this->renderSuccessResponse($presets, ['preset_list']);
    }

    /**
     * @Extra\Route("/style/list", name="style_list")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Samples"},
     *     description="Get styles",
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns all styles",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Style::class, groups={"style_list"}))
     *        )
     * )
     *  )
     *
     * @return Response
     */
    public function stylesListAction()
    {
        $styles = $this->getDoctrine()->getRepository('SamplerBundle:Style')->findAll();

        return $this->renderSuccessResponse($styles, ['style_list']);
    }

    /**
     * @Extra\Route("/preset/list_with_samples", name="preset_list_with_samples")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Samples"},
     *     description="Get sample presets with samples",
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns all presets",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Preset::class, groups={"preset_with_samples", "sample_list"}))
     *        )
     * )
     *  )
     *
     * @return Response
     */
    public function presetsWithSamplesAction()
    {
        $presets = $this->getPresets();

        return $this->renderSuccessResponse($presets, ['preset_with_samples', 'sample_list']);
    }

    /**
     * @Extra\Route("/sample/{presetId}/list", name="sample_list_by_id")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Samples"},
     *     description="Get samples by preset",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns all samples by preset",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Sample::class, groups={"sample_list"}))
     *        )
     *     ),
     *      @SWG\Response(
     *          response=404,
     *          description="Preset with id %s was not found"
     *      )
     * )
     *
     * @return Response
     */
    public function sampleListAction($presetId)
    {
        $preset = $this->getPresetRepository()->find($presetId);
        if (!$preset) {
            return new Response(sprintf('Preset with id %s was not found', $presetId), 404);
        }
        $samples = $this->getSampleRepository()->findBy(['preset' => $preset]);

        return $this->renderSuccessResponse($samples, ['sample_list']);
    }

    protected function getPresets()
    {
        return $this->getPresetRepository()->findAll();
    }

    /**
     * @return SampleRepository
     */
    protected function getSampleRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Sample');
    }

    /**
     * @return PresetRepository
     */
    protected function getPresetRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Preset');
    }
}
