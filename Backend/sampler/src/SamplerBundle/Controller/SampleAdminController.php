<?php

namespace SamplerBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use SamplerBundle\Entity\Preset;
use SamplerBundle\Entity\Repository\PresetRepository;
use SamplerBundle\Entity\Repository\SampleRepository;
use SamplerBundle\Entity\Sample;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Nelmio\ApiDocBundle\Annotation\Model;

class SampleAdminController extends AbstractRestController
{
    /**
     * @Extra\Route("/preset/{presetId}", name="preset_create_or_update")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Samples"},
     *     description="Create new preset or update exists. If you need create preset, just use 'new' as presetId'",
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         in="formData",
     *         name="backgroundImage",
     *         type="file",
     *         required=false
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="logoImage",
     *         type="file",
     *         required=false
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="video",
     *         type="file",
     *         required=false
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="color",
     *         type="string",
     *     ),
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns created or updated preset",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Preset::class, groups={"preset_list"}))
     *        )
     *    ),
     *     @SWG\Response(
     *          response=413,
     *          description="Preset with name %s already exists"
     *      )
     * )
     *
     *
     * @return Response
     */
    public function presetCreateAction(Request $request, $presetId)
    {
        $em = $this->getDoctrine()->getManager();
        $name = $request->get('name');

        if ($presetId === 'new') {
            $preset = new Preset();
            if (!$name) {
                return $this->renderError(409, sprintf('Field "name" is required for new preset'));
            }
            $preset->setName($name);
            $em->persist($preset);
            $presetId = $preset->getId();
        } else {
            $preset = $this->getPresetRepository()->find($presetId);
            if (!$preset) {
                return $this->renderError(409, sprintf('Preset with id %s does not exists', $presetId));
            }
        }

        /** @var Preset|null $presetByName */
        $presetByName = $this->getPresetRepository()->findOneBy(['name' => $name]);
        if ($presetByName && $presetByName->getId() !== $preset->getId()) {
            return $this->renderError(409, sprintf('Preset with name %s already exists', $name));
        }
        $em->flush();


        $fileProvider = $this->get('sampler.file.file_provider');
        /** @var UploadedFile $upload */
        $backgroundImage = $request->files->get('backgroundImage');
        $logoImage = $request->files->get('logoImage');
        $video = $request->files->get('video');

        try {
            if ($backgroundImage) {
                $preset->setBackgroundUrl(
                    $fileProvider->saveFile($backgroundImage, $presetId.'/background', ['jpg', 'jpeg', 'png'], 3000)
                );
            }

            if ($logoImage) {
                $preset->setLogoUrl($fileProvider->saveFile($logoImage, $presetId.'/logo', ['jpg', 'png'], 500));
            }

            if ($video) {
                $preset->setVideoUrl($fileProvider->saveFile($video, $presetId.'/video', ['mp4', 'avi'], 50000));
            }
        } catch (\Exception $exception) {
            return $this->renderError(409, $exception->getMessage());
        }
        $preset->setName($name);
        $preset->setColor($request->get('color'));


        $em->flush();

        return $this->renderSuccessResponse($preset, ['preset_list']);
    }

    /**
     * @Extra\Route("/preset/delete/{presetId}", name="preset_delete")
     * @Extra\Method({"DELETE"})
     *
     * @SWG\Delete(
     *     tags={"Samples"},
     *     description="Delete existing preset",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns 200 if success",
     *    ),
     *    @SWG\Response(
     *          response=409,
     *          description="Preset with id %s does not exists"
     *    )
     * )
     *
     *
     * @param $presetId
     * @return Response
     */
    public function presetsDeleteAction($presetId)
    {
        $preset = $this->getPresetRepository()->find($presetId);
        if (!$preset) {
            return $this->renderError(409, sprintf('Preset with id %s does not exists', $presetId));
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($preset);
        $em->flush();

        return $this->renderSuccessResponse();
    }


    /**
     * @Extra\Route("/sample/upload/{presetId}", name="sample_upload")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Samples"},
     *     description="Sample Upload",
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *         in="formData",
     *         name="mp3File",
     *         type="file",
     *         required=false
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="color",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="fontColor",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="title",
     *         type="string",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="key",
     *         type="string",
     *         required=true
     *     ),
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns uploaded sample data",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=Sample::class, groups={"sample_list"}))
     *        )
     *    ),
     *    @SWG\Response(
     *          response=409,
     *          description="File restrictions or allowed key restricitons"
     *    )
     * )
     *
     *
     * @param Request $request
     * @param $presetId
     * @return Response
     */
    public function uploadSampleAction(Request $request, $presetId)
    {
        /** @var Preset|null $preset */
        $preset = $this->getPresetRepository()->find($presetId);
        if (!$preset) {
            return $this->renderError(409, sprintf('Preset with id %s does not exists', $presetId));
        }
        $key = $request->get('key');

        if (!in_array($key, Sample::ALLOWED_KEYS)) {
            return $this->renderError(
                413,
                sprintf('Wrong sample key, allowed keys is: [%s]', implode(', ', Sample::ALLOWED_KEYS))
            );
        }

        $sample = $this->getSampleRepository()->findOneBy(['key' => $key, 'preset' => $preset]) ?: new Sample();

        /** @var UploadedFile $upload */
        $upload = $request->files->get('mp3File');

        if ($upload) {
            try {
                $url = $this->get('sampler.file.file_provider')->saveFile($upload, $presetId, ['mp3']);
            } catch (\Exception $exception) {
                return $this->renderError(409, $exception->getMessage());
            }
            $sample->setUrl($url);
        }
        $sample->setKey($key);
        $sample->setPreset($preset);
        $sample->setTitle($request->get('title'));
        $sample->setColor($request->get('color'));
        $sample->setFontColor($request->get('fontColor'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($sample);
        $em->flush();

        return $this->renderSuccessResponse($sample, ['sample_list']);
    }

    protected function getPresets()
    {
        return $this->getPresetRepository()->findAll();
    }

    /**
     * @return SampleRepository
     */
    protected function getSampleRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Sample');
    }

    /**
     * @return PresetRepository
     */
    protected function getPresetRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Preset');
    }
}
