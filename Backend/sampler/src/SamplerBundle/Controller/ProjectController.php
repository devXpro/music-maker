<?php

namespace SamplerBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use SamplerBundle\Entity\ProjectAssessment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Nelmio\ApiDocBundle\Annotation\Model;
use SamplerBundle\Entity\Project;

class ProjectController extends AbstractRestController
{
    /**
     * @Extra\Route("/all_projects/list", name="all_projects_list")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Projects"},
     *     description="Get all projects",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          in="query",
     *          type="string",
     *          description="Search by any fields in datagrid"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_field",
     *          in="query",
     *          type="string",
     *          description="Name of sorting field"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_order",
     *          in="query",
     *          type="string",
     *          description="asc or desc"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          in="query",
     *          type="integer",
     *          description="limit"
     *     ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          type="integer",
     *          description="page"
     *     ),
     *     @SWG\Response(
     *        response=200,
     *        description="Returns all projects",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                      type=Project::class,
     *                      groups={"project_list", "style_list", "user_list", "preset_list", "timestampable"}
     *                      )
     *             )
     *        )
     * )
     *  )
     *
     * @return Response
     */
    public function allProjectsAction(Request $request)
    {
        $qb = $this->getProjectRepository()->createQueryBuilder('project');

        return $this->renderDatagridResponse(
            $request,
            $qb,
            ['search_field' => 'name'],
            ['project_list', 'user_list', "style_list", 'preset_list', 'timestampable']
        );
    }

    /**
     * @Extra\Route("/all_projects/items_on_list", name="projects_on_list")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Projects"},
     *     description="Returns max items on list",
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *        response=200,
     *        description="max items on list",
     *        @SWG\Schema(
     *            type="integer",
     *        )
     * )
     *  )
     *
     * @return Response
     */
    public function itemsOnListAction()
    {
        return $this->renderSuccessResponse(30);
    }

    /**
     * @Extra\Route("/all_projects/vote/{projectId}/{stars}", name="project_vote")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Projects"},
     *     description="Project vote",
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Stars voting of project",
     *        @SWG\Schema(
     *            type="float",
     *        ),
     *     @SWG\Response(
     *          response=409,
     *          description="Sorry, but you already voted for this project"
     *      ),
     *     @SWG\Response(
     *          response=409,
     *          description="Stars value can be 1,2,3,4 or 5"
     *      )
     * )
     *  )
     *
     * @param int $projectId
     * @return Response
     */
    public function voteAction($projectId, $stars){
        $project = $this->findProjectById($projectId);
        if ($project instanceof Response) {
            return $project;
        }

        $em = $this->getDoctrine()->getManager();
        $projectAssessmentRepository = $em->getRepository('SamplerBundle:ProjectAssessment');
        $assessment = $projectAssessmentRepository->findOneBy(['user'=>$this->getUser(), 'project'=>$project]);
        if($assessment){
            return $this->renderError(409, 'Sorry, but you already voted for this project');
        }
        if($stars < 1 || $stars > 5){
            return $this->renderError(409, 'Stars value can be 1,2,3,4 or 5');
        }

        $assessments = $projectAssessmentRepository->findBy(['project'=>$project]);

        $count = 1;
        $sum = $stars;
        foreach ($assessments as $assessment){
            $sum += $assessment->getAssessment();
            $count++;
        }

        $rate = $sum/$count;

        $project->setRate($rate);
        $assessment = new ProjectAssessment();
        $assessment->setAssessment($stars);
        $assessment->setProject($project);
        $assessment->setUser($this->getUser());
        $em->persist($assessment);
        $em->flush();

        return $this->renderSuccessResponse(['message' => 'Your vote is accepted', 'newRate' => $rate]);
    }

    /**
     * @Extra\Route("/own_projects/list", name="own_projects_list")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Projects"},
     *     description="Get own projects",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          in="query",
     *          type="string",
     *          description="Search by any fields in datagrid"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_field",
     *          in="query",
     *          type="string",
     *          description="Name of sorting field"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_order",
     *          in="query",
     *          type="string",
     *          description="asc or desc"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          in="query",
     *          type="integer",
     *          description="limit"
     *     ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          type="integer",
     *          description="page"
     *     ),
     *     @SWG\Response(
     *        response=200,
     *        description="Returns own projects",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                      type=Project::class,
     *                      groups={"project_list", "style_list", "user_list", "preset_list", "timestampable"})
     *                      )
     *            )
     *       )
     *  )
     *
     * @return Response
     */
    public function ownProjectsAction(Request $request)
    {
        $qb = $this->getProjectRepository()->createQueryBuilder('project');
        $qb->where('project.user = :user')
            ->setParameter('user', $this->getUser());

        return $this->renderDatagridResponse(
            $request,
            $qb,
            ['search_field' => 'name'],
            ['project_list', 'user_list', 'preset_list', 'style_list','timestampable']
        );
    }

    /**
     * @Extra\Route("/own_projects/new", name="own_project_new")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Projects"},
     *     description="create new project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="new_project",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", description="project name"),
     *              @SWG\Property(property="save", type="object", description="save"),
     *              @SWG\Property(property="preset_id", type="integer", description="preset id")
     *         )
     *     ),
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns created or updated preset",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                  type=Project::class,
     *                  groups={"project_list", "user_list", "style_list", "style_list", "preset_list", "timestampable"}
     *                  )
     *            )
     *        )
     *    ),
     *     @SWG\Response(
     *          response=409,
     *          description="Preset with id %s was not found"
     *      )
     * )
     *
     *
     * @return Response
     */
    public function postProjectAction(Request $request)
    {
        return $this->updateProject($request, new Project());
    }

    /**
     * @Extra\Route("/own_projects/clone/{projectId}", name="own_project_clone")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Projects"},
     *     description="create new project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns cloned project",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                  type=Project::class,
     *                  groups={"project_list", "style_list", "user_list", "preset_list", "timestampable"}
     *                  )
     *            )
     *        )
     *    ),
     *     @SWG\Response(
     *          response=409,
     *          description="Project with id %s was not found"
     *      )
     * )
     *
     *
     * @return Response
     */
    public function cloneProjectAction($projectId)
    {
        $project = $this->findProjectById($projectId);
        if ($project instanceof Response) {
            return $project;
        }

        $clonedProject = new Project();
        $clonedProject->setMp3Url($project->getMp3Url());
        $clonedProject->setUser($this->getUser());
        $clonedProject->setPreset($project->getPreset());
        $clonedProject->setSave($project->getSave());
        $clonedProject->setStyle($project->getStyle());
        $clonedProject->setName($project->getName().'_cloned');

        $em = $this->getDoctrine()->getManager();
        $em->persist($clonedProject);
        $em->flush();

        return $this->renderSuccessResponse(
            $clonedProject,
            ["project_list", "user_list", "style_list", "preset_list", "timestampable"]
        );
    }

    /**
     * @Extra\Route("/own_projects/delete/{projectId}", name="own_project_delete")
     * @Extra\Method({"DELETE"})
     *
     * @SWG\Delete(
     *     tags={"Projects"},
     *     description="Delete project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Response(
     *          response=409,
     *          description="Project with id %s was not found"
     *      )
     * )
     * @param $projectId
     * @return Response
     */
    public function deleteOwnProject($projectId){
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProjectRepository()->findOneBy(['id' => (int)$projectId, 'user' => $this->getUser()]);

        if (!$project) {
            return $this->renderError(409, sprintf('Project with id %s was not found for this user', $projectId));
        }

        $em->remove($project);
        $em->flush();

        return $this->renderSuccessResponse();
    }

    /**
     * @Extra\Route("/own_projects/edit/{projectId}", name="own_project_edit")
     * @Extra\Method({"PUT"})
     *
     * @SWG\Put(
     *     tags={"Projects"},
     *     description="edit project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="new_project",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", description="project name"),
     *              @SWG\Property(property="save", type="object", description="save"),
     *              @SWG\Property(property="preset_id", type="integer", description="preset id")
     *         )
     *     ),
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns created or updated preset",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                  type=Project::class,
     *                  groups={"project_list", "user_list", "preset_list", "timestampable"}
     *                  )
     *            )
     *        )
     *    ),
     *     @SWG\Response(
     *          response=409,
     *          description="Project or Preset with id %s was not found"
     *      ),
     * )
     *
     * @param Request $request
     * @param $projectId
     * @return Response
     */
    public function editProjectAction(Request $request, $projectId)
    {
        $project = $this->findProjectById($projectId);
        if ($project instanceof Response) {
            return $project;
        }

        return $this->updateProject($request, $project);
    }

    /**
     * @Extra\Route("/all_projects/get/{projectId}", name="project_get")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Projects"},
     *     description="get project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Response(
     *        response=200,
     *        description="Returns created or updated preset",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(
     *                  type=Project::class,
     *                  groups={"project_list", "user_list", "style_list", "preset_list", "timestampable"}
     *                  )
     *            )
     *        )
     *    ),
     *     @SWG\Response(
     *          response=409,
     *          description="Project or Preset with id %s was not found"
     *      ),
     * )
     *
     * @param Request $request
     * @param string $projectId
     * @return Response
     */
    public function getProjectAction($projectId)
    {
        $project = $this->getDoctrine()->getRepository('SamplerBundle:Project')->find((int)$projectId);

        if (!$project) {
            return $this->renderError(409, sprintf('Project with id %s was not found', $projectId));
        }

        return $this->renderSuccessResponse(
            $project,
            ["project_list", "user_list", "style_list", "preset_list", "timestampable"]
        );
    }

    /**
     * @Extra\Route("/all_projects/mp3/{projectId}", name="own_project_mp3")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Projects"},
     *     description="edit project",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Response(
     *          response=409,
     *          description="Project or Preset with id %s was not found"
     *      ),
     * )
     *
     * @param Request $request
     * @param $projectId
     * @return Response
     */
    public function mp3ProjectAction(Request $request, $projectId)
    {
        $project = $this->getDoctrine()->getRepository('SamplerBundle:Project')->find((int)$projectId);

        if (!$project) {
            return $this->renderError(409, sprintf('Project with id %s was not found', $projectId));
        }

        $this->get('sampler.mp3.mp3renderer')->renderProject($project);

        return $this->renderSuccessResponse();
    }

    protected function updateProject(Request $request, Project $project): Response
    {
        $presetId = (int)$request->get('preset_id');
        $preset = $this->getPresetRepository()->find($presetId);
        if (!$preset) {
            return $this->renderError(409, sprintf('Preset with id %s was not found', $presetId));
        }
        $em = $this->getDoctrine()->getManager();
        $style = $this->getDoctrine()->getRepository('SamplerBundle:Style')->find($request->get('style_id'));
        $project->setUser($this->getUser());
        $project->setName($request->get('name'));
        $project->setSave($request->get('save'));
        $project->setStyle($style);
        $project->setPreset($preset);

        $em->persist($project);
        $em->flush();

        $rootDir = str_replace('/app', '/bin', $this->get('kernel')->getRootDir());
        $cmd = sprintf('%s/console %s %s', $rootDir, 'sampler:render-mp3', $project->getId());


        exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, '1.txt', '2.txt'));

        return $this->renderSuccessResponse(
            $project,
            ["project_list", "user_list", "style_list", "preset_list", "timestampable"]
        );
    }

    protected function getPresetRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Preset');
    }

    protected function getProjectRepository()
    {
        return $this->getDoctrine()->getRepository('SamplerBundle:Project');
    }

    /**
     * @param $projectId
     * @return null|object|Project|Response
     */
    protected function findProjectById($projectId){
        $project = $this->getProjectRepository()->find((int)$projectId);

        if (!$project) {
            return $this->renderError(409, sprintf('Project with id %s was not found', $projectId));
        }

        return $project;
    }
}
