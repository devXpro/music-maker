<?php

namespace SamplerBundle\File;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

class FileProvider
{
    const UPLOAD_FILES_DIR = 'uploads';
    const MAX_FILE_SIZE = 1000;

    /** @var KernelInterface */
    protected $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getFileFolder(): string
    {
        $rootDir = $this->kernel->getRootDir();
        $parts = explode('/', $rootDir);
        array_pop($parts);
        $path = implode('/', $parts);

        return $path.'/web/'.self::UPLOAD_FILES_DIR;
    }

    public function saveFile(
        UploadedFile $file,
        $folder,
        $allowedTypes = [],
        $maxSize = self::MAX_FILE_SIZE
    ): string {
        $maxSize = $maxSize * 1024;
        $extension = $file->getClientOriginalExtension();
        if ($allowedTypes && !in_array($extension, $allowedTypes, true)) {
            throw new Exception(sprintf('You can upload only %s files', implode(', ', $allowedTypes)));
        }

        if ($file->getClientSize() > $maxSize) {
            throw new Exception(
                sprintf(
                    'You try to upload file with size %s bytes but the allowed size is not more than %s KBytes',
                    $file->getClientSize(),
                    $maxSize
                )
            );
        }

        $fileName = $file->getClientOriginalName();
        $name = str_replace('.'.$extension, '', $fileName);
        $fileName = preg_replace('/[^A-Za-z0-9_\-]/', '_', $name).'.'.$extension;
        $baseFolder = $this->getFileFolder();
        $folderToSave = $baseFolder.'/'.$folder;

        if (!is_dir($folderToSave)) {
            mkdir($folderToSave, 0777, true);
        }
        $file->move($folderToSave, $fileName);

        return self::UPLOAD_FILES_DIR.'/'.$folder.'/'.$fileName;
    }
}
