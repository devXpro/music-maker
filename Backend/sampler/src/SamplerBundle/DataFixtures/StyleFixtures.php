<?php

namespace SamplerBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use SamplerBundle\Entity\Style;

class StyleFixtures extends Fixture
{
    const STYLES = [
        'Blues',
        'Country',
        'Easy listening',
        'Trap',
        'Folk',
        'Hip hop',
        'Jazz',
        'Latin',
        'Pop',
        'R&B',
        'Rock',
        'Funk',
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::STYLES as $styleName) {
            $style = new Style();
            $style->setName($styleName);
            $manager->persist($style);
        }

        $manager->flush();

        $styles = $manager->getRepository('SamplerBundle:Style')->findAll();
        $projects = $manager->getRepository('SamplerBundle:Project')->findAll();
        foreach ($projects as $project) {
            if (!$project->getStyle()) {
                $project->setStyle($styles[rand(0, count($styles) - 1)]);
            }
        }

        $manager->flush();
    }
}
