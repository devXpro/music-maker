<?php

namespace SamplerBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use SamplerBundle\Entity\Preset;
use SamplerBundle\Entity\Sample;

class SampleFixtures extends Fixture
{
    const PRESETS = [
        'preset.1' => [
            'name' => 'Danish Symphonic Orchestra',
            'logo' => 'http://nicholascollon.co.uk/wp-content/uploads/2018/02/Danish-National-SO1.jpg',
            'video' => 'https://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_10mb.mp4',
            'color' => '#c15000'
        ],
        'preset.2' => [
            'name' => 'Swedish Symphonic Orchestra',
            'logo' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/G%C3%B6teborgs_Symfoniker_Logo.svg/220px-G%C3%B6teborgs_Symfoniker_Logo.svg.png',
            'video' => 'https://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_10mb.mp4',
            'color' => '#c100bd'
        ],
        'preset.3' => [
            'name' => 'Norwegian Symphonic Orchestra',
            'logo' => 'http://harmonien.no/images/logo-01_EN.svg',
            'video' => 'https://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_10mb.mp4',
            'color' => '#00db2f'
        ],
    ];

    public function load(ObjectManager $manager)
    {
        $presets = $manager->getRepository('SamplerBundle:Preset')->findAll();
        $samples = $manager->getRepository('SamplerBundle:Sample')->findAll();
        $anyProject = $manager->getRepository('SamplerBundle:Project')->findOneBy([]);
        if ($anyProject) {
            return;
        }
        $removeObjects = array_merge($samples, $presets);
        foreach ($removeObjects as $removeObject) {
            $manager->remove($removeObject);
        }
        $manager->flush();

        foreach (self::PRESETS as $presetReference => $presetData) {
            $preset = new Preset();
            $preset->setName($presetData['name']);
            $preset->setLogoUrl($presetData['logo']);
            $preset->setVideoUrl($presetData['video']);
            $preset->setBackgroundUrl(
                'https://krannertcenter.com/sites/krannertcenter.com/files/krannert/main_image/1718_CSO.jpg'
            );
            $manager->persist($preset);

            $parts = explode('/', __DIR__);
            $parts = array_splice($parts, 0, -3);

            $path = implode('/', $parts).'/web/uploads/demo_samples';
            $sampleThemes = $this->dirToArray($path);
            $samples = [];
            foreach ($sampleThemes as $sampleTheme => $sampleFiles) {
                foreach ($sampleFiles as $sampleFile) {
                    $parts = explode('.mp3', $sampleFile);
                    $samples[] = [
                        'title' => str_replace(['_', '-'], ' ', ucfirst($parts[0])),
                        'file' => $sampleTheme.'/'.$sampleFile,
                    ];
                }
            }

            foreach (Sample::ALLOWED_KEYS as $index => $key) {
                $sample = new Sample();
                $sampleData = $samples[$index];
                $sample->setTitle($sampleData['title'])
                    ->setPreset($preset)
                    ->setColor($presetData['color'])
                    ->setFontColor('#ffffff')
                    ->setUrl('uploads/demo_samples'.'/'.$sampleData['file'])
                    ->setKey($key);
                $manager->persist($sample);
            }

            $manager->flush();
        }

    }

    protected function dirToArray($dir)
    {

        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dir.DIRECTORY_SEPARATOR.$value)) {
                    $result[$value] = $this->dirToArray($dir.DIRECTORY_SEPARATOR.$value);
                } else {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 2)), 2, '0', STR_PAD_LEFT);
    }

    function random_color()
    {
        return $this->random_color_part().$this->random_color_part().$this->random_color_part();
    }
}
