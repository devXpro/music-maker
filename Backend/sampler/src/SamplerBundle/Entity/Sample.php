<?php

namespace SamplerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Sample
 *
 * @ORM\Table(name="sample")
 * @ORM\Entity(repositoryClass="SamplerBundle\Entity\Repository\SampleRepository")
 */
class Sample
{
    const ALLOWED_KEYS = [
        "Digit1",
        "Digit2",
        "Digit3",
        "Digit4",
        "Digit5",
        "Digit6",
        "Digit7",
        "Digit8",
        "Digit9",
        "Digit0",
        "Minus",
        "Equal",
        "KeyQ",
        "KeyW",
        "KeyE",
        "KeyR",
        "KeyT",
        "KeyY",
        "KeyU",
        "KeyI",
        "KeyO",
        "KeyP",
        "BracketLeft",
        "BracketRight",
        "KeyA",
        "KeyS",
        "KeyD",
        "KeyF",
        "KeyG",
        "KeyH",
        "KeyJ",
        "KeyK",
        "KeyL",
        "Semicolon",
        "Quote",
        "KeyZ",
        "KeyX",
        "KeyC",
        "KeyV",
        "KeyB",
        "KeyN",
        "KeyM",
        "Comma",
        "Period",
        "Slash",
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var Preset
     *
     * @ORM\ManyToOne(targetEntity="SamplerBundle\Entity\Preset", inversedBy="samples", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="preset_id", nullable=false, referencedColumnName="id", onDelete="CASCADE")
     */
    private $preset;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"sample_list"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     * @Groups({"sample_list"})
     */
    private $color;

    /**
     * @var string|null
     *
     * @ORM\Column(name="font_color", type="string", length=255, nullable=true)
     * @Groups({"sample_list"})
     */
    private $fontColor;

    /**
     * @var string
     *
     * @ORM\Column(name="sample_key", type="string", length=20, nullable=false)
     * @Groups({"sample_list"})
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1000, nullable=true)
     * @Groups({"sample_list"})
     */
    private $url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getPreset(): Preset
    {
        return $this->preset;
    }

    public function setPreset(Preset $preset): self
    {
        $this->preset = $preset;

        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getFontColor(): ?string
    {
        return $this->fontColor;
    }

    public function setFontColor(?string $fontColor): self
    {
        $this->fontColor = $fontColor;

        return $this;
    }
}
