<?php

namespace SamplerBundle\Entity;

use ApiBundle\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;
use JMS\Serializer\Annotation\Groups;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="SamplerBundle\Entity\Repository\ProjectRepository")
 */
class Project
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"project_list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"project_list"})
     */
    private $name;

    /**
     * @var array
     *
     * @ORM\Column(name="save", type="json_array")
     * @Groups({"project_list"})
     */
    private $save;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float")
     * @Groups({"project_list"})
     */
    private $rate = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * @Groups({"project_list"})
     */
    private $user;

     /**
     * @var Preset
     *
     * @ORM\ManyToOne(targetEntity="SamplerBundle\Entity\Preset")
     * @ORM\JoinColumn(name="preset_id", referencedColumnName="id", onDelete="CASCADE")
     * @Groups({"project_list"})
     */
    private $preset;

     /**
     * @var Style
     *
     * @ORM\ManyToOne(targetEntity="SamplerBundle\Entity\Style")
     * @ORM\JoinColumn(name="style_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"project_list"})
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="mp3_url", type="string", nullable=true)
     * @Groups({"project_list"})
     */
    private $mp3Url;

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setSave(array $save): self
    {
        $this->save = $save;

        return $this;
    }

    public function getSave(): array
    {
        return $this->save;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPreset(): Preset
    {
        return $this->preset;
    }

    public function setPreset(Preset $preset): self
    {
        $this->preset = $preset;

        return $this;
    }

    public function getMp3Url(): ?string
    {
        return $this->mp3Url;
    }

    public function setMp3Url(?string $mp3Url): self
    {
        $this->mp3Url = $mp3Url;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(Style $style): self
    {
        $this->style = $style;

        return $this;
    }
}
