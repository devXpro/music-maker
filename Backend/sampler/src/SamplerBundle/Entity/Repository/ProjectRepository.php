<?php

namespace SamplerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use SamplerBundle\Entity\Project;
use UserBundle\Entity\User;

class ProjectRepository extends EntityRepository
{
    /**
     * @param null|string $sortField
     * @param null|string $order
     * @param User|null $user
     * @return Project[]
     */
    public function findProjects(?string $sortField, ?string $order, User $user = null)
    {
        $qb = $this->createQueryBuilder('project');
        if ($user) {
            $qb->where('project.user = :user')
                ->setParameter('user', $user);
        }
        if ($sortField && $order) {
            $qb->orderBy('project.'.$sortField, $order);
        }

        $qb->setMaxResults(20);

        return $qb->getQuery()->getResult();
    }
}
