<?php

namespace SamplerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Preset
 *
 * @ORM\Table(name="preset")
 * @ORM\Entity(repositoryClass="SamplerBundle\Entity\Repository\PresetRepository")
 */
class Preset
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="background_url", type="string", length=1000, nullable=true)
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $backgroundUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_url", type="string", length=1000, nullable=true)
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $logoUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="video_url", type="string", length=1000, nullable=true)
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $videoUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=20, nullable=true)
     * @Groups({"preset_list", "preset_with_samples"})
     */
    private $color;

    /**
     * @var Sample[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="SamplerBundle\Entity\Sample", mappedBy="preset")
     * @Groups({"preset_with_samples"})
     */
    private $samples;

    public function __construct()
    {
        $this->samples = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addSample(Sample $sample): self
    {
        if (!$this->samples->contains($sample)) {
            $this->samples->add($sample);
        }

        return $this;
    }

    public function getBackgroundUrl(): ?string
    {
        return $this->backgroundUrl;
    }

    public function setBackgroundUrl(?string $backgroundUrl): self
    {
        $this->backgroundUrl = $backgroundUrl;

        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function setLogoUrl(?string $logoUrl): self
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
