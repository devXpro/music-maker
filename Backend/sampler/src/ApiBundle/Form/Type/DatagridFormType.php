<?php

namespace ApiBundle\Form\Type;

use Doctrine\ORM\QueryBuilder as ORMQueryBuilder;

class DatagridFormType extends AbstractDatagridFormType
{
    protected function addQueryForORM(ORMQueryBuilder $qb, string $entityAlias): void
    {
    }
}
