<?php

namespace ApiBundle\Form\Type;

use Doctrine\ORM\QueryBuilder as ORMQueryBuilder;
use Doctrine\ORM\UnexpectedResultException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractDatagridFormType extends AbstractType
{
    /** @var int */
    public $count;

    /** @var bool */
    public $hasPagination;

    /** @var string */
    protected $search;

    /** @var string */
    protected $searchField;

    /** @var array */
    protected $searchFields;

    /** @var string */
    protected $defaultSortField;

    /** @var string */
    protected $sortField;

    /** @var string */
    protected $sortOrder;

    /** @var int */
    protected $limit;

    /** @var int */
    protected $offset;

    /** @var int */
    protected $page;

    /** @var FormInterface */
    protected $form;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('search', TextType::class, ['mapped' => false, 'required' => false])
            ->add('sort_field', TextType::class, ['mapped' => false, 'required' => false])
            ->add(
                'sort_order',
                ChoiceType::class,
                ['mapped' => false, 'required' => false, 'choices' => ['asc' => 'asc', 'desc' => 'desc']]
            )
            ->add('limit', IntegerType::class, ['mapped' => false, 'required' => false])
            ->add('page', IntegerType::class, ['mapped' => false, 'required' => false]);
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'postSubmit']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'search_field' => null,
                'search_fields' => [],
                'default_sort_field' => null,
                'csrf_protection' => false,
            ]
        );
    }

    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $this->initOptions($form);
        $qb = $event->getData();
        if ($qb instanceof ORMQueryBuilder) {
            $this->applyQueryForORM($qb);
        } else {
            $formError = new FormError('Wrong query builder instance');
            $form->addError($formError);
        }
    }

    protected function applyQueryForORM(ORMQueryBuilder $qb): void
    {
        $entityAlias = $qb->getRootAliases()[0];
        $this->addQueryForORM($qb, $entityAlias);
        if ($this->search) {
            if ($this->searchField) {
                $qb->andWhere(sprintf('%s.%s LIKE :search', $entityAlias, $this->searchField))
                    ->setParameter('search', "%{$this->search}%");
            } elseif ($this->searchFields) {
                $expressions = [];
                foreach ($this->searchFields as $searchField){
                    $expressions[] = sprintf('%s.%s LIKE :search', $entityAlias, $searchField);
                }
                $qb->andWhere(implode(' OR ',$expressions))
                    ->setParameter('search', "%{$this->search}%");
            }
        }
        $countQb = clone $qb;
        try {
            $this->count = (int)$countQb->select(sprintf('count(%s.id)', $entityAlias))
                ->getQuery()
                ->getSingleScalarResult();
        } catch (UnexpectedResultException $exception) {
            $this->count = 0;
        }
        if ($this->hasPagination) {
            $qb->setMaxResults($this->limit)->setFirstResult($this->offset);
        }
        if ($this->sortField) {
            $order = $this->sortOrder ?: 'desc';
            $qb->orderBy($entityAlias.'.'.$this->sortField, $order);
        } elseif ($this->defaultSortField) {
            $qb->orderBy($entityAlias.'.'.$this->defaultSortField, 'desc');
        }
    }

    public function initOptions(FormInterface $form): void
    {
        $this->form = $form;
        $this->limit = (int)$form->get('limit')->getData();
        $this->page = (int)$form->get('page')->getData();
        $this->search = (string)$form->get('search')->getData();
        $this->sortField = (string)$form->get('sort_field')->getData();
        $this->sortOrder = (string)$form->get('sort_order')->getData();
        $this->offset = $this->limit * $this->page - $this->limit;
        $config = $form->getConfig();
        $this->searchField = (string)$config->getOption('search_field');
        $this->searchFields = (array)$config->getOption('search_fields');
        $this->defaultSortField = (string)$config->getOption('default_sort_field');
        $this->hasPagination = $this->limit && $this->page;
    }

    abstract protected function addQueryForORM(ORMQueryBuilder $qb, string $entityAlias): void;
}
