<?php

namespace ApiBundle\Helper;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestHelper
{
    public function getCrossResponse(Request $request, $responseBody = '')
    {
        return new Response($responseBody, 200, $this->getCrossHeaders($request));
    }

    public function getCrossHeaders(Request $request)
    {
        $refer = $request->headers->get('Referer');
        if ($refer) {
            $parts = parse_url($refer);
            $star = $parts['scheme'] . '://' . $parts['host'];
            if (array_key_exists('port', $parts) && $parts['port']) {
                $star = $star . ':' . $parts['port'];
            }
        } else {
            $star = '*';
        }

        return [
            'Access-Control-Allow-Origin' => $star,
            'Access-Control-Allow-Credentials' => "true",
            'Access-Control-Allow-Headers' => 'Accept, Cookie, Content-Type, Authorization, Content-Disposition',
            'Access-Control-Allow-Methods' => 'GET, HEAD, OPTIONS, POST, PUT, PATCH, DELETE',
            'Access-Control-Max-Age' => 14400,
        ];
    }
}
