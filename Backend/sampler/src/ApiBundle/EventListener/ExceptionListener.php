<?php

namespace ApiBundle\EventListener;

use ApiBundle\Helper\RequestHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /** @var RequestHelper */
    protected $requestHelper;

    /** @var RequestStack */
    protected $requestStack;

    /**
     * ExceptionListener constructor.
     * @param RequestHelper $requestHelper
     * @param RequestStack $requestStack
     */
    public function __construct(RequestHelper $requestHelper, RequestStack $requestStack)
    {
        $this->requestHelper = $requestHelper;
        $this->requestStack = $requestStack;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = new JsonResponse(['message' => $exception->getMessage()]);

        if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
            if ($code === 405 || $code === 404) {
                $request = $this->requestStack->getCurrentRequest();
                $response = $this->requestHelper->getCrossResponse($request);
                $response->setStatusCode(200);
                $response->headers->set('X-Status-Code', 200);
                $event->setResponse($response);
                return;
            }
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}
