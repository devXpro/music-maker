<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FileController extends AbstractRestController
{
    public function getFileAction(Request $request)
    {
        return new JsonResponse(['Yeah!!']);
    }

    public function getRawFile($fileName)
    {
        if (file_exists($fileName)) {
            if (strpos($fileName, '.xlsx') !== false) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($fileName));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($fileName));
                // читаем файл и отправляем его пользователю
                if ($fd = fopen($fileName, 'rb')) {
                    while (!feof($fd)) {
                        print fread($fd, 1024);
                    }
                    fclose($fd);
                }
                exit;
            } else {
                header('Content-Type: ' . mime_content_type($fileName));
                readfile($fileName);
                exit;
            }
        }

        return $this->renderError(404);
    }

}
