<?php

namespace ApiBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AbstractRestController extends FOSRestController
{
    protected function renderDatagridResponse(
        Request $request,
        QueryBuilder $qb,
        array $parameters = [],
        array $serializationGroups = [],
        string $formServiceDefinition = null
    ): Response {
        $formService = $formServiceDefinition ? $this->get($formServiceDefinition) :
            $this->get('api.form_type.datagrid_form_type');
        $form = $this->createForm(get_class($formService), $qb, $parameters);
        $form->submit($request->query->all());

        if ($form->isValid()) {
            /** @var QueryBuilder $qb */
            $qb = $form->getData();
            $query = $qb->getQuery();
            $data = $qb instanceof QueryBuilder ?  $query->getResult() : $query->execute()->toArray(false);
            $responseData = $formService->hasPagination ? ['count' => $formService->count, 'data' => $data] : $data;

            return $this->renderSuccessResponse($responseData, $serializationGroups);
        }

        return $this->renderFormError($form);
    }


    protected function renderError($code, $message = null) : Response
    {
        $view = new View();
        $view->setFormat('json');
        $view->setData(
            [
                'message' => $this->get('translator')->trans($message),
            ]
        )
            ->setStatusCode($code);

        return $this->handleView($view);
    }

    protected function renderFormError(FormInterface $form) : Response
    {
        $view = new View();
        $view->setFormat('json');
        $errors = $form->getErrors(true);
        $resultErrors = [];
        foreach ($errors as $error) {
            $resultErrors[] = sprintf('Invalid %s: %s', $error->getOrigin()->getName(), $error->getMessage());
        }
        $view->setData(['message' => $resultErrors])
            ->setStatusCode(400);

        return $this->handleView($view);
    }

    /**
     * @param array|string|object $data
     * @param array $groups
     * @param bool $crossRequest
     * @return Response
     */
    protected function renderSuccessResponse($data = [], $groups = [], $crossRequest = true): Response
    {
        $view = new View();
        $view->setData($data)->setFormat('json')->setStatusCode(200);

        if ($groups) {
            $context = new Context();
            $context->setGroups($groups);
            $view->setContext($context);
        }

        if ($crossRequest) {
            $request = $this->get('request_stack')->getCurrentRequest();
            $view->setHeaders($this->get('api.helper.request_helper')->getCrossHeaders($request));
        }

        return $this->handleView($view);
    }

    /**
     * @param object|object[] $entity
     * @param array $groups
     * @param string $format
     * @return string
     */
    protected function serialize($entity, $groups = [], $format = 'array')
    {
        $context = SerializationContext::create()->setGroups($groups)->setSerializeNull(true);
        $serializer = $this->get('jms_serializer');

        return $format === 'array' ?
            $serializer->toArray($entity, $context) :
            $serializer->serialize($entity, $format, $context);
    }
}
