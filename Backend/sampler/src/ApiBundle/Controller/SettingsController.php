<?php

namespace ApiBundle\Controller;

use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;

class SettingsController extends AbstractRestController
{
    /**
     * @Extra\Route("/get", name="get_settings")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"Settings"},
     *     description="get settings",
     *     consumes={"application/json"},
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @return Response
     */
    public function getSettings(): Response
    {
        return $this->renderSuccessResponse($this->get('settings_manager')->all());
    }

    /**
     * @Extra\Route("/set", name="set_settings")
     * @Extra\Method({"POST"})
     *
     * @SWG\Post(
     *     tags={"Settings"},
     *     description="set settings",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="facebook_link", type="string", description="facebook_link"),
     *              @SWG\Property(property="instagram_link", type="string", description="instagram_link")
     *         )
     *     ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     * @return Response
     */
    public function setSettings(Request $request): Response
    {
        $sm = $this->get('settings_manager');
        $sm->setMany($request->request->all());

        return $this->renderSuccessResponse($sm->all());
    }
}
