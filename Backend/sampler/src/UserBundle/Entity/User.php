<?php

namespace UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="sampler_user")
 */
class User implements UserInterface
{
    const TYPE_VISITOR = 0;
    const TYPE_ADMIN = 1;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user_list"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"user_list"})
     */
    protected $username;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_list"})
     * @JMS\Type("DateTime<'U'>")
     */
    protected $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_list"})
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_list"})
     */
    protected $photoUrl;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $type = 0;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $token;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @Groups({"user_list"})
     */
    protected $askProfileData = true;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $facebookId;

    /**
     * @return array
     * @throws \Exception
     */
    public function getRoles()
    {
        if($this->type === self::TYPE_VISITOR){
            return ['ROLE_VISITOR'];
        } elseif ($this->type === self::TYPE_ADMIN){
            return ['ROLE_VISITOR', 'ROLE_ADMIN'];
        } else{
            throw new \Exception('Role eas not found');
        }
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
    
    public function setType(int $type): self 
    {
        $this->type = $type;
        
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTime
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTime $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getPhotoUrl(): ?string
    {
        return $this->photoUrl;
    }

    public function setPhotoUrl(string $photoUrl): self
    {
        $this->photoUrl = $photoUrl;

        return $this;
    }

    public function isAskProfileData(): ?bool
    {
        return $this->askProfileData;
    }

    public function setAskProfileData(bool $askProfileData): self
    {
        $this->askProfileData = $askProfileData;

        return $this;
    }
}
