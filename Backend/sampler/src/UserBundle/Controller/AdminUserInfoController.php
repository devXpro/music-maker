<?php

namespace UserBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Nelmio\ApiDocBundle\Annotation\Model;
use UserBundle\Entity\User;
use Swagger\Annotations as SWG;

class AdminUserInfoController extends AbstractRestController
{
    /**
     * @Extra\Route("/users/list", name="get_all_users")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"User profile"},
     *     description="get profile",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          in="query",
     *          type="string",
     *          description="Search by any fields in datagrid"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_field",
     *          in="query",
     *          type="string",
     *          description="Name of sorting field"
     *     ),
     *     @SWG\Parameter(
     *          name="sort_order",
     *          in="query",
     *          type="string",
     *          description="asc or desc"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          in="query",
     *          type="integer",
     *          description="limit"
     *     ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="query",
     *          type="integer",
     *          description="page"
     *     ),
     *     @SWG\Response(
     *        response=200,
     *        description="Returns user profile data",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"preset_list"}))
     *        )
     *    ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @return Response|JsonResponse
     */
    public function getProfileAction(Request $request)
    {
        $qb = $this->getDoctrine()->getRepository('UserBundle:User')->createQueryBuilder('user');

        return $this->renderDatagridResponse($request, $qb, ['search_fields'=> ['username', 'city']], ['user_list']);
    }
}

