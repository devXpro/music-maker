<?php

namespace UserBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Nelmio\ApiDocBundle\Annotation\Model;
use UserBundle\Entity\User;
use Swagger\Annotations as SWG;

class UserInfoController extends AbstractRestController
{
    /**
     * @Extra\Route("/profile", name="get_profile")
     * @Extra\Method({"GET"})
     *
     * @SWG\Get(
     *     tags={"User profile"},
     *     description="get profile",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns user profile data",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"preset_list"}))
     *        )
     *    ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @return Response|JsonResponse
     */
    public function getProfileAction()
    {
        return $this->renderSuccessResponse($this->getUser(), ['user_list']);
    }

    /**
     * @Extra\Route("/profile", name="edit_profile")
     * @Extra\Method({"PUT"})
     *
     * @SWG\Put(
     *     tags={"User profile"},
     *     description="edit profile",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="dateOfBirth", type="string", description="date of birth in linux time"),
     *              @SWG\Property(property="city", type="string", description="city"),
     *              @SWG\Property(property="askProfileData", type="boolean", description="Ask Profile Data")
     *         )
     *     ),
     *     @SWG\Response(
     *        response=200,
     *        description="Returns user profile data",
     *        @SWG\Schema(
     *            type="array",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"preset_list"}))
     *        )
     *    ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @return Response|JsonResponse
     */
    public function editProfileAction(Request $request){
        /** @var User $user */
        $user = $this->getUser();

        if($request->get('dateOfBirth')){
            try {
                $user->setDateOfBirth((new \DateTime())->setTimestamp($request->get('dateOfBirth')));
            } catch (\Exception $exception){
                return $this->renderError(400, 'dateOfBirth must be in linux time, for example: 1531160243');
            }
        }

        if($request->get('city')){
            $user->setCity($request->get('city'));
        }

        if($request->get('askProfileData') !== null){
            $user->setAskProfileData($request->get('askProfileData'));
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->renderSuccessResponse($this->getUser(), ['user_list']);
    }
}

