<?php

namespace UserBundle\Controller;

use ApiBundle\Controller\AbstractRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 *
 */
class LoginController extends AbstractRestController
{
    /**
     * @Extra\Route("/login", name="login_by_name_password")
     * @Extra\Method({"POST"})
     *
     *
     * @SWG\Post(
     *     tags={"User"},
     *     description="Login by username and password",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="username", type="string", description="Username"),
     *              @SWG\Property(property="password", type="string", description="Password")
     *         )
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="",
     *          @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="token", type="string", description="User token")
     *          )
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="User has not found"
     *      ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @param Request $request
     * @return Response
     */
    public function loginByNamePasswordAction(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('UserBundle:User')->findOneBy(['username' => $username]);
        if (!$user) {
            return $this->renderError(404, 'User was not found');
        }

        if (!$this->checkPassword($user, $password)) {
            return $this->renderError(403, 'Invalid credentials');
        }

        return new JsonResponse(['token' => $this->generateToken($user)]);
    }

    /**
     * @Extra\Route("/facebook_login", name="login_by_facebook")
     * @Extra\Method({"POST"})
     *
     *
     * @SWG\Post(
     *     tags={"User"},
     *     description="facebook login",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="facebook_token", type="string", description="Facebook token")
     *         )
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="",
     *          @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="token", type="string", description="User token")
     *          )
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="User has not found"
     *      ),
     *      @SWG\Response(
     *          response=403,
     *          description="Invalid credentials"
     *      )
     *  )
     *
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function facebookLoginAction(Request $request)
    {
        $facebookToken = $request->get('facebook_token');
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('UserBundle:User');

        try {
            $facebookData = $this->getFacebookData($facebookToken);
        } catch (\Exception $exception) {
            return $this->renderError(403, $exception->getMessage());
        }

        $user = $userRepository->findOneBy(['facebookId' => $facebookData['id']]);

        if (!$user) {
            $user = new User();
            $user->setUsername($facebookData['name']);
            $user->setFacebookId($facebookData['id']);
            $em->persist($user);
        }

        $user->setPhotoUrl($facebookData['picture']['url']);
        $em->flush();

        return new JsonResponse(['token' => $this->generateToken($user)]);
    }

    /**
     * @Extra\Route("/registration", name="registration_by_name_password")
     * @Extra\Method({"POST"})
     *
     *
     * @SWG\Post(
     *     tags={"User"},
     *     description="Registration by username and password",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="username", type="string", description="Username"),
     *              @SWG\Property(property="password", type="string", description="Password")
     *         )
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="",
     *          @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref=@Model(type=User::class, groups={"user_list"}))
     *        )
     *      ),
     *      @SWG\Response(
     *          response=409,
     *          description="User with username %s already exist"
     *      )
     *  )
     *
     *
     * @param Request $request
     * @return Response
     */
    public function registrationByLoginPasswordAction(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('UserBundle:User')->findOneBy(['username' => $username]);
        if ($user) {
            return $this->renderError(409, sprintf('User with username %s already exist', $username));
        }
        $user = new User();
        $user->setUsername($username);
        $user->setType(User::TYPE_VISITOR);
        $this->setPassword($user, $password);
        $em->persist($user);
        $em->flush();

        return $this->renderSuccessResponse($user, ['user_list']);
    }

    /**
     * @Extra\Route("/geo", name="geo_data")
     * @Extra\Method({"GET"})
     *
     *
     * @SWG\Get(
     *     tags={"User"},
     *     description="Get geo data",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *        response=200,
     *        description="Returns geo data",
     *    )
     * )
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function geoAction(Request $request): JsonResponse
    {
        return new JsonResponse(
            file_get_contents(
                sprintf('http://gd.geobytes.com/AutoCompleteCity?%s', http_build_query($request->query->all()))
            )
        );
    }

    /**
     * @Extra\Route("/credentials_modify", name="credentials_modify")
     * @Extra\Method({"POST"})
     *
     *
     * @SWG\Post(
     *     tags={"User"},
     *     description="Modify credentials any exists user",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="Login form",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="master_key", type="string", description="master key"),
     *              @SWG\Property(property="user", type="string", description="id or username"),
     *              @SWG\Property(property="password", type="string", description="Password"),
     *              @SWG\Property(property="type", type="integer", description="User Type")
     *         )
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="",
     *          @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref=@Model(type=User::class, groups={"user_list"}))
     *        )
     *      ),
     *      @SWG\Response(
     *          response=403,
     *          description="Wrong master key"
     *      ),
     *      @SWG\Response(
     *          response=409,
     *          description="User has not found"
     *      )
     *  )
     *
     *
     * @param Request $request
     * @return Response
     */
    public function credentialsModifyAction(Request $request)
    {
        $masterKey = $request->get('master_key');
        $userData = $request->get('user');
        $password = $request->get('password');
        $type = $request->get('type');

        if ($masterKey !== 'linkrust') {
            return $this->renderError(403, 'Wrong master key');
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository('UserBundle:User')->find($userData);
        if (!$user) {
            $user = $em->getRepository('UserBundle:User')->findOneBy(['username' => $userData]);
            if (!$user) {
                return $this->renderError(404, 'User was not found');
            }
        }

        $user->setType($type);
        $this->setPassword($user, $password);
        $em->persist($user);
        $em->flush();

        return $this->renderSuccessResponse($user, ['user_list']);
    }

    protected function checkPassword(User $user, $password)
    {
        return md5($user->getSalt().$password) === $user->getPassword();
    }

    public function setPassword(User $user, $password)
    {
        $user->setPassword(md5($user->getSalt().$password));
    }

    /**
     * @param string $socialToken
     * @return array
     * @throws \Exception
     */
    protected function getFacebookData($socialToken)
    {
        $fb = $this->get('facebook_sdk');
        $fb->setDefaultAccessToken($socialToken);

        $response = $fb->get('/me?fields=id,name,email,gender,picture');
        $userNode = $response->getGraphUser();

        return $userNode->asArray();
    }

    protected function generateToken(User $user): string
    {
        $token = $user->getToken();

        if (!$token) {
            $token = bin2hex(random_bytes(20));
            $user->setToken($token);
            $this->getDoctrine()->getManager()->flush();
        }

        return $token;
    }
}
