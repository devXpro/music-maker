<?php

namespace UserBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
    const AUTHORIZATION_HEADER_KEY = 'Authorization';

    public function getCredentials(Request $request)
    {
        return $this->getTokenFromHeader($request);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByUsername($credentials);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(['message' => 'Authentication Required'], Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(['message' => 'Authentication Required'], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Request $request
     * @return string|null
     */
    protected function getTokenFromHeader(Request $request)
    {
        if (!$token = $request->headers->get(self::AUTHORIZATION_HEADER_KEY)) {
            $headers = $this->getHeaders();
            if (array_key_exists(self::AUTHORIZATION_HEADER_KEY, $headers)) {
                $token = $headers[self::AUTHORIZATION_HEADER_KEY];
            } elseif (array_key_exists(strtolower(self::AUTHORIZATION_HEADER_KEY), $headers)) {
                $token = $headers[strtolower(self::AUTHORIZATION_HEADER_KEY)];
            } else {
                $token = null;
            }
        }

        return $token;
    }

    protected function getHeaders()
    {
        if (!function_exists('getallheaders')) {
            if (!is_array($_SERVER)) {
                return [];
            }

            $headers = [];
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(
                        ' ',
                        '-',
                        ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))
                    )] = $value;
                }
            }

            return $headers;
        }

        return getallheaders();
    }
}
