const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const APIURL = "https://api.sampler.coderoom.pro";

// Is the current build a development build
const IS_DEV = (process.env.NODE_ENV === 'dev');

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, 'app');
const dirAssets = path.join(__dirname, 'app');

/**
 * Webpack Configuration
 */
module.exports = {
    entry: {
        vendor: [
            'jquery',
            'select2'
        ],
        bundle: path.join(dirApp, 'index')
    },
    resolve: {
        modules: [
            dirNode,
            dirApp,
            dirAssets
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            IS_DEV: IS_DEV
        }),

        new HtmlWebpackPlugin({
            template: path.join(dirApp, 'index.html')
        }),

        new HtmlWebpackPlugin({
            filename: 'classified.html',
            template: path.join(dirApp, 'classified.html')
        }),


        new HtmlWebpackPlugin({
            filename: 'projects.html',
            template: path.join(dirApp, 'projects.html')
        }),

        new HtmlWebpackPlugin({
            filename: 'my_projects.html',
            template: path.join(dirApp, 'my_projects.html')
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        }),
    ],
    module: {
        rules: [
            // BABEL
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                options: {
                    compact: true
                }
            },

            // STYLES
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: IS_DEV
                        }
                    },
                ]
            },

            // CSS / SASS
            {
                test: /\.scss/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: IS_DEV
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: IS_DEV,
                            includePaths: [dirAssets]
                        }
                    }
                ]
            },

            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            // IMAGES,
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[path][name].[hash].[ext]",
                    },
                },
            },


        ]
    },
    // htmlLoader: {
    //     ignoreCustomFragments: [/\{\{.*?}}/],
    //     root: path.resolve(__dirname, ''),
    //     attrs: ['img:src', 'link:href']
    // },
    devServer: {
        stats: "errors-only",
        open: true,
        proxy: {
            "/api": {
                "target": APIURL,
                "secure": false
            },
            "/uploads": {
                "target": APIURL,
                "secure": false
            },
            "/demo_samples": {
                "target": APIURL,
                "secure": false
            },
            "/samples": {
                "target": APIURL,
                "secure": false
            }
        }
    }
};
