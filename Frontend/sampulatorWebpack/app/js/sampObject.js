window.AudioContext = window.AudioContext || window.webkitAudioContext;
import root from './shared/root';

export default class projectObject {

    static createProjectObject() {
        root.Obj = {
            ...root.Obj,
            project: {
                ...root.Obj.project,
                loopDuration: 0,
                bpm: 130,
                barCount: 4,
                beatsPerBar: 4,
                loops: {}
            },
            bpmRange: 80,
            bpmStartValue: 70,
            hasDb: false,


            keyboardMap: {
                49: 'key_1',
                50: 'key_2',
                51: 'key_3',
                52: 'key_4',
                53: 'key_5',
                54: 'key_6',
                55: 'key_7',
                56: 'key_8',
                57: 'key_9',
                48: 'key_0',
                189: 'MINUS',
                187: 'EQUALS',
                81: 'key_q',
                87: 'key_w',
                69: 'key_e',
                82: 'key_r',
                84: 'key_t',
                89: 'key_y',
                85: 'key_u',
                73: 'key_i',
                79: 'key_o',
                80: 'key_p',
                219: 'OPEN_BRACKET',
                221: 'CLOSE_BRACKET',

                65: 'key_a',
                83: 'key_s',
                68: 'key_d',
                70: 'key_f',
                71: 'key_g',
                72: 'key_h',
                74: 'key_j',
                75: 'key_k',
                76: 'key_l',
                186: 'SEMICOLON',
                222: 'QUOTE',

                90: 'key_z',
                88: 'key_x',
                67: 'key_c',
                86: 'key_v',
                66: 'key_b',
                78: 'key_n',
                77: 'key_m',
                188: 'COMMA',
                190: 'DOT',
                191: 'SLASH',
            },

            soundsToKeys: {},
            soundsToGroups: {},
            soundsToColor: {},
            groupSounds: {}
        };

        root.Obj.context = new AudioContext;

    }

    static keyCodes() {
        let key, keyObject, keys;

        keys = root.Obj.keys;

        for (key in keys) {

            keyObject = keys[key];
            let {sound, color} = keyObject;

            root.Obj.soundsToKeys[sound] = key;
            root.Obj.soundsToColor[sound] = color;
        }
    }
}
