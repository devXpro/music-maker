import app from './app.js';
import httpClient from './sampApi';
import root from './shared/root';

// stopRecordingClearing -> root.Obj.variable.StReq

export function mainLogic() {
    var a = null, b, c = 0, d = 260, e, f = null,
        g = root.Obj.context.createDynamicsCompressor(),
        h = 0, j, addNewProjectToLoop, RemoveOllLoop, addNewLoop,
        clearProject, btnPlayStop, btnRecord,
        l, m = null, o = false, q = false, r = false, s = false, t = {},
        u = [], v = 0,
        x, y = null, z = 0, A, B, playingTrack = false,
        D, E, F, H = null, J = 0, cleaningProject, L,
        N = [], O = true,
        //P = true,
        S = null, T = false, U = true, V, W = 0, X, Y, stopClearRecord,
        aa, ba, ca = {}, ea = null,
        removeHit
    ;

    root.Obj = {
        ...root.Obj,
        variable: {
            runnerIsActive: false,
            StReq: 0
        },
        method: {
            ...root.Obj.method,
            H
        },
        sounds: {}
    };



    B = root.Obj.context.createGain();
    B.gain.value = 0.8;

    const C = `<div class='runner'>
                <div class='actions'>
                    <input class="js-track track" type="text" />
                    <div class='action action-delete js-delete-loop-prompt'>
                        DELETE
                    </div>
                    <div class='action action-mute js-mute'>
                        MUTE
                    </div>
                    <div class='action action-active js-record-active'>
                        ACTIVE
                    </div>
                </div>
        </div>`;

    const CC2 = `<div class='recording-progress js-progress-bg'></div>`;

    /*
    * DONE
    * */
    root.Obj['saveRecording'] = function(type) {
        console.log( 'rebuildingControlAndLoop saveRecording')
        const {bpm, barCount, beatsPerBar, loops} = root.Obj.project;

        const objForSave = {
            lastLoopId: v,
            bpm: bpm,
            barCount: barCount,
            beatsPerBar: beatsPerBar,
            loops: {},
        };

        for (let d in loops) {
            if ('metronome' !== d) {
                const { hits, muted, name, lastHitId } = loops[d];

                const a = {
                    hits: {},
                    muted,
                    name,
                    lastHitId
                };

                for (let c in hits) {
                    const {sound, time, url} = hits[c];
                    a.hits[c] = {
                        sound,
                        time,
                        url
                    };
                }
                objForSave.loops[d] = a;
            }
        }

        return Object.keys(objForSave.loops).length && httpClient.saveProject({
            name: root.Obj.project.title,
            save: objForSave,
            style_id: root.Obj.project.styleId,
            preset_id: document.querySelector('.js-preset-btn.active')
                ? document.querySelector('.js-preset-btn.active').dataset.id
                : root.Obj.edit.preset_id
        }, type);

    };
    /**
     * DONE
     * @returns {Array}
     */
    addNewProjectToLoop = function(loopIndex) {
        $('.runners').append(C);
        const loopHits = root.Obj.project.loops[loopIndex].hits;
        const lastLoop = $('.runners .runner').last();
        const hitsList = [];
        lastLoop[0].dataset.id = loopIndex;
        lastLoop.find('.recording-progress').remove();

        lastLoop.find('.js-track').val(root.Obj.project.loops[loopIndex].name);

        if (root.Obj.project.loops[loopIndex].muted) {
            lastLoop.addClass('muted');
            lastLoop.find('.js-mute').addClass('active');
            lastLoop.find('.js-show-actions').addClass('active');
        }

        Object.keys(loopHits).forEach(hitKey => {
           const hit = loopHits[hitKey];
           const timePercent = hit.time / root.Obj.project.loopDuration * 100;
            /**
             * Unused variables
             */
           const soundName = hit.sound;
           const hitControl = root.Obj.soundsToKeys[soundName];
           const hitLabel = root.Obj.keys[hitControl].label;
           const hitTemplate = `<div class='hit ' style='left: ${timePercent}%; background: ${hit.color}' data-id='${hitKey}'></div>`;

            hitsList.push(lastLoop.append(hitTemplate));
        });

        return hitsList;
    };

    clearProject = function() {
        let scope_v = v;
        let runnerIsActive = root.Obj.variable.runnerIsActive;
        let inNum = Number.isInteger(runnerIsActive);

        if (inNum) {
            scope_v = runnerIsActive;
        }

        root.Obj.project.loops[scope_v] = {
            muted: false,
            hits: {},
            name: `track ${scope_v}`,
            lastHitId: 0,
            loopGain: root.Obj.context.createGain()
        };

        $(".runner[data-id='" + scope_v + "'] .hit").remove()

    };

    root.Obj.method['clearProject'] = clearProject;

    /*
     * DONE
     * */
    stopClearRecord = function() {
        let oneHit,
            runnerIsActive = root.Obj.variable.runnerIsActive,
            inNum = Number.isInteger(runnerIsActive),
            scope_v = inNum ? runnerIsActive : v,
            hits = root.Obj.project.loops[scope_v] === undefined ? [] : root.Obj.project.loops[scope_v].hits;

        s = false;
        r = false;
        Y();

        if (Object.keys(hits).length > 0) {

            for (let b in hits) {
                oneHit = hits[b]
                delete oneHit.neverPlayed;
                oneHit.isOverwritable = true;
                oneHit.hitGain.gain.value = 1;
            }
        }

        $('.js-btn-record').removeClass('active')
        $('.runner .recording-progress').remove()
    };

    addNewLoop = function() {
        v += 1,
            root.Obj.project.loops[v] = {
                muted: false,
                hits: {},
                name: `track ${v}`,
                lastHitId: 0,
                loopGain: root.Obj.context.createGain()
            },
            $('.runners').append(C),
            $('.runners .runner').last().find('.track').val(`track ${v}`),
            $('.runners .runner').last()[0].dataset.id = v
    };

    //старт плей loops
    X = function() {//
        var a, c, d, e, f, scope_v = v;

        let isAddDynamic = root.Obj.project.loops[scope_v] !== undefined && !Object.keys(root.Obj.project.loops[scope_v].hits).length;
        let runnerIsActive = root.Obj.variable.runnerIsActive;
        let inNum = Number.isInteger(runnerIsActive);

        if (inNum) {
            scope_v = runnerIsActive;
        }

        !isAddDynamic && !inNum && ((v += 1) && (scope_v += 1));

        root.Obj.project.loops[scope_v] = {
            muted: false,
            hits: {},
            name: `track ${scope_v}`,
            lastHitId: 0,
            loopGain: root.Obj.context.createGain()
        };

        aa();
        r || V();

        $('.js-btn-record').addClass('active');
        s = true;
        J = new Date().getTime();

        !isAddDynamic && !inNum && $('.runners').append(C);
        !isAddDynamic && !inNum && ($('.runners .runner').last()[0].dataset.id = scope_v);
        !isAddDynamic && !inNum && $('.runners .runner').last().find('.track').val(`track ${scope_v}`);
        !isAddDynamic && !inNum && $('.runners .runner').last().append(CC2);
        $(".runner[data-id='" + scope_v + "']").append(CC2);

        for (f = [], d = 0, e = u.length; e > d; d++)
            (a = u[d]),
                (c = J - a.time),
                100 > c
                    ? (b(0, a.key), f.push(E(a.sound, 0)))
                    : f.push(void 0);

        return f;
    };

    //todo метроном
    E = function(a, b, c, d, e) {
        var f, h, i, j, k;
        return (
            (f = f || null),
                (b = b || 0),
                (c = c || false),
                c
                    ? ((k = root.Obj.context.createBufferSource()),
                        (i = root.Obj.project.loops[d].hits[e].hitGain),
                        (j = root.Obj.project.loops[d].loopGain),
                        root.Obj.sounds[a].sources.push(k),
                        k.connect(i),
                        i.connect(j),
                        j.connect(B),
                    'metronome' !== d &&
                    ((h = root.Obj.project.loops[d].hits[e]),
                        h.neverPlayed
                            ? ((i.gain.value = 0), (h.neverPlayed = false))
                            : h.isOverwritable === false && (h.isOverwritable = true)))
                    : (root.Obj.sounds[a].liveGainNode &&
                    'hat' !== a &&
                    (root.Obj.sounds[a].liveGainNode.gain.value = 0),
                        (k = root.Obj.context.createBufferSource()),
                        (root.Obj.sounds[a].liveGainNode = root.Obj.context.createGain()),
                        k.connect(root.Obj.sounds[a].liveGainNode),
                        root.Obj.sounds[a].liveGainNode.connect(B)),
            k.start || (k.start = k.noteOn),
                (k.buffer = root.Obj.sounds[a].buffer),
                (b = b / 1e3 + root.Obj.context.currentTime),
                B.connect(g),
                g.connect(root.Obj.context.destination),
                k.start(b)
        );
    };

    /*
    * DONE
    * */
    l = function(a) {
        var b = a / root.Obj.project.loopDuration * 100;
        $('.js-progress-bg').css({width: '' + b + '%'});
        $('.js-progress-line').css({left: '' + b + '%'});
        $(".js-play-time-text").empty()
        $(".js-play-time-text").append(A(a))
    };

    //todo метроном
    /**
     * DONE
     */
    L = function() {
        const loops = root.Obj.project.loops;
        N = [];
        Object.keys(loops).forEach(loopKey => {
            const hits = loops[loopKey].hits;
            Object.keys(hits).forEach(hitKey => {
                const hit = hits[hitKey];
                const time = hit.time - W;

                if (time >= 0) {
                    N.push({
                        time,
                        sound: hit.sound,
                        loopId: loopKey,
                        id: hitKey
                    });

                    E(hit.sound, time, true, loopKey, hitKey);
                }
            })
        });

        return N.sort((first, second) => first.time < second.time ? -1 : first.time > second.time ? 1 : 0);
    };

    /**
     * DONE
     */
    Y = function() {
        cancelAnimationFrame(ea);
        $('.js-btn-play-stop').removeClass('active');
        (W = 0);


        Object.keys(root.Obj.sounds).forEach(soundName => {
            const sound = root.Obj.sounds[soundName];

            sound.sources.forEach(source => {
                source.stop();
            });
            sound.sources = [];
        });
        if (!r) {
            l(0);
            $('.box-key-btn').removeClass('auto-active');
        }
    };

    /**
     * DONE
     */
    V = function() {
        $('.js-btn-play-stop').addClass('active');
        z = 1000 * root.Obj.context.currentTime;
        L();

        if (!r) {
            r = true;
            $('.js-progress-line').addClass('active');

            Object.keys(root.Obj.sounds).forEach(name => {
                root.Obj.sounds[name].liveGainNode ? root.Obj.sounds[name].liveGainNode.gain.value = 0 : null;
            })
        }
        return requestAnimFrame(F);
    };

    /**
     * DONE
     */
    ba = function(key) {
        cancelRequestAnimFrame(ca[key]);

        ca[key] = requestAnimFrame(function() {
            // var b;

            if ('metronome' !== key) {
                const keyButton = $(`.box-key-btn#${key}`);

                if (t[key]) {
                    clearTimeout(t[key]);
                }

                keyButton.addClass('auto-active');
                t[key] = setTimeout(() => keyButton.removeClass('auto-active'), 50);
            }

        });
    };

    D = function(a) {
        var b, c, d, e, f;
        q = true;
        c = a;

        if (0 === Object.keys(root.Obj.project.loops[v].hits).length) {
            return (q = false);
        }

        e = root.Obj.project.loops[v].hits;
        f = [];

        for (b in e) {
            if (((d = e[b]), !(d.time < c && d.isOverwritable))) {
                q = false;
                break;
            }
            0 === Object.keys(root.Obj.project.loops[v].hits).length
                ? f.push((q = false))
                : f.push(void 0);
        }
        return f;
    };

    A = function(a) {
        var b, c;
        return (
            (c = Math.round(a / 1e3)),
                (b = ((a / 1e3) % 1).toFixed(2).substring(2)),
            1 === c.toString().length && (c = '0' + c),
            1 === b.toString().length && (b = '0' + b),
                `${c}.<span>${b}</span>`
        );
    };

    //loop stop
    root.Obj.variable.StReq = 0;
    var startRecord = false;
    F = function() {
        var a, b, c, d, e, f, g, i;
        if (r) {
            for (h = 1e3 * root.Obj.context.currentTime - z; N.length && N[0].time < h && r;) {
                startRecord = true;



                if (root.Obj.variable.StReq > h && !playingTrack) {
                    startRecord = false
                    stopClearRecord();
                    r = false;
                    root.Obj.variable.runnerIsActive = false;
                    $('.runner').removeClass('active');
                    root.Obj.variable.StReq = 0;
                    return false;

                } else {
                     root.Obj.variable.StReq = h;

                    //step 1
                    e = null;
                    f = root.Obj.project.loops[N[0].loopId];

                    if ('metronome' !== N[0].loopId) {
                        if (f.hits[N[0].id] && f.hits[N[0].id].time !== N[0].time)
                            N.splice(0, 1);
                        else if (!f || f.muted || (S && S !== N[0].loopId))
                            N.splice(0, 1);
                        else {
                            g = N[0].sound;
                            a = root.Obj.soundsToGroups[g];
                            if (a) {
                                b = root.Obj.groupSounds[a];
                            }
                            i = f.hits;

                            //todo - this is for loop
                            for (d in i)
                                (c = i[d]),
                                c.time - W < N[0].time &&
                                (c.sound === g && 'hat' !== c.sound
                                    ? ((c.hitGain.gain.value = 0),
                                        (c.hitGain = root.Obj.context.createGain()))
                                    : b &&
                                    b.indexOf(c.sound) > -1 &&
                                    ((c.hitGain.gain.value = 0),
                                        (c.hitGain = root.Obj.context.createGain())));
                            'undefined' != typeof f.hits[N[0].id] &&
                            ba(root.Obj.soundsToKeys[g]),
                                N.splice(0, 1);
                        }
                    } else {
                        N.splice(0, 1);
                        if (f.muted) {
                            ba('metronome');
                        }
                    }

                }
            }
            return h >= root.Obj.project.loopDuration - W
                ? (Y(), V())
                : (l(h + W), (ea = requestAnimFrame(F)), s && !q ? D(h) : void 0);
        }
    };

    /*
    * DONE
    * */
    cleaningProject = function() {
        const { bpm, barCount, beatsPerBar } = root.Obj.project;
        Y();
        app.hideEdit();
        r = false;
        s = false;
        N = [];
        z = 0;
        J = 0;
        h = 0;

        root.Obj.project = {
            loops: {},
            bpm: bpm || 130,
            barCount: barCount || 4,
            beatsPerBar: beatsPerBar || 4
        };

        v = 0;
        S = null;
        O = true;
        U = true;
        l(0);

        $('body').removeClass('has-loops');
        $('.runners .runner').remove();
    };

    aa = function() {
        // $('.js-solo').removeClass('active');
        const loopList = root.Obj.project.loops;

        Object.keys(loopList).forEach(loopKey => {
            const loop = loopList[loopKey];

            if (!loop.muted) {
                if (loop.loopGain) {
                    loop.loopGain.gain.value = 1;
                }

                $(`.runner[data-id='${loopKey}']`).removeClass('muted')
            }
        });

        S = null;
    };

    b = function(a, b, c) {
        var d, e, f, g, h, i, j, scope_v = v;

        if (root.Obj.variable.StReq > a) {
            return false
        }

        let runnerIsActive = root.Obj.variable.runnerIsActive;

        if (Number.isInteger(runnerIsActive)) {
            scope_v = runnerIsActive;
        }

        return (
            (h = a % root.Obj.method.H),
                (d = root.Obj.method.H - h),
                d > h ? (a -= h) : (a += d),
                root.Obj.project.loops[scope_v].lastHitId++,
                (f = root.Obj.project.loops[scope_v].lastHitId),
                // (console.log('-> ', a)),
                (root.Obj.project.loops[scope_v].hits[f] = {
                    time: a,
                    sound: root.Obj.keys[b].sound,
                    color: root.Obj.keys[b].color,
                    url: root.Obj.keys[b].url,
                    hitGain: root.Obj.context.createGain(),
                    isOverwritable: false
                }),
            c && (root.Obj.project.loops[scope_v].hits[f].neverPlayed = true),
                (j = root.Obj.project.loops[scope_v].hits[f].sound),
                (b = root.Obj.soundsToKeys[j]),
                (g = root.Obj.keys[b].label),
                (i = a / root.Obj.project.loopDuration * 100),
                (e = "<div class='hit ' style='left: " + i + "%; background: " + root.Obj.keys[b].color + "' data-id='" + f + "'></div>"),
                $(".runner[data-id='" + scope_v + "']").append(e)
        );
    };

    window.onblur = function() {
        s && stopClearRecord();
        r && Y();
    };

    /*
    * DONE
    * */
    root.Obj.createProject = function(objSave) {
        let oneHit, itemLoop, itemHits, arrNewProject = [];
        const {id, name: title, save, style} = objSave;
        const {bpm, barCount, beatsPerBar, loops} = save;

        cleaningProject();
        root.Obj.project = {
            ...root.Obj.project,
            id,
            title,
            bpm,
            barCount,
            beatsPerBar,
            loopDuration: bpm / 60 * 4 * barCount,
            loops,
            styleId: style.id
        };

        v = save.lastLoopId;//global
        root.Obj.setBpm(bpm, barCount);

        $(".js-load-project[data-id='" + id + "']").addClass('active')

        for (let e in loops) {
            itemLoop = loops[e];

            if ('metronome' !== e) {
                itemLoop.loopGain = root.Obj.context.createGain();

                if (itemLoop.muted && e !== S) {
                    itemLoop.loopGain.gain.value = 0
                }

                itemHits = itemLoop.hits

                for (let c in itemHits) {
                    oneHit = itemHits[c]
                    oneHit.color = root.Obj.soundsToColor[oneHit.sound]
                    oneHit.hitGain = root.Obj.context.createGain()
                }

                arrNewProject.push(addNewProjectToLoop(e));
            }
        }

        return arrNewProject;
    };

    root.Obj.setBpm = function(a, b) {
        var c, d, e,  g, h = false, i, j, k;

        if (a !== root.Obj.project.bpm) {
            h = true;
            d = root.Obj.project.bpm / a;
            root.Obj.project.bpm = a;
            j = root.Obj.project.loops;


            for (let f in j) {
                i = j[f]

                console.log(f, j, '1')

                if ('metronome' !== f) {
                    k = i.hits;
                    for (f in k) {
                        i = k[f];
                        i.time = i.time * d
                    }
                }
            }

        }


        if(b !== root.Obj.project.barCount) {
            h = true;
            c = root.Obj.project.barCount / b;
            root.Obj.project.barCount = b;

            $('.hit').each(function() {
                var a = parseFloat(this.style.left);
                this.style.left = '' + a * c + '%';
            });
        }

        root.Obj.project.loopDuration = 1e3 * (60 / root.Obj.project.bpm * 4 * root.Obj.project.barCount);

        e = A(root.Obj.project.loopDuration);


        $('.js-spm, .js-set-bpm-button').text(root.Obj.project.bpm);
            $('.js-select-bar-count').removeClass('active');
            $(".js-select-bar-count[data-count='" + root.Obj.project.barCount + "']").addClass('active');
            $('.js-select-time-signature').removeClass('active');
            $(".js-select-time-signature[data-beats-per-bar='" + root.Obj.project.beatsPerBar + "']").addClass('active');
            $('.js-bar-count').text(`${root.Obj.project.barCount}bar`);

            (root.Obj.project.barCount > 1) && $('.js-bar-count-label').html('&nbsp;bars');
        (1 === root.Obj.project.barCount) && $('.js-bar-count-label').html('&nbsp;bar');

        $('.js-duration-text').empty().append(e);
            (g = (root.Obj.project.bpm - root.Obj.bpmStartValue) / root.Obj.bpmRange * 100);
            $('.js-set-bpm-button').css({left: '' + g + '%'});
            app.rebuildingControlAndLoop();



        if(h && Object.keys(root.Obj.project.loops).length > 1 && r) {
            Y();
            V();
        }
    };

    // todo -> startApplication
    /*
     * DONE
     * */
    x = function() {
        cleaningProject();
        root.Obj.setBpm(root.Obj.project.bpm, root.Obj.project.barCount);
    };

    root.Obj.method = {
        ...root.Obj.method,
        stopClearRecord: stopClearRecord,
        addNewProjectToLoop,
        addNewLoop
    };
    window.methodDev = {
        forStartApp_x: x,
        forStartApp_T: T
    };

    //todo name Remove one loop
    /*
     * DONE
     * */
    j = function() {

        if (y) {
            $(".runner[data-id='" + y + "']").remove();
            if (root.Obj.project.loops[y].loopGain) {
                root.Obj.project.loops[y].loopGain.gain.value = 0;
            }

            delete root.Obj.project.loops[y];

            if (S === y) {
                aa();
            }

            if (0 === $('.runner').length) {
                root.Obj.launchNewProject()
                root.Obj.variable.runnerIsActive = false;
                $('.runner').removeClass('active');
            }

            y = null
        }

    };

    /*
     * DONE
     * */
    RemoveOllLoop = function() {
        $('.runner').map((inx, el) => {

            const { id } = el.dataset;

            if (id) {
                $(".runner[data-id='" + id + "']").remove();
                if (root.Obj.project.loops[id].loopGain) {
                    root.Obj.project.loops[id].loopGain.gain.value = 0;
                }

                delete root.Obj.project.loops[id];
                S === id && aa();
            }
        });
        root.Obj.launchNewProject()

    };

    root.Obj.method['RemoveOllLoop'] = RemoveOllLoop;

    /*
     * DONE
     * */
    root.Obj.launchNewProject = function() {
        cleaningProject();
        root.Obj.setBpm(root.Obj.project.bpm, root.Obj.project.barCount);
        app.toggleMetronome(true)
    };

    //EVENT FUNC
    /*
     * DONE
     * */
    btnPlayStop = function() {
        if (r === false) {
            playingTrack = true;
            V();
        } else {
            playingTrack = false;
            clearTimeout(f);
            r = false;
            Y();
            root.Obj.variable.StReq = 0;
            if (s) {
                stopClearRecord();
            }
        }
    };

    //EVENT FUNC
    /*
     * DONE
     * */
    btnRecord = function() {
        if (!s) {

            root.Obj.variable.runnerIsActive && root.Obj.method.clearProject();
            X();
        } else {
            $('.runner').removeClass('active');
            root.Obj.variable.runnerIsActive = false;
            root.Obj.variable.StReq = 0;
            stopClearRecord();
            r = false;
        }
    };

    removeHit = function(a) {
        if(!a) return false;

        root.Obj.project.loops[a.loopId].hits[a.id].hitGain.gain.value = 0;
        delete root.Obj.project.loops[a.loopId].hits[a.id];
        $(".runner[data-id='" + a.loopId + "'] .hit[data-id='" + a.id + "']").remove();
        a = null;

        root.Obj.saveRecording()
    };

    $(function() {

        $(document).on('keyup', function(a) {
            let b, c, e, f;

            // console.log('a.keyCode', a.keyCode)// 191
            // console.log('a.originalEvent.code', a.originalEvent.code)// Slash
            // console.log('a.which', a.which)// 191
            // console.log('a.charCode', a.charCode)// 0


            // if (
            //     m = c.originalEvent.code,
            // m || (m = c.which || c.charCode,
            // 173 === m && (m = 189),
            // 61 === m && (m = 187),
            // 59 === m && (m = 186),
            //     m = samp.keyCodeToKeyName[m]),
            //     $("input").is(":focus"))


            e = new Date().getTime();
            f = e - J;
            c = app.getKeyName(a.keyCode);

            if ($('body').hasClass('start')) {
                if (a.keyCode == 32 && !$('body').hasClass('no-keyboard')) {
                    btnPlayStop()
                }

                if (a.keyCode == 13 && !$('body').hasClass('no-keyboard')) {
                    btnRecord()
                }
            }

            if (!c) {

                c = a.which || a.charCode
                173 === c && (c = 189)
                61 === c && (c = 187)
                59 === c && (c = 186)

                c = root.Obj.keyboardMap[c]

            }

            // b = $(`.box-key-btn.active`);
            b = $(`.box-key-btn#${c}`);

            if (b.hasClass('active')) {
                b.removeClass('active')
            } else if($(`.box-key-btn.active`)) {
                $(`.box-key-btn.active`).removeClass('active')
            }

        });

        $('.box-key-btn').mousedown(function() {
            var a, c, d, e, f, g, i, j, k, l, m, n;

            if ($(this).hasClass('disabled')) {
                return false;
            }

            if (
                ((c = $(this)), (f = c[0].id), (j = root.Obj.keys[f].sound),
                    (d = root.Obj.keys[f].group),
                    (a = root.Obj.keys[f].color),
                    !c.hasClass('active'))
            ) {
                if (root.Obj.sounds[j]) {
                    if (
                        (c.addClass('active'),
                            (e = {
                                time: new Date().getTime(),
                                key: f,
                                sound: j
                            }),
                            u.push(e),
                            (u = u.slice(0, 3)),
                        d && !o)
                    )
                        for (n = root.Obj.groupSounds[d], l = 0, m = n.length; m > l; l++)
                            (g = n[l]),
                            root.Obj.sounds[g].liveGainNode &&
                            (root.Obj.sounds[g].liveGainNode.gain.value = 0);
                    E(j, 0),
                    s &&
                    ((k = h + W),
                        (i = false),
                        50 > k
                            ? (k = 0)
                            : k + 100 > root.Obj.project.loopDuration &&
                            ((i = true), (k = 0)),
                        b(k, f, i));
                }
            }
        });

        $(document).on('mousedown', '.js-delete-project-button', function() {
            $(document).off('mouseleave', '.header-content');
            clearTimeout(m);
        });

        $(document).on('mousedown', '.js-select-bar-count', function() {
            var a;

            $('.js-select-bar-count').removeClass('active');
            $(this).addClass('active');
            a = parseInt($(this).text());
            root.Obj.setBpm(root.Obj.project.bpm, a);
        });

        $(document).on('mousedown', '.js-btn-record', () => {
            btnRecord()
        });

        $(document).on('mousedown', '.js-btn-play-stop', btnPlayStop);

        $(document).on('mousedown', '.js-delete-loop-prompt', function() {
            y = $(this).parents('.runner').first()[0].dataset.id;
            j();
        });

        $(document).on('mousedown', '.js-mute', function() {
            var a, b, c;
            aa();
            a = $(this).parents('.runner').first();
            b = a[0].dataset.id;
            c = a.find('.js-show-actions');
            $(this).toggleClass('active');

            if (root.Obj.project.loops[b].muted) {
                c.removeClass('active');
                a.removeClass('muted');
                root.Obj.project.loops[b].muted = false;
                root.Obj.project.loops[b].loopGain.gain.value = 1;
            } else {
                c.addClass('active');
                a.addClass('muted');
                root.Obj.project.loops[b].muted = true;
                root.Obj.project.loops[b].loopGain.gain.value = 0;
            }
        });

        $(document).on('mousedown', 'html', function(event) {
            var c, d, e;

            if (
                !$(event.target).hasClass('hit') &&
                (a && ($('.hit').removeClass('active'), (a = null)),
                $(event.target).hasClass('runner') ||
                $(event.target).hasClass('runners') ||
                $(event.target).hasClass('progress-bg') ||
                $(event.target).hasClass('recording-progress'))
            ) {
                if (((e = $('.runners').outerWidth()), (c = event.pageX - $('.runners').offset().left), (d = c / e), 24 > e - c))
                    return;
                return (
                    $('.js-progress-line').addClass('active'),
                        r
                            ? (Y(),
                                (W = parseInt(root.Obj.project.loopDuration * d)),
                                l(W),
                                V())
                            : ((W = parseInt(root.Obj.project.loopDuration * d)), l(W))
                );
            }
        });

        /*
        * DONE
        * */
        $(document).on('mousedown', '.hit', function(b) {

            var $cloneSelectHit,
                nextStepAfterCloneSelect,
                lastHitId,
                $runner = $(this).parent(),
                runnerDataId = $runner[0].dataset.id,
                i = $('.runners').outerWidth() - 8;

            //ALT copy
            if (b.altKey) {
                root.Obj.project.loops[runnerDataId].lastHitId++;
                lastHitId = root.Obj.project.loops[runnerDataId].lastHitId;
                $cloneSelectHit = $(this).clone();
                $cloneSelectHit.attr('data-id', lastHitId).appendTo($runner);
                const {
                    time,
                    sound,
                    color,
                    url
                } = root.Obj.project.loops[runnerDataId].hits[this.dataset.id];

                root.Obj.project.loops[runnerDataId].hits[lastHitId] = {
                    time,
                    sound,
                    color,
                    url,
                    hitGain: root.Obj.context.createGain(),
                    isOverwritable: false
                }
            } else {
                $cloneSelectHit = this;
                lastHitId = $cloneSelectHit.dataset.id;
            }

            nextStepAfterCloneSelect = root.Obj.project.loops[runnerDataId].hits[lastHitId];
            nextStepAfterCloneSelect.hitGain.gain.value = 0;

            a = {
                loopId: runnerDataId,
                id: lastHitId
            };

            $('.hit').removeClass('active dragging');
            $($cloneSelectHit).addClass('active dragging');

            $(document).on('mousemove', function(event) {
                var b, d, percentForHit, f, g;

                let evPageX = event.pageX - 200;
                let winInnerWind = window.innerWidth - 200;

                if (evPageX < 0) {
                    event.pageX = 0
                } else {
                    (evPageX > winInnerWind) && (event.pageX = winInnerWind) && (evPageX = event.pageX - 200);

                    $(document).off('mouseleave');

                    percentForHit = evPageX / winInnerWind * 100;
                    g = root.Obj.method.H / root.Obj.project.loopDuration * 100;
                    d = percentForHit % g;
                    b = g - d;

                    b > d ? (percentForHit -= d) : (percentForHit += b);

                    f = 100 - g;

                    percentForHit > f && (percentForHit = f);
                    $($cloneSelectHit).css({left: '' + percentForHit + '%'})
                }
            });

            $(document).on('mouseup', function(a) {
                var b, f, i;
                $($cloneSelectHit).removeClass('dragging');
                f = parseFloat($($cloneSelectHit)[0].style.left) / 100;
                b = f * root.Obj.project.loopDuration;
                $(document).off('mousemove');
                $(document).off('mouseup');

                if (nextStepAfterCloneSelect.time === b) {
                    nextStepAfterCloneSelect.hitGain.gain.value = 1
                } else {
                    nextStepAfterCloneSelect.time = b;
                    nextStepAfterCloneSelect.hitGain = root.Obj.context.createGain();
                    nextStepAfterCloneSelect.hitGain.gain.value = 1;
                    i = b - h;

                    if (i > 0) {

                        r && !root.Obj.project.loops[runnerDataId].muted && E(nextStepAfterCloneSelect.sound, i);

                        N.push({
                            time: b,
                            sound: nextStepAfterCloneSelect.sound,
                            loopId: runnerDataId,
                            id: lastHitId
                        });

                        N.sort(function(a, b) {
                            return a.time < b.time ? -1 : a.time > b.time ? 1 : 0;
                        })
                    }
                }
            });
        });

        $(document).on('mouseenter', '.header-content', function() {
            $('.js-title-input-wrap').hasClass('animate-in')
                ? ($(document).off('mouseleave', '.header-content'), void clearTimeout(m))
                : root.Obj.project.title && root.Obj.project.title.length
                ? ($(document).off('mouseleave', '.header-content'),
                    clearTimeout(m),
                    $('.js-edit-buttons').show(),
                    getComputedStyle($('.js-edit-buttons')[0]).opacity,
                    $('.js-title-wrap').addClass('animate-up'),
                    $('.js-edit-buttons').addClass('animate-up'),
                    $(document).on('mouseleave', '.header-content', function() {
                        return app.hideEditButtons();
                    }))
                : undefined
        });

        $(document).on("keydown", function(c) {
            let
                //d,
                g,
               // k,
                l, m,
              //  n,
                p, q, t;
                //, w, x, z;
            let keyCode = c.keyCode;

            if (!$('body').hasClass('no-keyboard')) {

                // m = c.originalEvent.code;
                //
                // m || (
                //
                //     m = c.which || c.charCode,
                //     173 === m && (m = 189),
                //     61 === m && (m = 187),
                //     59 === m && (m = 186),
                //         m = app.getKeyName(c.keyCode)
                // );

                // let originalEventCode = c.originalEvent.code;

                switch (c.originalEvent.code) {
                    case 'Backslash':
                        keyCode = (keyCode === 220 || keyCode === 222) ? '' : keyCode
                        break;
                    case 'Slash':
                        keyCode = keyCode === 191  ? keyCode : 191;
                        break;
                    case 'Minus':
                        keyCode = keyCode === 189  ? keyCode : 189;
                        break;
                    case 'Equal':
                        keyCode = keyCode === 187  ? keyCode : 187;
                        break;
                    case "Backspace":
                        removeHit(a);
                        break;
                }


                m = app.getKeyName(keyCode)

                // console.log('a.keyCode', c.keyCode)// 191
                // console.log('a.originalEvent.code', c.originalEvent.code)// Slash
                // console.log('a.which', c.which)// 191
                // console.log('a.charCode', c.charCode)// 0
                //
                // console.log(app.getKeyName(c.keyCode))

                c.preventDefault();

                if (root.Obj.keys[m])
                    if (g = $(`.box-key-btn#${m}`),
                        q = root.Obj.keys[m].sound,
                        g.hasClass("active"))
                        ;
                    else if (root.Obj.sounds[q]) {
                        if (g.addClass("active"),
                            l = {
                                time: (new Date).getTime(),
                                key: m,
                                sound: q
                            },
                            u.push(l),
                            u = u.slice(0, 3)

                        )
                            if (E(q, 0), s) {
                                return t = h + W,
                                    p = false,
                                    50 > t ? t = 0 : t + 100 > root.Obj.project.loopDuration && (p = true, t = 0),
                                    b(t, m, p)
                            }

                    }

            }

        });

        // $(document).on("keyup", function(a) {
        //     var b, c, e, f;
        //     return e = (new Date).getTime(),
        //         f = e - 0,
        //         c = a.originalEvent.code,
        //     c || (c = a.which || a.charCode,
        //     173 === c && (c = 189),
        //     61 === c && (c = 187),
        //     59 === c && (c = 186),
        //         c = root.Obj.keyboardMap[c]),
        //         "ArrowLeft" === c ? (P = true,
        //             void (d = 260)) : "Space" === c ? void (U = true) : (b = $(`.box-key-btn#${c}`),
        //             b ? b.removeClass("active") : void 0)
        // });
    });
}
