
export default function initSelector(clsSelect) {

        var classes = $(`${clsSelect}`).attr("class"),
            id      = $(`${clsSelect}`).attr("id"),
            name    = $(`${clsSelect}`).attr("name");

        var template =  '<div class="' + classes + '">';
        template += `<span  class="custom-select-trigger">` + $(`${clsSelect}`).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(`${clsSelect}`).find("option").each(function() {
            template += '<span data-id='+$(this).data("id")+' class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
        template += '</div></div>';

        $(`${clsSelect}`).wrap('<div class="custom-select-wrapper"></div>');
        $(`${clsSelect}`).hide();
        $(`${clsSelect}`).after(template);


    $(".custom-option:first-of-type").hover(function() {
        $(this).parents(".custom-options").addClass("option-hover");
    }, function() {
        $(this).parents(".custom-options").removeClass("option-hover");
    });

    $(".custom-select-trigger").on("click", function() {
        $('html').one('click',function() {
            $(`${clsSelect}`).removeClass("opened");
        });
        $(this).parents(`${clsSelect}`).toggleClass("opened");
        event.stopPropagation();
    });

    $(".custom-option").on("click", function() {
        $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
        $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(`${clsSelect}`).removeClass("opened");
        $(this).parents(`${clsSelect}`).find(".custom-select-trigger").text($(this).text());
    });

    $(this).addClass('initSelector');



}


export function changeSelector(clsSelect, id) {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(`${clsSelect}`).removeClass("opened");
    $(this).parents(`${clsSelect}`).find(".custom-select-trigger").text($(this).text());
}
