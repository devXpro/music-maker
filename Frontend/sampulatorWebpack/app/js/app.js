import {Event} from './shared/constEvent';
import root from './shared/root';

export default class app {
    constructor() {
        this._addEventListeners();
        this._initTool();
    }

    _initTool() {

        window.cancelRequestAnimFrame = (
            window.cancelAnimationFrame ||
            window.webkitCancelRequestAnimationFrame ||
            window.mozCancelRequestAnimationFrame ||
            window.oCancelRequestAnimationFrame ||
            window.msCancelRequestAnimationFrame ||
            clearTimeout
        );

        window.requestAnimFrame = (
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (a) {
                return window.setTimeout(a, 1e3 / 60);
            }
        );
    }

    _addEventListeners() {

        $(document)
            .on(Event.MOUSEDOWN, '.js-toggle-metronome', function() {
                $(this).toggleClass('active');
                app.toggleMetronome($(this).hasClass('active'));
            })

            .on(Event.MOUSEDOWN, '.js-toggle-hold', function() {
                return $(this).toggleClass('active');
            })

            .on(Event.MOUSEDOWN, '.js-new-project', () => {
                return root.Obj.launchNewProject();
            })

            .on(Event.MOUSEDOWN, '.js-confirm-delete-button', function() {
                $(this).addClass('loading');
                root.Obj.api.deleteProject();
            })

            .on(Event.FOCUS, 'input',  function() {
                return $(this).parents('.flex-input').addClass('focus');
            })
            .on(Event.BLUR, 'input',  function() {
                return $(this).parents('.flex-input').removeClass('focus');
            });
        $(".mixer-board").mouseup(function() {
            return $(".box-key-btn").removeClass("active")
        })
    }

    static hideEdit() {
        $('.js-title-input-wrap').addClass('animate-out');
        $('.js-save-button').removeClass('loading');

        setTimeout(function () {
            $('.js-title-input-wrap').hide().removeClass('animate-in animate-out');
            $('.js-title-wrap').show();
            $('.js-title-wrap').addClass('active')
        }, 300);
    };

    static toggleMetronome(a) {
        if (a) {
            $('.js-toggle-metronome').addClass('active');
            root.Obj.project.loops.metronome.muted = false;
            root.Obj.project.loops.metronome.loopGain.gain.value = 1;
        } else {
            $('.js-toggle-metronome').removeClass('active');
            root.Obj.project.loops.metronome.muted = true;
            root.Obj.project.loops.metronome.loopGain.gain.value = 0;
        }
    };

    static hideEditButtons() {
        $('.js-title-wrap').removeClass('animate-up');
        $('.js-edit-buttons').removeClass('animate-up');
        m = setTimeout(function () {
            return $('.js-edit-buttons').hide();
        }, 400);
    };

    static rebuildingControlAndLoop() {
        var a, b, c, d, e, f, g, h, i;
        for (
            b = root.Obj.project.beatsPerBar * root.Obj.project.barCount,
                c = 60 / root.Obj.project.bpm * 1e3,
                root.Obj.method.H = c / 8,
            8 === root.Obj.project.beatsPerBar && (c /= 2),
                e = 0,
                $('.beat-line').remove(),
                f = !0,
            $('.js-toggle-metronome').hasClass('active') && (f = !1),
            root.Obj.project.loops.metronome ||
            ((root.Obj.project.loops.metronome = {
                muted: f,
                hits: {},
                lastHitId: 0,
                loopGain: root.Obj.context.createGain()
            }),
            f && (root.Obj.project.loops.metronome.loopGain.gain.value = 0)),
                i = [],
                h = 1;
            b >= 1 ? b >= h : h >= b;
            b >= 1 ? h++ : h--
        )
            e > 0 &&
            ((a =
                e % root.Obj.project.beatsPerBar ? 'beat-line' : 'beat-line major'),
                (d = $("<div class='" + a + "' style='margin-left:" + e * (100 / b) + "%'></div>")),
                 $('.runners').append(d)
            ),
            (g = e % root.Obj.project.beatsPerBar ? 'metronome' : 'metronomeUp'),
                (root.Obj.project.loops.metronome.hits[e] = {
                    time: e * c,
                    sound: g,
                    color: null,
                    hitGain: root.Obj.context.createGain()
                }),
                i.push(e++);
        return i;
    }

    static renderingBTN(inxAllBoxKey, allBoxKey) {
        let idItemBoxKey, itemBoxKey, btnItemBoxKey;

        itemBoxKey = $(allBoxKey[inxAllBoxKey]);
        btnItemBoxKey = itemBoxKey.find('button');
        idItemBoxKey = itemBoxKey[0].id;
        inxAllBoxKey += 1;
        if (root.Obj.keys[idItemBoxKey]) {
            btnItemBoxKey
                .empty()
                .text(root.Obj.keys[idItemBoxKey].label)
                .css("color", root.Obj.keys[idItemBoxKey].fontColor)
                .css("background", root.Obj.keys[idItemBoxKey].color)
                .prop('disabled', false);

        } else if(!itemBoxKey.hasClass('disabled')) {
            btnItemBoxKey
                .empty()
                .prop('disabled', true)
                .removeAttr('style');
        }

        if (!(inxAllBoxKey > allBoxKey.length - 1)) {
            setTimeout(()=>{app.renderingBTN(inxAllBoxKey, allBoxKey)},20)
        }

        $('.js-mixer-controls').removeClass('disable')

    }

    static getKeyName(keyCode) {
        return root.Obj.keyboardMap[keyCode]
    }

    static startApplication() {

        $('body').addClass('start');

        let bufferLoader = new BufferLoader(root.Obj.context, root.Obj.urlList, function (a) {
            let key, inxAllBoxKey, allBoxKey, f;
            for (key in a) {
                f = a[key];
                root.Obj.sounds[key] = {};
                root.Obj.sounds[key].sources = [];
                root.Obj.sounds[key].buffer = f.buffer
            }

            //todo <- internal
            window.methodDev.forStartApp_x();

            $('.loops .loader').hide();
            //todo IG REMOVE
            $('.js-toggle-metronome').prop( "disabled", false );
            app.toggleMetronome(true);
            window.methodDev.forStartApp_T = false;//TODO always falls ??
            allBoxKey = $('.box-key-btn');

            inxAllBoxKey = 0;

            app.renderingBTN(inxAllBoxKey, allBoxKey);

        });

        bufferLoader.load();
    }

    static init(config) {
        return new app(config);
    }
}
