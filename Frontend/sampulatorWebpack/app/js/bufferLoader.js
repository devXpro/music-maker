export default function bufferLoader() {
    window.BufferLoader = function(context, urlList, onload) {
        this.context = context;
        this.urlList = urlList;
        this.onload = onload;
        this.bufferList = {};
        this.loadCount = 0
    };

    BufferLoader.prototype.loadBuffer = function(name, url) {
        var self = this,
            xhr;

        xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';

        xhr.onload = function() {
            self.context.decodeAudioData(xhr.response, function(buffer) {
                if (buffer) {
                    self.bufferList[name] = {};
                    self.bufferList[name].buffer = buffer;

                    if (++self.loadCount === Object.keys(self.urlList).length) {
                        self.onload(self.bufferList)
                    }
                } else {
                    console.warn('error decoding file data: ' + url);
                }
            });
        };

        xhr.onerror = function() {
            console.log('Could not load sounds. Either your internet connection has dropped or our server is down.')
        };
        xhr.send();
    };

    BufferLoader.prototype.load = function() {
        var name, url;
        for (name in this.urlList) {
            url = this.urlList[name];
            this.loadBuffer(name, url);
        }
    };
}

