import app from '../app';
import httpClient from '../sampApi';
import { Event } from '../shared/constEvent'
import { APIURL } from '../../config';
import root from '../shared/root';
import "../plugins/stepper";
import initSelector from "../plugins/select";
import 'select2/dist/css/select2.css';

export default class Classified {
    constructor() {
        this.inx = 0;
        this.jsTimeSigCurrentValue = 4;
        this.jsBarsCurrentValue = 4;
        this.jsBpmCurrentValue = 130;
        this.initStyleList();
        this._addEventListeners();
        this._initTool();

        // $.magnificPopup.open({
        //     items: {
        //         src: `#name-popup`
        //     },
        //     removalDelay: 300,
        //     mainClass: 'mfp-zoom-in',
        //     callbacks: {
        //         open: function() {
        //             $('body').addClass('no-keyboard');
        //
        //             setTimeout(() => {
        //                 typeof openCallback === 'function' && openCallback()
        //             }, 300)
        //
        //         },
        //         close: function() {
        //             $('body').removeClass('no-keyboard');
        //
        //             setTimeout(() => {
        //                 typeof closeCallback === 'function' && closeCallback()
        //             }, 300)
        //
        //         }
        //     },
        //     midClick: true
        // });
    }

    initStyleList(){
        httpClient.getStyleList().then(respons => {
            let htmlResp = respons.map(style =>`<option value="${style.id}">${style.name}</option>`).join('');
            $('.js-inp-style .placeholder').after(htmlResp)
        })
    }

    _addEventListeners() {
        const self = this;

        $(document)
            .on('click', '.js-track', function() {
                $('body').addClass('no-keyboard');
                $(this).prop('readonly', '');
                $(this).focus();
            })
            .on('blur', '.js-track', function() {
                $('body').removeClass('no-keyboard');
                $(this).prop('readonly', 'readonly');

                root.Obj.project.loops[
                    $(this).closest('.runner').data('id')
                    ].name = $(this).val();
            })
            .on('click', '.js-record-active', function() {
                let $runner = $(this).closest('.runner');

                if ($runner.hasClass('active')) {
                    $runner.removeClass('active');
                    root.Obj.variable.runnerIsActive = false;
                } else {
                    $('.runner').removeClass('active');
                    $runner.addClass('active');
                    root.Obj.variable.runnerIsActive = +$runner.data('id');
                }

                root.Obj.method.stopClearRecord();
                root.Obj.variable.StReq = 0;

            })
            .on('click', '.js-btn-clear', function(e) {
                root.Obj.method.RemoveOllLoop()
            })
            .on('click', '.js-login', function() {

                startAjaxRequest({
                    "username": $('.js-inp-login').val(),
                    "password": $('.js-inp-password').val()
                })
            })
            .on('click', '.js-name-btn', function(e) {
                let name = $('.js-inp-name').val();
                let styleId = $('.js-inp-style').val();
                if (name) {
                    root.Obj.project.title = name;
                    root.Obj.project.styleId = styleId;
                    root.Obj.saveRecording('POST');
                    $.magnificPopup.close();
                }
            })
            .on('click', '.js-info-btn', function(e) {
                let dateOfBirth = root.personInfo.dateOfBirth;
                let city = root.personInfo.city;
                let askProfileData = $('.js-not-ask').is(":checked");
                httpClient.saveProfile((response)=>{
                    console.log('111', response)
                }, {
                    dateOfBirth,
                    city,
                    askProfileData
                });
                $.magnificPopup.close();
            })
            .on('click', '.js-add-loop', function() {
                root.Obj.method.addNewLoop()
                $('.box-runners').scrollTop( $('.js-runner').height() )
            })
            .on('click', '.js-btn-save', function() {

                const {
                    city,
                    dateOfBirth,
                    askProfileData
                } = root.personInfo;
                if(!askProfileData || !(city || dateOfBirth)) {
                    self.openPopUp('info-popup', '', ()=>{
                        !root.Obj.project.title && self.openPopUp('name-popup')
                    })
                } else if (!root.Obj.project.title) {
                    self.openPopUp('name-popup', ()=>{}, ()=>{})
                } else {
                    root.Obj.saveRecording('PUT');
                }
            })
            .on('click', '.js-btn-download', function() {
                const id = root.Obj.edit.preset_id;


                httpClient.getSamplePackById(id, function(filePath) {
                    const a = document.createElement("a");
                    a.href = APIURL + '/' + filePath;
                    a.download = 'true';
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function() {
                        document.body.removeChild(a);
                    }, 0);
                })
            })
            .on(Event.CLICK, '.js-preset-btn', function(e) {
                let presetId = +$(e.currentTarget).attr('data-id');
                $('.js-preset-btn').removeClass('active');
                $(e.currentTarget).addClass('active');

                httpClient.loadPresetItem(presetId, (response) => {
                    {

                        if (!self.inx) root.Obj.method.initApplication(response);
                        if (self.inx) root.Obj.method.rebuildData(response);

                        $('#classified').removeClass('composition');
                        $('.select-box-h1').text(``).hide();

                        if(!root.Obj.edit) {
                            root.Obj.edit = {
                                preset_id: presetId
                            }
                        } else {
                            root.Obj.edit.preset_id = presetId;
                        }


                        self.inx++
                    }
                })
            });


        $('.js-stepper')
            .on('focus', function(e) {
            $('body').addClass('no-keyboard');
        })
        //     .bind('input', function(e) {
        //
        //         let min = +$(this).attr('min');
        //         let max = +$(this).attr('max');
        //         let val = +$(this).val();
        //         let isType = $(this).attr('isType');
        //
        //
        //
        //     switch (isType) {
        //         case 'inpBpm':
        //             // self.inpBpm(val, this, max, min);
        //             break;
        //         case 'inpTimeSig':
        //             // root.Obj.project.beatsPerBar = this.val;
        //             // app.rebuildingControlAndLoop();
        //             break;
        //         case 'inpBars':
        //             // self.inpBars(val, this, max, min);
        //             break;
        //         default:
        //             break
        //     }
        // })
            .on('blur', function() {
                $('body').removeClass('no-keyboard');

                let min = +$(this).attr('min');
                let max = +$(this).attr('max');
                let val = +$(this).val();

                let isType = $(this).attr('isType');
                isType === 'inpBpm' && self.inpBpmBlur(val, this, max, min);
                isType === 'inpBars' && self.inpBarsBlur(val, this, max, min);
                isType === 'inpTimeSig' && self.inpTimeSigBlur(val, this, max, min);
            });



        function startAjaxRequest(body) {
            httpClient.login(body, (response) => {
                {
                    localStorage.setItem('classified', response.token);
                    console.log('$(\'#popup\').hide();')
                    $.magnificPopup.close()
                }
            })
        }

    }

    inpBpmBlur(val, _this, max, min) {

        if (val < min) {
            $(_this).val(min)
        } else if (val > max) {
            $(_this).val(max)
        }

        this.jsBpmCurrentValue = $(_this).val();
        root.Obj.setBpm(this.jsBpmCurrentValue, this.jsBarsCurrentValue);
    }

    inpTimeSigBlur(val, _this, max, min){
        if (val < min) {
            $(_this).val(min)
        } else if (val > max) {
            $(_this).val(max)
        }

        this.jsTimeSigCurrentValue = $(_this).val();
        root.Obj.project.beatsPerBar = this.jsTimeSigCurrentValue;
        app.rebuildingControlAndLoop();
    }

    inpBarsBlur(val, _this, max, min) {

        if (val <= 2) {
            $(_this).val(min)
        } else if (val <= 4) {
            $(_this).val(4)
        }  else if (val <= 8) {
            $(_this).val(8)
        }  else if (val <= 16) {
            $(_this).val(16)
        }  else if (val > 16) {
            $(_this).val(16)
        }

        this.jsBarsCurrentValue = $(_this).val();
        root.Obj.setBpm(this.jsBpmCurrentValue, this.jsBarsCurrentValue);
    }

    _initTool() {
        const self = this;

        $('.js-toggle-metronome').prop("disabled", true);

        $('#popup').magnificPopup({
            delegate: 'a',
            removalDelay: 500,
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                },
                open: function() {
                    $('body').addClass('no-keyboard')
                },
                close: function() {
                    $('body').removeClass('no-keyboard')
                }
            },
            midClick: true
        });

        $('.js-name-city').select2({
            width: '100%',
            dropdownParent: $("#info-popup"),
            placeholder: 'Select',
            language: {
                inputTooShort: function() {
                    return "Enter 3 Character";
                },
                inputTooLong: function() {
                    return "You typed too much";
                },
                errorLoading: function() {
                    return "Error loading results";
                },
                loadingMore: function() {
                    return "Loading more results";
                },
                noResults: function() {
                    return "No results found";
                },
                searching: function() {
                    return "Searching...";
                },
                maximumSelected: function() {
                    return "Error loading results";
                }
            },
            templateSelection: function(state){
                root.personInfo = {
                    ...root.personInfo,
                    city: state.text
                };
                return state.text
            },
            ajax: {
                url: 'https://api.sampler.coderoom.pro/api/user/geo?callback=?',
                data: function (params) {
                    var q = {
                        q: params.term
                    }
                    return q;
                },
                delay: 250,
                processResults: function(data) {
                    var newData = JSON.parse(data.replace(/\?\(/g, '').replace(/\)\;/g, ''));

                    const results = Object.keys(newData).map((el, inx) => {
                        return newData[inx] ? {
                                id: inx,
                                text: newData[inx]
                            }
                            : {}
                    });

                    return {
                        results: self._isEmpty(results[0]) ? [] : results
                    };
                },
            },
            minimumInputLength: 3
        });


        $('.js-inp-style').select2({
            width: '100%',
            dropdownParent: $("#name-popup"),
            placeholder: 'Select',
            language: {
                inputTooShort: function() {
                    return "Enter 3 Character";
                },
                inputTooLong: function() {
                    return "You typed too much";
                },
                errorLoading: function() {
                    return "Error loading results";
                },
                loadingMore: function() {
                    return "Loading more results";
                },
                noResults: function() {
                    return "No results found";
                },
                searching: function() {
                    return "Searching...";
                },
                maximumSelected: function() {
                    return "Error loading results";
                }
            },
            // templateSelection: function(state){
            //     root.personInfo = {
            //         ...root.personInfo,
            //         city: state.text
            //     };
            //     return state.text
            // },
            // ajax: {
            //     url: 'https://api.sampler.coderoom.pro/api/user/geo?callback=?',
            //     data: function (params) {
            //         var q = {
            //             q: params.term
            //         }
            //         return q;
            //     },
            //     delay: 250,
            //     processResults: function(data) {
            //         var newData = JSON.parse(data.replace(/\?\(/g, '').replace(/\)\;/g, ''));
            //
            //         const results = Object.keys(newData).map((el, inx) => {
            //             return newData[inx] ? {
            //                     id: inx,
            //                     text: newData[inx]
            //                 }
            //                 : {}
            //         });
            //
            //         return {
            //             results: self._isEmpty(results[0]) ? [] : results
            //         };
            //     },
            // },
            // minimumInputLength: 3
        });



            $(".js-stepper").stepper({
                customClass: "", // Class applied to instance
                labels: {
                    up: "Up", // Up arrow label
                    down: "Down" // Down arrow label
                }
            });



        const param = window.location.search;

        if ( param ) {
            let {presetId, projectId} = this.getUrlVars();

            if (Number.isInteger(+presetId) && Number.isInteger(+projectId)) {

                $('#classified').addClass('composition')
                this.startAjaxRequest()

                root.Obj = {
                    ...root.Obj,
                    edit: {}
                };

                root.Obj.edit["preset_id"] = presetId;
                root.Obj.edit["projectId"] = projectId;

                httpClient.loadPresetItem(presetId, (response) => {
                    {
                        if (!self.inx) root.Obj.method.initApplication(response);
                        self.inx++;

                        httpClient.loadProject(projectId, (response) => {
                            {
                                $('.select-box-h1').text(`Edit ${response.name}`).show();
                                setTimeout(()=>{ root.Obj.createProject(response) }, 1000);
                            }
                        });
                    }
                })
            } else if(Number.isInteger(+presetId) && projectId === undefined) {
                root.Obj = {
                    ...root.Obj,
                    edit: {}
                };

                root.Obj.edit["preset_id"] = presetId;
                $('#classified').addClass('composition')
                this.startAjaxRequest()

                httpClient.loadPresetItem(presetId, (response) => {
                    {
                        if (!self.inx) root.Obj.method.initApplication(response);

                        self.inx++;

                        httpClient.loadPresetList((response)=>{{

                            response.map((resp)=> {{
                                if (resp.id === +presetId) {
                                    $('.select-box-h1').text(`${resp.name ? resp.name : ''}`).show();
                                }
                            }});
                        }})
                    }
                })
            }
        } else {
            this.startAjaxRequest()
        }
    }

    _isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }

        return JSON.stringify(obj) === JSON.stringify({});
    }

    openPopUp(val, openCallback, closeCallback) {
        $.magnificPopup.open({
            items: {
                src: `#${val}`
            },
            removalDelay: 300,
            mainClass: 'mfp-zoom-in',
            callbacks: {
                open: function() {
                    $('body').addClass('no-keyboard');

                    setTimeout(() => {
                        typeof openCallback === 'function' && openCallback()
                    }, 300)

                },
                close: function() {
                    $('body').removeClass('no-keyboard');

                    setTimeout(() => {
                        typeof closeCallback === 'function' && closeCallback()
                    }, 300)

                }
            },
            midClick: true
        });
    }

    startAjaxRequest() {

        httpClient.loadPresetList((response)=>{{

            var rez = response.map((resp)=> {{
                return `<option value=${resp.name} class='js-preset-btn' data-id=${resp.id}>${resp.name}</option>`
            }});

            $('.js-select-box select').append(rez.join(' '));

            initSelector('.custom-select')

        }})
    }

    getUrlVars() {
        let vars = {};
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => vars[key] = value);
        return vars;
    }


    static init() {
        return new Classified();
    }
}
