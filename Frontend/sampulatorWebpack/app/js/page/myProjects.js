import httpClient from '../sampApi';
import {APIURL} from '../../config';

export default class myProjects {
    constructor() {
        this._addEventListeners();
        this.getProjectList();
        this.params = {};
    }

    _addEventListeners() {
        const self = this;
        $('#projectsControls').on('click', 'li', function () {
            const element = $(this);
            const order = element.data('order') === 'asc' ? 'desc' : 'asc';
            const carret = element.find('img');
            $('.projects-list__controls-item').removeClass('desc');
            carret.src = order === 'asc' ? '/carret.png' : 'carret-up.png';
            order === 'asc' ? element.removeClass('desc') : element.addClass('desc');
            element.data('order', order);

            self.params = {
                sort_field: element.data('field'),
                sort_order: order
            };

            self.getProjectList(self.params);
        });

        $('.projects').on('click', '.share-button', function(e) {
            const token =  localStorage.getItem('classified');
            const link = $(this).attr('data-href');
            const title = $(this).attr('data-title');
            const description = $(this).attr('data-description');

            if (token) {
                FB.ui({
                    method: 'share',
                    href: link,
                    title: title,
                    description: description
                }, function(callback){
                    console.log(callback);
                });
            } else {
                window.toastr.error('Please log in to be able to share the project on your Facebook page');
            }

            return false;
        });

        $('#projectsList').on('click', '#downloadLink', function (e) {
            const target = $(this);

            fetch(target.attr('href'))
                .then(res => res.blob())
                .then(blob => {
                    $("<a>").attr({
                        download: target.attr('data-name'),
                        href: URL.createObjectURL(blob)
                    })[0].click();
                });

            return false;
        });

        $('#projectsList').on('click', '.js-play', function (e) {
            const target = $(this);
            const audioFiles = $('.projects__audio');
            const currentStatus = target.attr('data-status');

            audioFiles.each((index, file) => {
                file.pause();
                file.currentTime = 0;
            });

            $('.js-play[data-status="play"]').each((index, element) => {
                $(element).children('#playProject').removeClass('hidden');
                $(element).children('#stopProject').addClass('hidden');
                $(element).attr('data-status', 'stop');
            });


            if (currentStatus === 'stop') {
                target.attr('data-status', 'play');
                target.children('audio')[0].play();
                target.children('#playProject').addClass('hidden');
                target.children('#stopProject').removeClass('hidden');
            } else {
                target.attr('data-status', 'stop');
                target.children('#playProject').removeClass('hidden');
                target.children('#stopProject').addClass('hidden');
            }
        });

        $('#projectsList').on('click', '.js-remove', function (e) {
            const projectId = $(this).attr('data-id');
            httpClient.deleteProject(projectId, (result) => {
                self.getProjectList(self.params);
            })
        })

    }

    getProjectList(body) {
        httpClient.getMyProjects(body).then(projects => {
            let template = '';

            projects.forEach(project => template += this.getProjectListTemplate(project));

            $('#projectsList').html(template);

            $('.project-stars').each((index, star) => {
                var star = $(star).rateYo({
                    halfStar: true,
                    rating: $(star).attr('data-value'),
                    starWidth: '20px',
                    onSet: function (rating, rateYoInstance) {
                        const elem = $(this);
                        const voteDelay = elem.attr('voteDelay');
                        const token =  localStorage.getItem('classified');

                        if (token && +voteDelay === 0) {
                            const projectId = elem.attr('id');
                            httpClient.voteProject(projectId, rating, function (response, status) {
                                elem.attr('voteDelay', 1);
                                setTimeout(() => {
                                    elem.attr('voteDelay', 0);
                                }, 0);

                                if (200 !== status) {
                                    window.toastr.error(response.message);
                                    star.rateYo('rating', elem.attr('data-value'));
                                } else {
                                    window.toastr.success(response.message);
                                    elem.attr('data-value', response.newRate);
                                }
                            })
                        }
                    }
                });

                $(star).click(function (e) {
                    const token =  localStorage.getItem('classified');

                    if (!token) {
                        window.toastr.error('Please login to vote for project');
                        star.rateYo('rating', $(this).attr('data-value'));
                    }
                });
            });

        });
    }

    getProjectListTemplate(item) {
        let renderStars = (count) => {
            let template = '';

            for (let i = 0; i < 5; i++) {
                let image = $('#starEmpty').attr('src');
                if (i < count) {
                    image = $('#star').attr('src');
                }
                template += `<li class="projects__stars-item">
                            <img src="${image}" alt="">
                        </li>`
            }

            return template;
        };

        let formatDate = (date) => {
            const monthNames = ["january", "february", "march", "april", "may", "june",
                "july", "august", "september", "october", "november", "december"
            ];

            const d = new Date(date);
            return `${d.getDate()} ${monthNames[d.getMonth()]}`;
        };

        let renderProjectLogo = (url) => {
            let styles = url ? `background: url('${APIURL}/${url}') center no-repeat; background-size: contain;` : '';


            return `<div class="projects__item-logo" style="${styles}">
                    ${!url ? '<span class="projects__item-logo-text">LOGO...</span>' : ''}
                </div>`
        };

        return `<li class="projects__item">
        ${renderProjectLogo(item.preset.logoUrl)}
        <div class="projects__description projects__description-width">
            <div class="projects__description-content clearfix">
                <div class="projects__description-play js-play" data-status="stop">
                    <img id="playProject" src="${$('#playBtn').attr('src')}" alt="">
                    <div id="stopProject" class="projects__description-stop hidden"></div>
                    <audio class="hidden projects__audio" src="${APIURL}/${item.mp3Url}" loop></audio>
                </div>
                <div class="projects__description-text">
                    <p class="projects__description-name">${item.preset.name}</p>
                    <br>
                    <p class="projects__description-title">${item.name}</p>
                    <br>
                    <p class="projects__description-author"><strong>Created by:</strong> ${item.user.username}</p>
                </div>
                <div class="projects__description-date">
                    ${formatDate(item.createdAt)}
                </div>
                <div class="projects__description-rating">
                    <div class="project-stars" id="${item.id}" data-value="${item.rate}" voteDelay="0"></div>
                    <div class="projects__description-tag">
                        #${item.style.name}
                    </div>
                </div>
            </div>
            <div class="projects__controls">
                <a id="downloadLink" href="${APIURL}/${item.mp3Url}" data-name="${item.name}.mp3" class="projects__button" style="background-color: ${item.preset.color}" download="file.mp3">Download</a>
                <span class="projects__button share-button" 
                style="background-color: ${item.preset.color}"
                data-href="${window.origin}/classified.html?presetId=${item.preset.id}&projectId=${item.id}" 
                data-description="${item.preset.name}"
                data-title="${item.name} - ${item.user.username}">Share</span>
                <a href="/classified.html?presetId=${item.preset.id}&projectId=${item.id}" class="projects__button" style="background-color: ${item.preset.color}">Edit</a>
                <span class="projects__button projects__button--delete js-remove" data-id=${item.id} style="background-color: ${item.preset.color}">Delete</span>
            </div>
        </div>
    </li>`;
    }

    static init() {
        return new myProjects();
    }
}
