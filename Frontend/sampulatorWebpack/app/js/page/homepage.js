import httpClient from '../sampApi';
import {APIURL} from '../../config';

export default class HomePage {
    constructor() {
        this._addEventListeners();
        this.getPresetList();
    }

    _addEventListeners() {
        $('.js-content').magnificPopup({
            delegate: '.js-play', // child items selector, by clicking on it popup will open
            type: 'iframe'
        });

    }

    getPresetList() {
        let callback = (result) => {
            const presets = result.slice(0, 3);
            const contentContainer = $('.js-content');
            const presetBackground = $('.js-front-images');
            const backgroundColors = $('.js-background');
            const pseudoBackgroundColors = $('.js-pseudo-background');

            presets.forEach((preset, index) => {
                $(contentContainer[index]).append(this.getPresetTemplate(preset));
                $(presetBackground[index]).attr('src', preset.backgroundUrl);
                $(backgroundColors[index]).css('background-color', preset.color);
                $(pseudoBackgroundColors[index]).css('background-color', preset.color);
            })
        };

        httpClient.loadPresetList(callback)
    }

    getPresetTemplate(preset) {
        // preset['videoUrl'] = 'https://static.videezy.com/system/resources/previews/000/014/281/original/Pianos.mp4';
        let getVideoControl = (url) => {
            return url ? `<a href="${APIURL}/${url}" class="js-play"><img data-video="${url}" src="${$('#playIcon').attr('src')}" class="front-part__play-video fa fa-play-circle"/></a>` : '';
        };

        const presetTitle = preset.name.split('-');
        const fontSize = preset.fontSize;
        return `<div class="front-part__content" ${fontSize && `style="font-size: ${fontSize}px"`}>
                        <strong class="front-part__name">${presetTitle[0]}</strong>
                        ${presetTitle[1] ? `<span>${presetTitle[1]}</span>` : ''}
                        <div class="front-part__content-controls">
                            <a href="/classified.html?presetId=${preset.id}">
                                <img src="${$('#plusIcon').attr('src')}" alt="">
                             </a>
                            ${getVideoControl(preset.videoUrl)}                  
                        </div>
                    </div>`
    }

    static init() {
        return new HomePage();
    }
}
