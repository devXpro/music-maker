import httpClient from './sampApi.js';

export default class someLogicAllPages {
    constructor() {
        this._addEventListeners();
        this._processGlobalSettings();
    }

    _addEventListeners() {
        const self = this;
        $('.js_my_projects_item').on('click', function (e) {

            let isLoggedIn = $(this).hasClass('loggedIn');

            if(!isLoggedIn) {
                e.preventDefault();
                e.stopPropagation();
                window.login()
            }
        });

    }

    _processGlobalSettings() {
        if($('body').attr('id') !== 'allProjects') {
            httpClient.getGlobalSettings(response => {
                const {
                    facebookUrl,
                    instagramUrl
                } = response;

                $('#facebookLink').attr('href', facebookUrl);
                $('#instagramLink').attr('href', instagramUrl);
            });
        }

    }

    static init() {
        return new someLogicAllPages();
    }
}
