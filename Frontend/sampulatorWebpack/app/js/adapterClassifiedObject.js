import app from "./app";
import projectObject from "./sampObject";
import {APIURL} from "../config"
import root from './shared/root';

import bufferLoader from "./bufferLoader";
import { dragger, mainLogic } from "./core";

// initSelector()
root.Obj = {
    // ...window.samp
};

function adapterClassifiedObject(Data) {
    let urlList = {
        metronome: 'app/samples/metronome.mp3',
        metronomeUp: 'app/samples/metronome-up.mp3'
    };

    const test = {//todo remove
        'Digit1':'key_1',
        'Digit2':'key_2',
        'Digit3':'key_3',
        'Digit4':'key_4',
        'Digit5':'key_5',
        'Digit6':'key_6',
        'Digit7':'key_7',
        'Digit8':'key_8',
        'Digit9':'key_9',
        'Digit0':'key_0',
        'Minus':'MINUS',
        'Equal':'EQUALS',
        'KeyQ':'key_q',
        'KeyW':'key_w',
        'KeyE':'key_e',
        'KeyR':'key_r',
        'KeyT':'key_t',
        'KeyY':'key_y',
        'KeyU':'key_u',
        'KeyI':'key_i',
        'KeyO':'key_o',
        'KeyP':'key_p',
        'BracketLeft':'OPEN_BRACKET',
        'BracketRight':'CLOSE_BRACKET',
        'KeyA':'key_a',
        'KeyS':'key_s',
        'KeyD':'key_d',
        'KeyF':'key_f',
        'KeyG':'key_g',
        'KeyH':'key_h',
        'KeyJ':'key_j',
        'KeyK':'key_k',
        'KeyL':'key_l',
        'Semicolon':'SEMICOLON',
        'Quote':'QUOTE',
        'KeyZ':'key_z',
        'KeyX':'key_x',
        'KeyC':'key_c',
        'KeyV':'key_v',
        'KeyB':'key_b',
        'KeyN':'key_n',
        'KeyM':'key_m',
        'Comma':'COMMA',
        'Period':'DOT',
        'Slash':'SLASH'
    }

    let keys = Data.reduce((p, {key, title, color, url, fontColor}) => {
        let sound = title.replace(/'| /g, '_');
        if (url !== undefined) {
            urlList = {
                ...urlList,
                [sound]: APIURL + '/' + url
            };

            return {
                ...p, [test[key]]: {
                    color,
                    label: title,
                    sound,
                    url,
                    fontColor: fontColor || '#282a30'
                }
            }
        } else {
              return {
                ...p
              }
            }
    }, {});

    keys = (keys === undefined) ? {} : keys
    root.Obj = {
            ...root.Obj,
            urlList,
            keys
        };
}

function initApplication(s) {
    adapterClassifiedObject(s);

    projectObject.createProjectObject();
    projectObject.keyCodes();

    bufferLoader();
    mainLogic();

    app.startApplication();
    root.Obj.method.stopClearRecord();

}

function rebuildData(s) {
    adapterClassifiedObject(s);
    projectObject.keyCodes();
    app.startApplication();
    root.Obj.method.stopClearRecord();
};

root.Obj.method = {
    ...root.Obj.method,
    rebuildData,
    initApplication
};
