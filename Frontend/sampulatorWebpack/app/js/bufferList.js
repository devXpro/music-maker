import root from './shared/root';

export class BufferList {
    static audioBank = {};
    static gain = root.Obj.context.createGain();
    static commpressor = root.Obj.context.createDynamicsCompressor();

    static h = 0;
    static z = 0;
    static W = 0;
    static s = false;
    static play = false;
    static q = false;
    static hitsList = [];
    static animationFrames = null;

    constructor() {
        BufferList.gain.gain.value = 0.8;
    }

    static playProject() {
        var a, b, c, d, e, f, g, i;
        for (BufferList.h = 1e3 * root.Obj.context.currentTime - BufferList.z; BufferList.hitsList.length && BufferList.hitsList[0].time < BufferList.h;) {

            //step 1
            e = null;
            f = root.Obj.project.loops[BufferList.hitsList[0].loopId];

            if ('metronome' !== BufferList.hitsList[0].loopId) {
                if (f.hits[BufferList.hitsList[0].id] && f.hits[BufferList.hitsList[0].id].time !== BufferList.hitsList[0].time)
                    BufferList.hitsList.splice(0, 1);
                else if (!f || f.muted || (S && S !== BufferList.hitsList[0].loopId))
                    BufferList.hitsList.splice(0, 1);
                else {
                    g = BufferList.hitsList[0].sound;
                    a = root.Obj.soundsToGroups[g];
                    if (a) {
                        b = root.Obj.groupSounds[a];
                    }
                    i = f.hits;

                    //todo - this is for loop
                    for (d in i)
                        (c = i[d]),
                        c.time - BufferList.W < BufferList.hitsList[0].time &&
                        (c.sound === g && 'hat' !== c.sound
                            ? ((c.hitGain.gain.value = 0),
                                (c.hitGain = root.Obj.context.createGain()))
                            : b &&
                            b.indexOf(c.sound) > -1 &&
                            ((c.hitGain.gain.value = 0),
                                (c.hitGain = root.Obj.context.createGain())));
                    'undefined' != typeof f.hits[N[0].id],
                        BufferList.hitsList.splice(0, 1);
                }
            } else {
                BufferList.hitsList.splice(0, 1);
            }
        }

        return BufferList.h >= root.Obj.project.loopDuration - BufferList.W
            ? (BufferList.helper2(), BufferList.helper1())
            : (BufferList.getPlayedProgress(BufferList.h + BufferList.W), (BufferList.animationFrames = requestAnimFrame(BufferList.playProject)), s && !q ? D(BufferList.h) : void 0);
    }

    static helper1() {
        var a, b;
        BufferList.z = 1e3 * root.Obj.context.currentTime;
        BufferList.playLoops();
        if (!BufferList.play) {
            BufferList.play = true;
            for (a in root.Obj.sounds)
                (b = root.Obj.sounds[a]), b.liveGainNode && (b.liveGainNode.gain.value = 0);
        }
        return requestAnimFrame(BufferList.playProject);
    };

    static helper2() {
        var a, b, c, d, e, f;
        cancelAnimationFrame(BufferList.animationFrames);
        (BufferList.W = 0);
        for (a in root.Obj.sounds) {
            for (b = root.Obj.sounds[a], f = b.sources, d = 0, e = f.length; e > d; d++)
                (c = f[d]), c.stop();
            b.sources = [];
        }

        if (!BufferList.play) {
            // Set time 0
        }

        BufferList.play = false;
    }

    static helper3 (a) {
        var b, c, d, e, f;
        // console.log('metod ')
        BufferList.q = true;
        c = a;

        if (0 === Object.keys(root.Obj.project.loops[v].hits).length) {
            return (BufferList.q = false);
        }

        e = root.Obj.project.loops[v].hits;
        f = [];

        for (b in e) {
            if (((d = e[b]), !(d.time < c && d.isOverwritable))) {
                BufferList.q = !1;
                break;
            }
            BufferList.removeHit({id: b}),
                0 === Object.keys(root.Obj.project.loops[v].hits).length
                    ? f.push((BufferList.q = !1))
                    : f.push(void 0);
        }
        return f;
    }

    static removeHit (b) {
        var c, d;
        if (((d = !1), b.loopId && (d = !0), (c = b.loopId || v),
                b.loopId
                    ? (root.Obj.project.loops[c].hits[b.id].hitGain.gain.value = 0)
                    : (root.Obj.project.loops[c].hits[b.id].hitGain.gain.value = 0),
                delete root.Obj.project.loops[c].hits[b.id],
                (a = null),
            0 === Object.keys(root.Obj.project.loops[c].hits).length)
        ) {
            if (!s || v !== parseInt(c)) return (y = c), j();
        } else if (d) return root.Obj.saveRecording();
    }

    static getPlayedProgress(time) {
        return time;
    }

    static playLoops() {
        const loops = root.Obj.project.loops;
        BufferList.hitsList = [];

        loops.forEach((loop, loopIdx) => {
            loop.hits.forEach((hit, hitIdx) => {
                const time = hit.time - W;

                if (time => 0) {
                    BufferList.hitsList.push({
                        time,
                        sound: hit.sound,
                        loopId: loopIdx,
                        id: hitIdx
                    });

                    BufferList.playSample(hit.sound, time, true, loopIdx, hitIdx);
                }
            })
        });

        return BufferList.hitsList.sort(function (a, b) {
            return a.time < b.time ? -1 : a.time > b.time ? 1 : 0;
        });
    }

    static playSample(a, b, c, d, e) {
        var f, h, i, j, k;
        return (
            (f = f || null),
                (b = b || 0),
                (c = c || !1),
                c
                    ? ((k = root.Obj.context.createBufferSource()),
                    (i = root.Obj.project.loops[d].hits[e].hitGain),
                    (j = root.Obj.project.loops[d].loopGain),
                        root.Obj.sounds[a].sources.push(k),
                    k.connect(i),
                    i.connect(j),
                    j.connect(B),
                'metronome' !== d &&
                ((h = root.Obj.project.loops[d].hits[e]),
                    h.neverPlayed
                        ? ((i.gain.value = 0), (h.neverPlayed = !1))
                        : h.isOverwritable === !1 && (h.isOverwritable = !0)))
                    : (root.Obj.sounds[a].liveGainNode &&
                'hat' !== a &&
                (root.Obj.sounds[a].liveGainNode.gain.value = 0),
                    (k = root.Obj.context.createBufferSource()),
                    (root.Obj.sounds[a].liveGainNode = root.Obj.context.createGain()),
                    k.connect(root.Obj.sounds[a].liveGainNode),
                        root.Obj.sounds[a].liveGainNode.connect(B)),
            k.start || (k.start = k.noteOn),
                (k.buffer = root.Obj.sounds[a].buffer),
                (b = b / 1e3 + root.Obj.context.currentTime),
                BufferList.gain.connect(g),
                BufferList.commpressor.connect(root.Obj.context.destination),
                k.start(b)
        );
    };
}
