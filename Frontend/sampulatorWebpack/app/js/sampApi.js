import {APIURL} from "../config"
import root from './shared/root';

export default class httpClient {

    static createRequest(type, url, body, callback) {
        var xhr;
        const classifiedToken = localStorage.getItem('classified');
        const apiUrl = APIURL + url;
        xhr = new XMLHttpRequest();
        xhr.open(type, apiUrl, true);
        xhr.setRequestHeader("Authorization", classifiedToken);
        if ('PUT' === type || 'POST' === type) {
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send(JSON.stringify(body));
        } else {
            xhr.send();
        }

        xhr.onload = function () {
            try {
                callback(JSON.parse(xhr.responseText), xhr.status);
            } catch (err) {
                console.log(err.message)
            }
        };

        xhr.onerror = function () {
            console.log('Either your internet connection has dropped or our server is down.')
        };
    }

    static getProfile(callback) {
        return httpClient.createRequest('GET', `/api/user/profile`, null, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static saveProfile(callback, body) {
        return httpClient.createRequest('PUT', `/api/user/profile`, body, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static login(body, callback) {
        return httpClient.createRequest('POST', '/api/user/login', body, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static getMyProjects(body) {
        let getMyProjectsUrl = '/api/project/own_projects/list';
        let url = new URL(getMyProjectsUrl, APIURL);

        if (body) {
            Object.keys(body).forEach(key => {
                url.searchParams.append(key, body[key]);
            });
        }

        getMyProjectsUrl = url.pathname + url.search;

        return new Promise((resolve) => {
            return httpClient.createRequest('GET', getMyProjectsUrl, body, function (response, status) {
                if (status === 200) {
                    resolve(response);
                } else if(status === 403) {
                    localStorage.removeItem('classified');
                } else {
                    console.log('request failed ->', response);
                }
            }, resolve);
        })
    }

    static getStyleList() {
        return new Promise((resolve) => {
            return httpClient.createRequest('GET', '/api/sampler/style/list', {}, function (response, status) {
                if (status === 200) {
                    resolve(response);
                } else {
                    console.log('request failed ->', response);
                }
            }, resolve);
        })
    }

    static getAllProjects(body) {

        let getMyProjectsUrl = `/api/project/all_projects/list`;
        let url = new URL(getMyProjectsUrl, APIURL);

        if (body) {
            Object.keys(body).forEach(key => {
                url.searchParams.append(key, body[key]);
            });
        }

        getMyProjectsUrl = url.pathname + url.search;

        return new Promise((resolve) => {
            return httpClient.createRequest('GET', getMyProjectsUrl, body, function (response, status) {
                if (status === 200) {
                    resolve(response);
                } else if(status === 403) {
                    localStorage.removeItem('classified');
                } else {
                    console.log('request failed ->', response);
                }
            }, resolve);
        })
    }

    static loadPresetList(callback) {
        return httpClient.createRequest('GET', '/api/sampler/preset/list', null, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }

        });
    }

    static loadPresetItem(id, callback) {
        return httpClient.createRequest('GET', `/api/sampler/sample/${id}/list`, null, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static getSamplePackById(presetId, callback) {
        return httpClient.createRequest('GET', `/api/sampler/preset/${presetId}/sample_pack`, null, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static facebookToken(body, callback) {
        return httpClient.createRequest('POST', `/api/user/facebook_login`, body, function (response, status) {
            if (status === 200) {
                callback(response)
            } else {
                console.log('request failed ->', response);
            }
        });
    }

    static saveProject(b, type) {
        let projectId,
            url = '/api/project/own_projects/new';

        if (type === 'PUT') {
            projectId = root.Obj.project.projectId ? root.Obj.project.projectId : root.Obj.edit.projectId;
            url = `/api/project/own_projects/edit/${projectId}`;
        }

        httpClient.createRequest(type, url, b, function (response, status) {
            if (200 !== status) {
                console.log('error --- ', response)
            } else {
                root.Obj.project.projectId = response.id
            }
        })

    }

    static deleteProject(projectId, callback) {
        return (
            httpClient.createRequest('DELETE', `/api/project/own_projects/delete/${projectId}`, null, function (response, status) {

                if (200 !== status) {
                    if (404 === status) {
                        console.log(status, response)
                    } else {
                        console.log(status, response)
                    }
                } else {
                    callback(response);
                }
            })
        );
    }

    static cloneProject(params, callback) {
        return (
            httpClient.createRequest('POST', `/api/project/own_projects/clone/${params.projectId}`, params, function (response, status) {

                if (200 !== status) {
                    if (404 === status) {
                        console.log(status, response)
                    } else {
                        console.log(status, response)
                    }
                } else {
                    callback(response);
                }
            })
        );
    }

    static loadProject(b, callback) {
        return (
            httpClient.createRequest('GET', `/api/project/all_projects/get/${b}`, null, function (response, status) {

                if (200 !== status) {
                    if (404 === status) {
                        console.log(status, response)
                    } else {
                        console.log(status, response)
                    }
                } else {
                    callback(response);
                }
            })
        );
    }

    static voteProject(projectId, rate, callback) {
        const params = {
            projectId,
            stars: rate
        };
        return (
            httpClient.createRequest('POST', `/api/project/all_projects/vote/${projectId}/${rate}`, params, function (response, status) {

                // if (200 !== status) {
                //     if (404 === status) {
                //         console.log(status, response)
                //     } else {
                //         console.log(status, response)
                //     }
                // } else {
                    callback(response, status);
                // }
            }));
    }

    static getGlobalSettings(callback) {
        httpClient.createRequest('GET', `/api/settings/get`, null, function (response, status) {
            callback(response, status);
        });
    }
}


