//fb
import httpClient from './sampApi.js';
import root from './shared/root';
import myProjects from './page/myProjects';

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        // appId: '193176258182736',// -
        appId      : '820178758180012',//
        cookie: true,
        xfbml: true,
        version: 'v3.0'
    });

    FB.AppEvents.logPageView();

};

window.login = function() {
    FB.login((response) => {
        if (response.status === 'connected') {
            httpClient.facebookToken({
                "facebook_token": response.authResponse.accessToken
            }, (response) => {
                {
                    localStorage.setItem('classified', response.token);

                    httpClient.getProfile( (resp) => {{
                        $('.js-button-fb').removeClass('active');
                        const headerLogo = $('#userLogo');
                        $('.js-info-user').addClass('active');
                        headerLogo.attr('title', resp.username);

                        if (resp.photoUrl) {
                            headerLogo.attr('src', resp.photoUrl);
                            headerLogo.removeClass('empty');
                            headerLogo.on('error', (e) => {
                                const defaultLogo = $('#emptyLogo').attr('src');
                                headerLogo.attr('src', defaultLogo);
                                headerLogo.addClass('empty')
                            });
                        }

                        $('.js_my_projects_item').addClass('loggedIn');
                        $('.js-info-user span').text(`${resp.username}`);

                        root.personInfo = resp;
                        getInfo();
                    }} );

                }
            });
        }
    })
};

window.getInfo = function() {
    FB.api('/me', 'GET', {fields: 'location,first_name,last_name,name,id,picture.width(150).height(150)'}, function(response) {
        const headerLogo = $('#userLogo');
        $('.js-info-user').addClass('active');
        $('.js_my_projects_item').addClass('loggedIn');
        headerLogo.attr('title', response.username);

        if(response.photoUrl) {
            headerLogo.attr('src', response.photoUrl);
            headerLogo.removeClass('empty');
            headerLogo.on('error', (e) => {
                const defaultLogo = $('#emptyLogo').attr('src');
                headerLogo.attr('src', defaultLogo);
                headerLogo.addClass('empty')
            });
        }

        // $('.js-info-user span').text(`${response.first_name} ${response.last_name}`);
        $('.js-button-fb').removeClass('active');
        $('.js-btn-save').removeClass('notActive');
        $('.main.myProjects').removeClass('notLoggedIn');

        if($('body').is('#myProjects')) {
            myProjects.init();
        }
    });
};

window.logout = function() {
    window.logoutLocalStore();
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout();
        }
    });

};

window.logoutLocalStore = function() {
    root.personInfo = {};
    localStorage.removeItem('classified');
    $('.js-button-fb').addClass('active');
    $('.js-info-user').removeClass('active');
    $('.js-info-user span').text(``);
    $('.js-btn-save').addClass('notActive');
    $('.js_my_projects_item').removeClass('loggedIn');
    $('.main.myProjects').addClass('notLoggedIn');
    $('#myProjects .projects__list').empty();
}
