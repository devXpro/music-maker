let instance = null;
class root {//todo rename
    constructor() {
        if (!instance) {
            instance = this;
        }

        return instance;
    }




    /*
    * Obj
    *
    * */

    get Obj() {
        return this._winObj;
    }

    set Obj(value) {
        this._winObj = value;
    }

    /*
    * personInfo
    * */
    get personInfo() {
        return this._personInfo;
    }

    set personInfo(value) {
        this._personInfo = value;
    }
}

export default new root();
