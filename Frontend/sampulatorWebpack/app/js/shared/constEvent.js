export const Event = {
	CLICK: `click`,
	CHANGE: `change`,
	KEYDOWN: `keydown`,
	TOUCHEND: `touchend`,
	KEYUP: `keyup`,
	MOUSE_ENTER_LEAVE: `mouseenter mouseleave`,
	TOUCHSTART: `touchstart`,
	FOCUS: `focus`,
	BLUR: `blur`,
	MOUSEDOWN: `mousedown`,
    MOUSEENTER:  `mouseenter`
};
