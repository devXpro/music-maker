/**
 * Application entry point
 */
import httpClient from './js/sampApi';
import 'styles/index.scss';
import './js/plugins/MagnificPopup.js';
import './js/plugins/jquery.rateyo.js';
import toastr from './js/plugins/toastr';
import './js/adapterClassifiedObject.js';
import './js/fb';
import app from './js/app';
import Classified from './js/page/classified';
import myProjects from './js/page/myProjects';
import allProjects from './js/page/allProjects';
import HomePage from './js/page/homepage';
import 'select2';
import root from './js/shared/root';
import 'daterangepicker';
import moment from 'moment';
import someLogicAllPages from './js/someLogicAllPages';

// Notyficaion init
window.toastr = toastr;
someLogicAllPages.init();
const classifiedToken = localStorage.getItem('classified');


// CLASSIFIED
if($('body').attr('id') === 'classified') {
    app.init();
    Classified.init();
}

// MY PROJECTS
if($('body').attr('id') === 'myProjects') {
    if(classifiedToken) {
        myProjects.init();
    }
}

// All PROJECTS
if($('body').attr('id') === 'allProjects') {
    allProjects.init();
}

// HOME
if($('body').attr('id') === 'home') {
    HomePage.init();
}


if (typeof window.orientation !== 'undefined') {
    $('.main').hide();
    $('body').append( "<p class='notDesktop' style='display:none'>We support only Desktop, please visit via PC</p>" );
    $('.nav').hide();
    $('.footer').hide();
}

$(window).load(function() {
    function hidePreloader() {
        setTimeout(function() {
            $('.lds-ellipsis').remove();
            $('.notDesktop').fadeIn();
            $('.js-fade').fadeIn();
        }, 100);
    }

    hidePreloader();
});

if(classifiedToken) {

    httpClient.getProfile( (resp) => {{
        const headerLogo = $('#userLogo');
        root.personInfo = resp;
        $('.js-info-user').addClass('active');
        headerLogo.attr('title', resp.username);

        if (resp.photoUrl) {
            headerLogo.attr('src', resp.photoUrl);
            headerLogo.removeClass('empty');
            headerLogo.on('error', (e) => {
                const defaultLogo = $('#emptyLogo').attr('src');
                headerLogo.attr('src', defaultLogo);
                headerLogo.addClass('empty')
            });
        }

        $('.js_my_projects_item').addClass('loggedIn');
        $('.js-info-user span').text(`${resp.username}`);
        $('.main.myProjects').removeClass('notLoggedIn');
    }} );

} else {
    $('.js-button-fb').addClass('active');
    $('.js-btn-save').addClass('notActive');
    $('.main.myProjects').addClass('notLoggedIn');
}

setTimeout(() => {
    $(function () {

        $("#inputDat").daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            format: 'DD.MM.YYYY',
            showDropdowns: true,
            startDate: '12/31/2016',
            endDate: moment(),
            minDate: '01/01/1916',
            maxDate: '12/31/2016'
        }, function(start) {
            $('.js-birthday').val(start.format('YYYY-MM-DD'));
        });

        $('#inputDat').on('apply.daterangepicker', function(ev, picker) {
            var date = moment(picker.endDate.format('YYYY-MM-DD')),
                timestamp = moment(date).format("X");

            root.personInfo = {
                ...root.personInfo,
                dateOfBirth: ''+timestamp
            };
        });

    });
}, 500);

$(() => {
    $('.select2-enable').select2();
});
