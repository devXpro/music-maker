import {Injectable} from '@angular/core';

@Injectable()
export class ColorService {
    private componentToHex(color): string {
        const hex = color.toString(16);
        return hex.length === 1 ? '0' + hex : hex;
    }

    public rgbToHex(color): string {
        return "#" + this.componentToHex(color.r) + this.componentToHex(color.g) + this.componentToHex(color.b);
    }

    public hexToRgb(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });

        const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    public brighten(color: string, amount: number) {
        amount = (amount === 0) ? 0 : (amount || 10);
        const rgb = this.hexToRgb(color);
        rgb.r = Math.max(0, Math.min(255, rgb.r - Math.round(255 * -(amount / 100))));
        rgb.g = Math.max(0, Math.min(255, rgb.g - Math.round(255 * -(amount / 100))));
        rgb.b = Math.max(0, Math.min(255, rgb.b - Math.round(255 * -(amount / 100))));

        return this.rgbToHex(rgb);
    }

    public getColorSet(color, number, offset = 45) {
        const colors = [color];
        const step = Math.round(offset / number);
        let brighten = color;

        for (let i = 0; i < number - 1; i++) {
            brighten = this.brighten(brighten, step);
            colors.push(brighten);
        }

        return colors;
    }
}
