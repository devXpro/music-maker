import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from 'angular2-notifications';
import { apiUrl } from '../service.config';
import { SettingsInterface } from '../../../interfaces/SettingsInterface';
import { Observable } from "rxjs";

@Injectable()
export class SettingsService {
    private GetCustomSettingsUrl = apiUrl + 'api/settings/get';
    private SetCustomSettingsUrl = apiUrl + 'api/settings/set';
    constructor(private http: HttpClient, private notifications: NotificationsService) {
    }

    public getCustomSettings(): Observable<SettingsInterface> {
        return this.http.get<SettingsInterface>(this.GetCustomSettingsUrl);
    }

    public updateCustomSettings(params: SettingsInterface) {
        return this.http.post(this.SetCustomSettingsUrl, params)
            .subscribe(() => {
                this.notifications.success('Settings updated successfully');
            });
    }
}
