import {Injectable, EventEmitter} from '@angular/core';
import {PresetService} from '../preset.service/preset.service';
import {ColorService} from "../color.service/color.service";


@Injectable()
export class KeyboardService {
    private keyboardEmmiter = new EventEmitter<any>();
    private keyboardMap: string[][] = [
        ['Digit1', 'Digit2', 'Digit3', 'Digit4', 'Digit5', 'Digit6', 'Digit7', 'Digit8', 'Digit9', 'Digit0', 'Minus', 'Equal'],
        ['KeyQ', 'KeyW', 'KeyE', 'KeyR', 'KeyT', 'KeyY', 'KeyU', 'KeyI', 'KeyO', 'KeyP', 'BracketLeft', 'BracketRight'],
        ['KeyA', 'KeyS', 'KeyD', 'KeyF', 'KeyG', 'KeyH', 'KeyJ', 'KeyK', 'KeyL', 'Semicolon', 'Quote'],
        ['KeyZ', 'KeyX', 'KeyC', 'KeyV', 'KeyB', 'KeyN', 'KeyM', 'Comma', 'Period', 'Slash']
    ];
    private keyboard = [];
    private presetId: number;

    constructor(private presetService: PresetService, private colorService: ColorService) {
        const sub = this.presetService.getPresetEmitter();
        sub.subscribe(() => this.getKeysWithSamplesById(this.presetId));
    }

    public getKeyboardMap(): Array<any> {
        return this.keyboardMap;
    }

    public getKeyboardEmitter() {
        return this.keyboardEmmiter;
    }

    public getKeysWithSamplesById(id?: number) {
        this.presetId = id;
        this.keyboard = this.keyboardMap.map(row => {
            return row.map(key => {
                const preset = this.presetService.getPresetById(id);
                const sample = preset && preset.samples.find(samp => samp.key === key);
                const temp = {
                    key
                };
                if (sample) {
                    temp['sample'] = sample.url;
                    temp['title'] = sample.title;
                    temp['color'] = sample.color;
                    temp['fontColor'] = sample.fontColor;
                }
                return temp;
            });
        });


        this.keyboardEmmiter.emit(this.keyboard);
        return this.keyboard;
    }

    public getButtonsWithColorPreset(color: string, rowNumber: number) {
        const colors = this.colorService.getColorSet(color, this.keyboardMap[rowNumber].length);
        this.keyboard = this.keyboard.map((row, rowIdx) => {
            return row.map((key, keyIdx) => {
                if (rowIdx === rowNumber) {
                    key['color'] = colors[keyIdx];
                }
                return key;
            });
        });

        this.keyboardEmmiter.emit(this.keyboard);
    }

    public getButtonsWithFontColor(color: string, rowNumber: number) {
        this.keyboard = this.keyboard.map((row, rowIdx) => {
            return row.map(key => {
                if (rowIdx === rowNumber) {
                    key['fontColor'] = color;
                }
                return key;
            });
        });

        this.keyboardEmmiter.emit(this.keyboard);
    }

    public saveButtonsWithColorPreset() {
        this.keyboard.forEach(row => {
            row.forEach(key => {
                const params = {
                    ...key,
                    presetId: this.presetId,
                    title: key.title ? key.title : key.key
                };
                this.presetService.uploadSample(params);
            });
        });
    }
}