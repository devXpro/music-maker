import { Injectable } from '@angular/core';
import { apiUrl } from '../service.config';
import { SampleInterface } from '../../../interfaces/PresetsInterface';

declare const AudioContext, webkitAudioContext: any;
@Injectable()
export class AudioService {
    private context;
    private noteMap = {};
    private bufferSource;

    constructor() {
        this.context = new (AudioContext || webkitAudioContext)();
    }

    public getSamples(samples: SampleInterface[]) {
        samples.forEach(sample => sample.url && this.getSample(sample));
    }

    public getSample(sample) {
        const request = new XMLHttpRequest();
        request.open('GET', apiUrl + sample.url, true);
        request.responseType = 'arraybuffer';
        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                this.mapNote(sample.url, request.response);
            }
        };
        request.send();
    }

    public playSound(buffer, gain) {
        this.stopPlay();
        this.bufferSource = this.context.createBufferSource();
        this.bufferSource.buffer = buffer;

        const gainNode = this.context.createGain();
        gainNode.gain.value = gain;

        this.bufferSource.connect(gainNode);
        gainNode.connect(this.context.destination);

        this.bufferSource.start();
    }

    public stopPlay() {
        if (this.bufferSource) {
            this.bufferSource.stop();
        }
    }

    public triggerNote(note: string, gain: number = 127) {
        if (this.noteMap[note]) {
            this.playSound(this.noteMap[note], gain / 127);
        }
    }

    public mapNote(note: string, sample: ArrayBuffer) {
        this.context.decodeAudioData(sample, (audioBuffer) => {
            this.noteMap[note] = audioBuffer;
        });
    }
}
