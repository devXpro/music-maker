import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PresetsInterface, SampleInterface } from '../../../interfaces/PresetsInterface';
import { AudioService } from '../audio.service/audio.service';
import { NotificationsService } from 'angular2-notifications';
import { apiUrl } from '../service.config';

@Injectable()
export class PresetService {
    private presets: PresetsInterface[] = [];
    private getPresetWithSamplesUrl = apiUrl + 'api/sampler/preset/list_with_samples';
    private getPresetListUrl = apiUrl + 'api/sampler/preset/list';
    private createPresetUrl = apiUrl + 'api/admin/sampler/preset/new';
    private removePresetByIdUrl = apiUrl + 'api/admin/sampler/preset/delete/';
    private editPresetUrl = apiUrl + 'api/admin/sampler/preset/';
    private uploadSampleUrl = apiUrl + 'api/admin/sampler/sample/upload/';
    private presetEmitter = new EventEmitter<PresetsInterface[]>();

    constructor(private http: HttpClient, private audioService: AudioService, private notifications: NotificationsService) {
    }

    public getPresetWithSamples() {
        return this.http.get<PresetsInterface[]>(this.getPresetWithSamplesUrl)
            .subscribe((presets: PresetsInterface[]) => {
                let samples = [];
                presets.forEach(preset => {
                    samples = [
                        ...samples,
                        ...preset.samples
                    ];
                });
                samples = this.removeDuplicates(samples, 'url');
                this.audioService.getSamples(samples);
                this.presets = presets;
                this.presetEmitter.emit(presets);
                return presets;
            });
    }

    public removePresetById(id: number) {
        return this.http.delete(this.removePresetByIdUrl + id)
            .subscribe(() => {
                this.presets = this.presets.filter(preset => preset.id !== id);
                this.notifications.success('Preset removed successfully');
                this.presetEmitter.emit(this.presets);
            });
    }

    public createPreset(params) {
        return this.http.post<PresetsInterface>(this.createPresetUrl, params)
            .subscribe((preset: PresetsInterface) => {
                preset['samples'] = [];
                this.presets.push(preset);
                this.notifications.success('Preset created successfully');
                this.presetEmitter.emit(this.presets);
                return this.presets;
            });
    }

    public editPreset(params, id) {
        const headers = new HttpHeaders();
        const body = new FormData();
        Object.keys(params).forEach(param => body.append(param, params[param]));
        headers.append('Content-Type', 'multipart/form-data');

        return this.http.post<PresetsInterface>(this.editPresetUrl + id, body, {headers}).subscribe((preset: PresetsInterface) => {
            this.presets = this.presets.map(pres => {
                if (pres.id === preset.id) {
                    pres['name'] = preset.name;
                    pres['fontSize'] = preset.fontSize;
                }
                return pres;
            });
            this.notifications.success('Preset updated successfully');
            this.presetEmitter.emit(this.presets);
        });
    }

    public uploadSample(params) {
        const headers = new HttpHeaders();
        const body = new FormData();
        Object.keys(params).forEach(param => body.append(param, params[param]));
        headers.append('Content-Type', 'multipart/form-data');
        this.http.post(this.uploadSampleUrl + params.presetId, body, {
            headers
        }).subscribe((response: SampleInterface) => {
            this.presets = this.presets.map(preset => {
                if (preset.id === params.presetId) {
                    if (preset.samples.find(sample => sample.key === response.key)) {
                        preset.samples = preset.samples.map(sample => {
                            if (sample.key === response.key) {
                                return response;
                            }
                            return sample;
                        });
                    } else {
                        preset.samples.push(response);
                    }
                }
                return preset;
            });
            if (response.url) {
                this.audioService.getSample(response);
            }
            this.presetEmitter.emit(this.presets);
        });
    }

    public getPresetEmitter() {
        return this.presetEmitter;
    }

    public getPresetsList(): PresetsInterface[] {
        this.presetEmitter.emit(this.presets);
        return this.presets;
    }

    public getPresetById(id: number): PresetsInterface {
        const presetId = id ? id : this.getFirstPresetId();
        return this.presets.find(preset => preset.id === presetId);
    }

    private getFirstPresetId() {
        return this.presets[0] && this.presets[0].id;
    }

    private removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }
}
