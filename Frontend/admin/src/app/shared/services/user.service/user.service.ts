import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
    UserInterface,
    GetUsersListParamsInterface,
    GetUsersListResponseInterface
} from '../../../interfaces/UserInterface';
import {apiUrl} from '../service.config';
import {Router} from '@angular/router';


@Injectable()
export class UserService {
    private loginUrl = apiUrl + 'api/user/login';
    private usersListUrl = apiUrl + 'api/user/users/list';
    private user: UserInterface = JSON.parse(localStorage.getItem('currentUser'));
    private prevUrl: string;

    constructor(private http: HttpClient, private router: Router) {

    }

    public getUsersList(params: GetUsersListParamsInterface) {
        const model = new HttpParams({
            fromObject: {
                limit: String(params.limit),
                page: String(params.page),
                search: params.search,
                sort_order: params.sort_order,
                sort_field: params.sort_field
            }
        });
        return this.http.get<GetUsersListResponseInterface>(this.usersListUrl, {params: model});
    }

    public logIn(params) {
        return this.http.post<UserInterface>(this.loginUrl, params)
            .map(user => {
                if (user && user.token) {
                    this.setUser(user);
                }

                return user;
            });
    }

    public logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.user = null;
        this.router.navigate(['/login']);
    }

    private getUserT(params) {
        this.logIn(params);
    }

    public getUser(): UserInterface {
        return this.user;
    }

    public setUser(user: UserInterface) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.user = user;
    }

    public isUserLoggedIn() {
        return this.user && this.user.token;
    }

    public getPrevUrl() {
        return this.prevUrl ? this.prevUrl : '/';
    }

    public setPrevUrl(url) {
        return this.prevUrl = url;
    }
}
