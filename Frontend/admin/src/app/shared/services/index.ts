import { UserService } from './user.service/user.service';
import { PresetService } from './preset.service/preset.service';
import { KeyboardService } from './keyboard.service/keyboard.service';
import { AudioService } from './audio.service/audio.service';
import { ColorService } from './color.service/color.service';
import { SettingsService } from './settings/settings.service';

export const SharedServices = [UserService, PresetService, KeyboardService, AudioService, ColorService, SettingsService];
