import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ColorPickerModule } from 'ngx-color-picker';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedServices } from './services/';
import { SharedComponents } from './components';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatButtonModule,
        ColorPickerModule
    ],
    declarations: [
        ...SharedComponents
    ],
    exports: [
        ReactiveFormsModule,
        ...SharedComponents
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [...SharedServices]
        };
    }
}
