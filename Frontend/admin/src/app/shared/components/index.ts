import { FileInputComponent } from './file-input/file-input.component';
import { ColorpickerButtonComponent } from './colorpicker-button/colorpicker-button.component';

export const SharedComponents = [FileInputComponent, ColorpickerButtonComponent];
