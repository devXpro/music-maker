import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'colorpicker',
    templateUrl: './colorpicker-button.component.html',
    styleUrls: ['./colorpicker-button.component.scss']
})
export class ColorpickerButtonComponent {
    @Input() public buttonLabel: string;
    @Input() public buttonColor: string;
    @Input() public color: string;
    @Input() public toggle: boolean = false;
    @Output()
    private onColorChangeHandler = new EventEmitter<string>();

    public colorChangedHandler(color) {
        this.color = color;
        this.onColorChangeHandler.emit(color);
    }

    public toggleColorPicker() {
        this.toggle = !this.toggle;
    }
}
