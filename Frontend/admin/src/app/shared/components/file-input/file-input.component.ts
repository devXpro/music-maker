import { Component, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'file-input',
    templateUrl: './file-input.component.html',
    styleUrls: ['./file-input.component.scss']
})
export class FileInputComponent implements AfterViewInit {
    @Input()
    public inputLabel: string;
    @Output()
    private onFileDropHandler = new EventEmitter<any>();
    @ViewChild('fileInput', {read: ElementRef}) fileInput: ElementRef;

    public ngAfterViewInit() {
        if (this.fileInput) {
            this.fileInput.nativeElement.addEventListener('change', this.fileDropped.bind(this));
        }
    }

    public triggerInput() {
        this.fileInput.nativeElement.click();
    }

    public fileDropped(event) {
        const file = event.target.files[0];
        this.onFileDropHandler.emit(file);
        this.fileInput.nativeElement.value = "";
    }
}