import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routes';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AuthGuard } from './helpers/auth.guard';
import { GlobalErrorHandler } from './helpers/error.interceptor';
import { JwtInterceptorProvider } from './helpers/jwt.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminModule } from './admin/admin.module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/distinctUntilChanged';

import { AppComponent } from './app.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(AppRoutes),
        SharedModule.forRoot(),
        SimpleNotificationsModule.forRoot(),
        AdminModule

    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AuthGuard,
        JwtInterceptorProvider,
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler,
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
