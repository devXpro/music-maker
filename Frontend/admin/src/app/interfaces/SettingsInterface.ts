export interface SettingsInterface {
    facebookUrl: string;
    instagramUrl: string;
    paginationLimit: number;
}
