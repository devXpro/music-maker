export interface UserInterface {
    token: string;
    role: string;
}

export interface GetUsersListParamsInterface {
    search: string;
    sort_field: string;
    sort_order: string;
    limit: number | string;
    page: number | string;
}

export interface UserListItemInterface {
    city: string;
    dateOfBirth: string;
    id: number;
    photoUrl: string;
    username: string;
}

export interface GetUsersListResponseInterface {
    count: number;
    data: UserListItemInterface[];
}
