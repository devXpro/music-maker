export interface PresetsInterface {
    id: number;
    name: string;
    fontSize: number;
    samples: SampleInterface[];
}

export interface SampleInterface {
    title?: string;
    color?: string;
    fontColor?: string;
    key?: string;
    url?: string
}