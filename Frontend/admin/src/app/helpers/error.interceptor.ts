import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { UserService } from '../shared/services/user.service/user.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private notifications: NotificationsService, private injector: Injector) {
    }

    public handleError(error) {
        if (error.status === 404 || error.status === 409 || error.status === 500 || error.status === 403) {
            const message = error.error.message ? error.error.message : error.toString();
            this.notifications.error('Error!', message, {
                timeOut: 3000,
                pauseOnHover: true,
                clickToClose: true,
                position: ["top", "left"]
            });
        } else if (error.status === 401 || error.status === 403) {
            this.injector.get(UserService).logout();
        }
        console.error(error);
    };
}
  