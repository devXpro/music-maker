import {Component, OnInit} from '@angular/core';
import {SampulatorService} from '../../services/samplulator.service';
import {AudioService} from "../../services/audio.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public sampulator = this.sampulatorService.getSampulator();

  constructor(private sampulatorService: SampulatorService, private audioService: AudioService) {
  }

  public mapNote(note: string, sample: ArrayBuffer) {
    this.audioService.mapNote(note, sample);
  }

  public triggerNote(note: string) {
    console.log(note);
    this.audioService.triggerNote(note);
  }

  public ngOnInit() {
    console.log(this.sampulator);
  }

  public changeTemp(value: number) {
    console.log(value);
    this.audioService.setMusicBpm(value);
  }
}
