import { ControlsComponent } from './controls/controls.component';
import { HeaderComponent } from './header/header.component';
import { LoopsComponent } from './loops/loops.component';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { ButtonComponent } from './button/button.component';

export const SampulatorComponents = [
    ControlsComponent,
    HeaderComponent,
    LoopsComponent,
    KeyboardComponent,
    ButtonComponent
];
