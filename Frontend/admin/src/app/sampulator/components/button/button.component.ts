import { Component, AfterViewInit, Input, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { AudioService } from "../../services/audio.service";

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements AfterViewInit {
    @Input()
    public sample: string;
    @Output()
    private triggerNote = new EventEmitter<any>();
    public active: boolean = false;
    @ViewChild('fileInput', {read: ElementRef}) fileInput: ElementRef;

    constructor(private audioService: AudioService) {
    }

    ngAfterViewInit() {
        if (this.fileInput) {
            this.fileInput.nativeElement.addEventListener('change', this.mapSample.bind(this));
        }
    }

    private chooseSample() {
        this.fileInput.nativeElement.click();
    }

    private mapSample(event) {
        this.sample = event.target.files[0].name;
        const reader = new FileReader();
        reader.addEventListener('loadend', () => {
            this.audioService.mapNote(this.sample, reader.result);
        });
        reader.readAsArrayBuffer(event.target.files[0]);
    }

    private activate() {
        this.active = true;
    }

    public deactivate() {
        this.active = false;
    }

    public triggerSample() {
        this.activate();
        this.triggerNote.emit(this.sample);
    }
}
