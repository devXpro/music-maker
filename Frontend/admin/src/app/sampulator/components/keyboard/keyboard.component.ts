import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
    selector: 'app-keyboard',
    templateUrl: './keyboard.component.html',
    styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit {
    @Input()
    public keys: [any];

    @Output()
    public triggerNote = new EventEmitter<any>();

    constructor() {}

    public ngOnInit() {
        console.log(this.keys);
    }

    public handleButtonEvent(note) {
        this.triggerNote.emit(note);
    }

    @HostListener('document:keypress', ['$event'])
    private handleKeyboardEvent(event: KeyboardEvent) {
        if (this.keys.find(key => key.includes(event.key.toLowerCase()))) {
            this.triggerNote.emit(event.key.toLowerCase());
        }
    }
}
