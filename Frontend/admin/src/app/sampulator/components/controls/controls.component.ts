import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-controls',
    templateUrl: './controls.component.html',
    styleUrls: ['./controls.component.scss']
})
export class ControlsComponent implements OnInit {
    @Output()
    private tempHandler = new EventEmitter<number>();

    constructor() {
    }

    public ngOnInit() {
    }

    public inputHandler(e) {
        this.tempHandler.emit(+e.target.value);
    }
}
