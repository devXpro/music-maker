import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SampulatorContainers} from './containers/';
import {SampulatorComponents} from './components/';
import {SampulatorService} from './services/samplulator.service';
import {AudioService} from './services/audio.service';
import {SampulatorRoutingModule} from './sampulator-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SampulatorRoutingModule
  ],
  declarations: [
    ...SampulatorContainers,
    ...SampulatorComponents
  ],
  exports: [
    ...SampulatorContainers
  ],
  providers: [
    SampulatorService,
    AudioService
  ]
})
export class SampulatorModule {
}
