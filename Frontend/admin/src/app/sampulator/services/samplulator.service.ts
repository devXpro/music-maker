import { Injectable } from '@angular/core';

@Injectable()
export class SampulatorService {
    private config = {
        project: {
            loopDuration: 0,
            bpm: 130,
            barCount: 4,
            beatsPerBar: 4,
            loops: {},
        },
        user: {},
        shouldSaveProjectAfterSignIn: false,
        shouldShowSignInToSave: false,
        shouldRefreshOnOverlayClose: false,
        currentOverlay: null,
        bpmRange: 80,
        bpmStartValue: 70,
        hasDb: false,
        context: new AudioContext,
        urlList: {
            8082: 'demo_samples/orc/blissed_out_harp_1.mp3',
            8083: 'demo_samples/orc/cheesy_horn_section_riff.mp3',
            8084: 'demo_samples/orc/cheesy_horn_section_riff-2.mp3',
            8085: 'demo_samples/orc/fluter-flutter.mp3',
            8086: 'demo_samples/orc/Gm-strings.mp3',
            clap: 'demo_samples/orc/minor-verby-string-hit.mp3',
            'snare-real-rim': 'demo_samples/orc/pretty-string-swell.mp3',
            snap: 'demo_samples/orc/old-string-rise.mp3',
            'snare-big': 'demo_samples/orc/sad-strings-Amaj.mp3',
            'trap-snare-dry': 'demo_samples/orc/swelling_triad.mp3',
            kick1: 'demo_samples/orc/violin_suspense.mp3',
            'kick-dull': 'demo_samples/orc/warm-horn-hit.mp3',

            triangle: 'demo_samples/trap/808-Bass-A-Overdrive-.mp3',
            hat: 'demo_samples/trap/808-Bass-C-Deep-O.mp3',
            hat2: 'demo_samples/trap/808-Bass-Drop.mp3',
            claves: 'demo_samples/trap/808-Bass-Key-.mp3',
            shaker: 'demo_samples/trap/808-Bass-Lex.mp3',


            crash: 'demo_samples/trap/Chant-Combo.mp3',
            gunshot: 'demo_samples/trap/Chant-Hey-001_2.mp3',
            conga4: 'demo_samples/trap/Chant-Oh-001.mp3',
            conga2: 'demo_samples/trap/Chant-What-001.mp3',
            stick: 'demo_samples/trap/Chant-What-002.mp3',
            tom: 'demo_samples/trap/Chant-Who-001.mp3',
            tom2: 'demo_samples/trap/Crash-001.mp3',


            keys1: 'demo_samples/trap/Clap-001.mp3',
            keys2: 'demo_samples/trap/Clap-009.mp3',
            keys4: 'demo_samples/trap/Clap-017_2.mp3',
            keys5: 'demo_samples/trap/HiHat-Closed-006-Tight-End.mp3',
            keys3: 'demo_samples/trap/HiHat-Closed-013-Trash.mp3',
            keys6: 'demo_samples/trap/HiHat-Open-001.mp3',
            guitar1: 'demo_samples/trap/HiHat-Open-003.mp3',
            guitar2: 'demo_samples/trap/Kick-002-Knock.mp3',
            guitar3: 'demo_samples/trap/Kick-003-Sensible.mp3',
            guitar4: 'demo_samples/trap/Kick-004-Blam.mp3',
            guitar5: 'demo_samples/trap/Kick-Drop.mp3',


            jyea: 'demo_samples/trap/Snare-001.mp3',
            ugh: 'demo_samples/trap/Snare-005.mp3',
            hey: 'demo_samples/trap/Rumble-to-Hiss.mp3',
            yeauh: 'demo_samples/trap/Timpani-001-Revved.mp3',
            'ross-huh': 'demo_samples/trap/Triangle-001.mp3',
            'another-one': 'demo_samples/trap/FX-UP.mp3',
            aweyeah: 'demo_samples/trap/Gun-Shot.mp3',
            // cowbell: '?',
            drakeugh: 'demo_samples/trap/Loading-a-Gun.mp3',
            ha: 'demo_samples/trap/Orchestra-Hit-001-Revved.mp3',
            holdup: 'demo_samples/trap/Up.mp3',
            // maybach: '?',

            metronome: 'samples/metronome.mp3',
            metronomeUp: 'samples/metronome-up.mp3',
        },
        keyCodeToKeyName: {
            49: 'Digit1',
            50: 'Digit2',
            51: 'Digit3',
            52: 'Digit4',
            53: 'Digit5',
            54: 'Digit6',
            55: 'Digit7',
            56: 'Digit8',
            57: 'Digit9',
            48: 'Digit0',
            189: 'Minus',
            187: 'Equal',
            81: 'KeyQ',
            87: 'KeyW',
            69: 'KeyE',
            82: 'KeyR',
            84: 'KeyT',
            89: 'KeyY',
            85: 'KeyU',
            73: 'KeyI',
            79: 'KeyO',
            80: 'KeyP',
            219: 'BracketLeft',
            221: 'BracketRight',
            65: 'KeyA',
            83: 'KeyS',
            68: 'KeyD',
            70: 'KeyF',
            71: 'KeyG',
            72: 'KeyH',
            74: 'KeyJ',
            75: 'KeyK',
            76: 'KeyL',
            186: 'Semicolon',
            222: 'Quote',
            90: 'KeyZ',
            88: 'KeyX',
            67: 'KeyC',
            86: 'KeyV',
            66: 'KeyB',
            78: 'KeyN',
            77: 'KeyM',
            188: 'Comma',
            190: 'Period',
            191: 'Slash',
            27: 'Escape',
            13: 'Enter',
            8: 'Backspace',
            37: 'ArrowLeft',
            32: 'Space',
            16: 'ShiftLeft',
        },

        soundsToKeys: {},
        soundsToGroups: {},
        soundsToColor: {},

        keys: {
            Digit1: {sound: 'clap', color: 'purp01', label: 'String Hit'},
            Digit2: {sound: 'snap', color: 'purp02', label: 'Old String Rise'},
            Digit3: {sound: 'snare-real-rim', color: 'purp03', label: 'Pretty Swell'},
            Digit4: {sound: 'snare-big', color: 'purp04', label: 'Sad Strings'},
            Digit5: {sound: 'trap-snare-dry', color: 'purp05', label: 'Swelling Triad'},
            Digit6: {sound: 'kick1', color: 'purp06', label: 'Violin Suspense'},
            Digit7: {sound: 'kick-dull', color: 'purp07', group: '808', label: 'Warm Horn Hit',},
            Digit8: {sound: '8082', color: 'purp08', group: '808', label: 'Blissed Out Harp',},
            Digit9: {sound: '8083', color: 'purp09', group: '808', label: 'Сheesy Horn 1',},
            Digit0: {sound: '8084', color: 'purp10', group: '808', label: 'Сheesy Horn 2',},
            Minus: {sound: '8085', color: 'purp11', group: '808', label: 'Fluter-Flutter',},
            Equal: {sound: '8086', color: 'purp12', group: '808', label: 'Strings'},
            KeyQ: {sound: 'hat', color: 'jade01', label: '808 Deep'},
            KeyW: {sound: 'hat2', color: 'jade02', label: '808 Drop'},
            KeyE: {sound: 'claves', color: 'jade03', label: '808 Key'},
            KeyR: {sound: 'triangle', color: 'jade04', label: '808 Overdrive'},
            KeyT: {sound: 'shaker', color: 'jade05', label: '808 Lex'},
            KeyY: {sound: 'crash', color: 'jade06', label: 'Chant Combo'},
            KeyU: {sound: 'gunshot', color: 'jade07', label: 'Chant Hey'},
            KeyI: {sound: 'conga4', color: 'jade08', label: 'Chant Oh'},
            KeyO: {sound: 'conga2', color: 'jade09', label: 'Chant What 1'},
            KeyP: {sound: 'stick', color: 'jade10', label: 'Chant What 2'},
            BracketLeft: {sound: 'tom', color: 'jade11', label: 'Chant Who'},
            BracketRight: {sound: 'tom2', color: 'jade12', label: 'Crash'},
            KeyA: {sound: 'keys1', color: 'green01', group: 'keys', label: 'Clap 1',},
            KeyS: {sound: 'keys2', color: 'green02', group: 'keys', label: 'Clap 2',},
            KeyD: {sound: 'keys4', color: 'green03', group: 'keys', label: 'Clap 3',},
            KeyF: {sound: 'keys5', color: 'green04', group: 'keys', label: 'HiHat Closed 1',},
            KeyG: {sound: 'keys3', color: 'green05', group: 'keys', label: 'HiHat Closed 2'},
            KeyH: {sound: 'keys6', color: 'green06', group: 'keys', label: 'HiHat Open 1'},
            KeyJ: {sound: 'guitar1', color: 'green07', label: 'HiHat Open 2'},
            KeyK: {sound: 'guitar2', color: 'green08', label: 'Kick Knock'},
            KeyL: {sound: 'guitar3', color: 'green09', label: 'Kick Sensible'},
            Semicolon: {sound: 'guitar4', color: 'green10', label: 'Kick Blam'},
            Quote: {sound: 'guitar5', color: 'green11', label: 'Kick Drop'},
            KeyZ: {sound: 'jyea', color: 'peach01', label: 'Snare 1'},
            KeyX: {sound: 'ugh', color: 'peach02', label: 'Snare 2'},
            KeyC: {sound: 'hey', color: 'peach03', label: 'Rumble to Hiss'},
            KeyV: {sound: 'yeauh', color: 'peach04', label: 'Timpani Revved'},
            KeyB: {sound: 'drakeugh', color: 'peach05', label: 'Loading a Gun'},
            KeyN: {sound: 'aweyeah', color: 'peach06', label: 'Gun Shot'},
            KeyM: {sound: 'ha', color: 'peach07', label: 'Orchestra Hit'},
            Comma: {sound: 'holdup', color: 'peach08', label: 'Up'},
            Period: {sound: 'ross-huh', color: 'peach09', label: 'Triangle'},
            Slash: {sound: 'another-one', color: 'peach10', label: 'FX UP'},
        },

        groupSounds: {}
    };

    private getKeyCodes() {
        let key, keyObject;

        for (key in this.config.keys) {
            keyObject = this.config.keys[key];
            let {group, sound, color} = keyObject;

            if (!this.config.groupSounds[group] && group) {
                this.config.groupSounds[group] = [];
                this.config.groupSounds[group].push(sound);
            }

            this.config.soundsToKeys[sound] = key;
            this.config.soundsToGroups[sound] = group;
            this.config.soundsToColor[sound] = color;
        }
    }

    public getSampulator() {
        this.getKeyCodes();
        return this.config;
    }
}