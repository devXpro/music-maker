import { Injectable } from '@angular/core';

declare const AudioContext, webkitAudioContext: any;
@Injectable()
export class AudioService {
    private context;
    private samples = [
        {
            name: '1',
            path: '/assets/samples/1.mp3'
        },
        {
            name: '2',
            path: '/assets/samples/2.mp3'
        }
    ];
    private noteMap = {};
    private bufferSource;

    private musicBpm = 100;

    constructor() {
        this.context = new (AudioContext || webkitAudioContext)();
        this.samples.forEach(sample => this.getSamples(sample));
    }

    public getSamples(sample) {
        const request = new XMLHttpRequest();
        request.open('GET', sample.path, true);
        request.responseType = 'arraybuffer';
        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                this.mapNote(sample.name, request.response);
            }


        };
        // request.addEventListener('onloadend', (response) => {
        //     console.log(response);
        //     // this.mapNote(sample.name, response.data);
        // });
        request.send();
    }

    public playSound(buffer, gain) {
        this.bufferSource = this.context.createBufferSource();
        this.bufferSource.buffer = buffer;

        /**
         * Control temp of music play
         * @type {number}
         */
        this.bufferSource.playbackRate.value = this.musicBpm / 100;

        const gainNode = this.context.createGain();
        gainNode.gain.value = gain;

        this.bufferSource.connect(gainNode);
        gainNode.connect(this.context.destination);

        this.bufferSource.start();
    }

    public triggerNote(note: string, gain: number = 127) {
        if (this.noteMap[note]) {
            this.playSound(this.noteMap[note], gain / 127);
        }
    }

    public mapNote(note: string, sample: ArrayBuffer) {
        this.context.decodeAudioData(sample, (audioBuffer) => {
            this.noteMap[note] = audioBuffer;
        });
    }

    public setMusicBpm(value: number) {
        this.musicBpm = value;
    }

    public getMusicBpm(): number {
        return this.musicBpm;
    }
}
