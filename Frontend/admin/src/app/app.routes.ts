import {Routes} from '@angular/router';
import { routes as AdminRoutes } from './admin/admin-routing.module';

// export const sampulatorModule = 'app/sampulator/sampulator.module#SampulatorModule';
// export const adminModule = 'app/admin/admin.module#AdminModule';

export const AppRoutes: Routes = [
  // {
  //   path: 'home',
  //   loadChildren: sampulatorModule
  // },
  {
    path: '',
    children: [
      ...AdminRoutes
    ]
  },
  // {
  //   path: '',
  //   redirectTo: 'admin',
  //   pathMatch: 'full'
  // }
];
