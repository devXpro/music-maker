import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { LoginComponent } from './containers/login/login.component';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { PresetsListComponent } from './containers/presets-list/presets-list.component';
import { PresetDetailsComponent } from './containers/preset-details/preset-details.component';
import { UserListComponent } from './containers/user-list/user-list.component';
import { SettingsComponent } from './containers/settings/settings.component';
import { AuthGuard } from '../helpers/auth.guard';


export const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                children: [
                    {
                        path: 'presets-list',
                        component: PresetsListComponent
                    },
                    {
                        path: 'presets-list/preset/:id',
                        component: PresetDetailsComponent
                    },
                    {
                        path: 'user-list',
                        component: UserListComponent
                    },
                    {
                        path: 'settings',
                        component: SettingsComponent
                    },
                    {
                        path: '',
                        redirectTo: 'presets-list',
                        pathMatch: 'full'
                    }
                ]
            },
            {
              path: '',
              redirectTo: 'dashboard',
              pathMatch: 'full'
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
