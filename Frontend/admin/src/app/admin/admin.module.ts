import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {AdminRoutingModule} from './admin-routing.module';
import {SharedModule} from '../shared/shared.module';
import {AdminContainers} from './containers/';
import {AdminComponents} from './components/';
import {AdminComponent} from './admin.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/';
import {ConfirmDialogComponent} from './components/confirm-dialog/confirm-dialog.component';
import {EditPresetDialogComponent} from './components/edit-preset-dialog/edit-preset-dialog.component';
import {EditSampleDialogComponent} from './components/edit-sample-dialog/edit-sample-dialog.component';
import {UserListComponent} from './containers/user-list/user-list.component';
import { SettingsComponent } from './containers/settings/settings.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        AdminRoutingModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatSortModule,
        MatIconModule,
        MatDialogModule,
        MatPaginatorModule
    ],
    declarations: [
        AdminComponent,
        ...AdminContainers,
        ...AdminComponents,
        EditSampleDialogComponent,
        UserListComponent,
        SettingsComponent
    ],
    exports: [
        ...AdminContainers
    ],
    providers: [],
    entryComponents: [ConfirmDialogComponent, EditPresetDialogComponent, EditSampleDialogComponent]
})
export class AdminModule {
}
