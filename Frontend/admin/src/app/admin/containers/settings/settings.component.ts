import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SettingsService } from '../../../shared/services/settings/settings.service';
import { SettingsInterface } from '../../../interfaces/SettingsInterface';
import { Subscription } from "rxjs";

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
    public settingsForm = new FormGroup({
        facebookUrl: new FormControl(),
        instagramUrl: new FormControl(),
        paginationLimit: new FormControl('', Validators.pattern('^(0|[1-9][0-9]*)$'))
    });

    private subscription: Subscription;

    constructor(private settingsService: SettingsService) {
    }

    public ngOnInit() {
        this.subscription = this.settingsService.getCustomSettings()
            .subscribe(params => {
                this.settingsForm.patchValue({
                    facebookUrl: params.facebookUrl,
                    instagramUrl: params.instagramUrl,
                    paginationLimit: params.paginationLimit
                });
            });
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public approveSettings({value, valid}: {value: SettingsInterface, valid: boolean}) {
        if (valid) {
            this.settingsService.updateCustomSettings(value);
        }
    }
}
