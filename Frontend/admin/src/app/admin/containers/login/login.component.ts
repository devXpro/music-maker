import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services/user.service/user.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public userForm = new FormGroup({
        username: new FormControl(),
        password: new FormControl()
    });

    constructor(private userService: UserService, private router: Router) {
    }

    public ngOnInit() {
        if (this.userService.isUserLoggedIn()) {
            this.router.navigate(['/dashboard']);
        }
    }

    public loginHandler() {
        this.userService.logIn(this.userForm.value)
            .subscribe(user  => {
                const url = this.userService.getPrevUrl();
                this.userService.setUser(user);
                this.router.navigate([url]);
            });
    }
}
