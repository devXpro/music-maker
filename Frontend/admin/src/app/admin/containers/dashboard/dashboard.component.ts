import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../shared/services/user.service/user.service';
import {PresetService} from '../../../shared/services/preset.service/preset.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    constructor(private userService: UserService, private presetService: PresetService) {
    }

    public ngOnInit() {
        this.presetService.getPresetWithSamples();
    }

    public logoutHandler() {
        this.userService.logout();
    }
}
