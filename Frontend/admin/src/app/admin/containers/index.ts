import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PresetsListComponent } from './presets-list/presets-list.component';
import { PresetDetailsComponent } from './preset-details/preset-details.component';

export const AdminContainers = [
    LoginComponent,
    DashboardComponent,
    PresetsListComponent,
    PresetDetailsComponent
];
