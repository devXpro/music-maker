import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { PresetService } from '../../../shared/services/preset.service/preset.service';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { EditPresetDialogComponent } from '../../components/edit-preset-dialog/edit-preset-dialog.component';

@Component({
    selector: 'app-presets-list',
    templateUrl: './presets-list.component.html',
    styleUrls: ['./presets-list.component.scss']
})
export class PresetsListComponent implements OnInit {
    private presetEmitter = this.presetService.getPresetEmitter();
    private deletePresetDialogRef: MatDialogRef<ConfirmDialogComponent>;
    public presets = this.presetService.getPresetsList();
    public displayedColumns = ['id', 'name', 'actions'];
    public presetsForm = new FormGroup({
        presetName: new FormControl()
    });

    constructor(private presetService: PresetService, public dialog: MatDialog) {
    }

    public ngOnInit() {
        this.presetEmitter.subscribe(presets => this.presets = [...presets]);
    }

    public onDeleteClickHandler(id: number) {
        const title = 'Are you sure?';
        this.deletePresetDialogRef = this.dialog.open(ConfirmDialogComponent, <any>{
            data: {
                title: title
            }
        });

        this.deletePresetDialogRef.afterClosed().subscribe(status => {
            if (status) {
                this.presetService.removePresetById(id);
            }
        });
    }

    public onEditClickHandler(id: number) {
        const title = 'Edit preset';
        const editPresetDialogRef: MatDialogRef<EditPresetDialogComponent> = this.dialog.open(EditPresetDialogComponent, <any>{
            data: {
                title: title,
                preset: this.presets.find(preset => preset.id === id),
                id
            }
        });

        editPresetDialogRef.afterClosed().subscribe(params => {
            if (params) {
                this.presetService.editPreset(params, id);
            }
        });
    }

    public createPreset() {
        const params = {
            name: this.presetsForm.get('presetName').value
        };
        this.presetService.createPreset(params);
        this.presetsForm.reset();
    }
}
