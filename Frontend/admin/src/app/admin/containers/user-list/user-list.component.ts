import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../shared/services/user.service/user.service';
import {GetUsersListParamsInterface, GetUsersListResponseInterface} from '../../../interfaces/UserInterface';
import {MatSort} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy, AfterViewInit {
    public presetsForm = new FormGroup({
        search: new FormControl()
    });
    public model: GetUsersListParamsInterface = {
        search: '',
        sort_field: '',
        sort_order: '',
        limit: 10,
        page: 1,
    };

    public list: GetUsersListResponseInterface = {
        count: 0,
        data: []
    };
    public displayedColumns = ['id', 'photoUrl', 'username', 'dateOfBirth', 'city'];

    @ViewChild(MatSort) sort: MatSort;

    private requestSubscription: Subscription;
    private searchSubscription: Subscription;

    constructor(private userService: UserService) {
    }

    public ngOnInit() {
        this.getUsersList();

        this.searchSubscription = this.presetsForm.get('search').valueChanges
            .debounceTime(500)
            .distinctUntilChanged()
            .switchMap(term => this.searchUser(term))
            .subscribe(list => {
                this.list = {
                    ...list
                };
            });
    }

    public ngOnDestroy() {
        this.requestSubscription.unsubscribe();
        this.searchSubscription.unsubscribe();
    }

    public ngAfterViewInit() {
        this.sort.sortChange.subscribe(({active, direction}) => {
            this.model = {
                ...this.model,
                sort_field: direction ? active : '',
                sort_order: direction ? direction : ''
            };
            this.getUsersList();
        });
    }

    public handlePaginator(pagination) {
        this.model = {
            ...this.model,
            page: pagination.pageIndex + 1,
            limit: pagination.pageSize
        };

        this.getUsersList();
    }

    private searchUser(term): Observable<GetUsersListResponseInterface> {
        this.model = {
            ...this.model,
            search: term
        };

        return this.userService.getUsersList(this.model);
    }

    private getUsersList() {
        this.requestSubscription = this.userService.getUsersList(this.model)
            .subscribe(users => {
                this.list = {
                    ...users
                };
            });
    }
}
