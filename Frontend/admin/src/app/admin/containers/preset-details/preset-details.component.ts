import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {PresetService} from '../../../shared/services/preset.service/preset.service';
import {KeyboardService} from '../../../shared/services/keyboard.service/keyboard.service';
import {Subscription} from 'rxjs/Subscription';
import {AudioService} from '../../../shared/services/audio.service/audio.service';
import {ColorService} from "../../../shared/services/color.service/color.service";
import {EditSampleDialogComponent} from '../../components/edit-sample-dialog/edit-sample-dialog.component';

@Component({
    selector: 'app-preset-details',
    templateUrl: './preset-details.component.html',
    styleUrls: ['./preset-details.component.scss']
})
export class PresetDetailsComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private keyBoardSubscription: Subscription;
    private presetEmitter: Subscription;
    private editSampleDialogRef: MatDialogRef<EditSampleDialogComponent>;
    public keys;
    public preset;
    public selectedRow: number = 0;

    constructor(private presetService: PresetService,
                private keyboardService: KeyboardService,
                private route: ActivatedRoute,
                private audioService: AudioService,
                public dialog: MatDialog) {
    }

    public ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            const id = +params['id']; // (+) converts string 'id' to a number
            this.keys = this.keyboardService.getKeysWithSamplesById(id);
            this.preset = this.presetService.getPresetById(id);
            this.keyBoardSubscription = this.keyboardService.getKeyboardEmitter()
                .subscribe(keys => this.keys = keys);
            this.presetEmitter = this.presetService.getPresetEmitter().subscribe(presets => {
                this.preset = presets.find(pres => pres.id === id);
            });
        });
    }

    public rowColorChanged(color) {
        this.keyboardService.getButtonsWithColorPreset(color, this.selectedRow);
    }

    public saveChanges() {
        this.keyboardService.saveButtonsWithColorPreset();
    }

    public fontColorChanged(color) {
        this.keyboardService.getButtonsWithFontColor(color, this.selectedRow);
    }

    public onEditSampleClicked(sample) {
        const title = 'Edit sample';
        this.editSampleDialogRef = this.dialog.open(EditSampleDialogComponent, <any>{
            data: {
                title: title,
                sample: sample,
                mapNote: this.mapNote.bind(this),
                triggerNote: this.triggerNote.bind(this)
            }
        });

        this.editSampleDialogRef.afterClosed().subscribe(body => {
            if (body) {
                const params = {
                    ...body,
                    presetId: this.preset.id
                };
                this.presetService.uploadSample(params);
            }
        });
    }

    public mapNote(note, sample) {
        this.audioService.mapNote(note, sample)
    }

    public triggerNote(note: string) {
        this.audioService.triggerNote(note);
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
        this.keyBoardSubscription.unsubscribe();
        this.presetEmitter.unsubscribe();
        this.audioService.stopPlay();
    }
}
