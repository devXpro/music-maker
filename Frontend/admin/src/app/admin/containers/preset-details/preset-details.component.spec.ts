import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresetDetailsComponent } from './preset-details.component';

describe('PresetDetailsComponent', () => {
  let component: PresetDetailsComponent;
  let fixture: ComponentFixture<PresetDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresetDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresetDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
