import { Component, Inject, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-edit-sample-dialog',
    templateUrl: './edit-sample-dialog.component.html',
    styleUrls: ['./edit-sample-dialog.component.scss']
})
export class EditSampleDialogComponent implements OnInit {
    public sampleForm: FormGroup;
    public file;
    public color: string = '#eef0fa';
    public active: boolean = false;
    public fontColor: string = '#000';
    private fileName;

    constructor(public dialogRef: MatDialogRef<EditSampleDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {
    }

    public ngOnInit() {
        this.sampleForm = new FormGroup({
            title: new FormControl(this.data.sample.title),
        });
        const {
            color = '#eef0fa',
            fontColor = '#000',
            sample
        } = this.data.sample;

        this.color = color;
        this.fontColor = fontColor;
        this.fileName = sample;
    }

    public fileDropped(file) {
        this.file = file;
        this.fileName = file.name + '_new';
        if (!this.data.sample.title || this.data.sample.title == 'title') {
            this.sampleForm.controls['title'].setValue(this.file.name.replace('.mp3',''));
        }
        const reader = new FileReader();
        reader.addEventListener('loadend', () => {
            this.data.mapNote(this.fileName, reader.result);
        });
        reader.readAsArrayBuffer(file);
    }

    public playSample() {
        if (this.fileName) {
            this.data.triggerNote(this.fileName);
            this.active = true;
        }
    }

    public removeActiveClass() {
        this.active = false;
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onOkClose() {
        const params = {
            ...this.sampleForm.value,
            mp3File : this.file,
            color: this.color,
            fontColor: this.fontColor,
            key: this.data.sample.key
        };

        this.dialogRef.close(params);
    }

}
