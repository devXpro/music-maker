import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSampleDialogComponent } from './edit-sample-dialog.component';

describe('EditSampleDialogComponent', () => {
  let component: EditSampleDialogComponent;
  let fixture: ComponentFixture<EditSampleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSampleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSampleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
