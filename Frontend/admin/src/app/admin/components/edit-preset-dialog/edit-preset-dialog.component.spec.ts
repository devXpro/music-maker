import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPresetDialogComponent } from './edit-preset-dialog.component';

describe('EditPresetDialogComponent', () => {
  let component: EditPresetDialogComponent;
  let fixture: ComponentFixture<EditPresetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPresetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPresetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
