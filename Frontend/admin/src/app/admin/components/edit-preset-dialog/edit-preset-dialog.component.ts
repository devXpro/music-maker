import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-edit-preset-dialog',
    templateUrl: './edit-preset-dialog.component.html',
    styleUrls: ['./edit-preset-dialog.component.scss']
})
export class EditPresetDialogComponent implements OnInit{
    public presetForm: FormGroup;
    public color: string;
    public logoImage;
    public backgroundImage;
    public video;

    constructor(public dialogRef: MatDialogRef<EditPresetDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data) {
    }

    public ngOnInit() {
        const {
            color
        } = this.data.preset;

        this.presetForm = new FormGroup({
            name: new FormControl(this.data.preset.name),
            fontSize: new FormControl(this.data.preset.fontSize)
        });

        this.color = color;
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onOkClose() {
        const params = {
            color: this.color,
            logoImage: this.logoImage,
            backgroundImage: this.backgroundImage,
            video: this.video,
            ...this.presetForm.value
        };

        Object.keys(params).forEach(key => params[key] === undefined && delete params[key]);

        this.dialogRef.close(params);
    }
}
