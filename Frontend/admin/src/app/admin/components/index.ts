import { KeyboardComponent } from './keyboard/keyboard.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { KeyboardButtonComponent } from './keyboard-button/keyboard-button.component';
import { EditPresetDialogComponent } from './edit-preset-dialog/edit-preset-dialog.component';

export const AdminComponents = [
    KeyboardComponent,
    ConfirmDialogComponent,
    KeyboardButtonComponent,
    EditPresetDialogComponent
];
