import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-keyboard',
    templateUrl: './keyboard.component.html',
    styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit {
    @Input()
    public keys: string[] = [];
    @Output()
    public triggerNote = new EventEmitter<string>();
    @Output()
    public editSample = new EventEmitter<string>();

    constructor() {
    }

    public ngOnInit() {
    }

    public playNote(url) {
        this.triggerNote.emit(url);
    }

    public editSampleHandler(sample) {
        this.editSample.emit(sample);
    }
}
