import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-keyboard-button',
    templateUrl: './keyboard-button.component.html',
    styleUrls: ['./keyboard-button.component.scss']
})
export class KeyboardButtonComponent {
    @Input()
    public sample;
    @Output()
    public triggerNote = new EventEmitter<string>();
    @Output()
    public editSample = new EventEmitter<string>();

    public active: boolean = false;

    constructor() {
    }

    public setActiveClass() {
        if (this.sample.sample) {
            this.triggerNote.emit(this.sample.sample);
            this.active = true;
        }
    }

    public removeActiveClass() {
        this.active = false;
    }

    public editSampleHandler() {
        this.editSample.emit(this.sample);
    }
}
